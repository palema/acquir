(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/admin/services/common.service.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/services/common.service.ts ***!
  \**************************************************/
/*! exports provided: CommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonService", function() { return CommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm2015/ng.apollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);




let CommonService = class CommonService {
    constructor(apollo) {
        this.apollo = apollo;
        this.shopProducts = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
    query ShopProducts {
      shopProducts{
        title
        sku
        quantity
      }
    }
  `;
        this.shopOrders = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
    query ShopOrders {
        shopOffers{
          user
          products{
            quantity
          }
        }
    }
  `;
        this.shopOffers = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
    query ShopOffers{
        shopOffers{
          user
          products{
            quantity
          }
          offer
        }
    }
  `;
        this.rootDepartments = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
    query RootDepartment {
      rootDepartment{
        name
        children{
          name
        }
      }
    }
  `;
    }
    getShopProducts() {
        return this.apollo.watchQuery({
            query: this.shopProducts
        }).valueChanges;
    }
    getShopOffers() {
        return this.apollo.watchQuery({
            query: this.shopOffers
        }).valueChanges;
    }
    getShopOrders() {
        return this.apollo.watchQuery({
            query: this.shopOrders
        }).valueChanges;
    }
    getRootDepartment() {
        return this.apollo.watchQuery({
            query: this.rootDepartments
        }).valueChanges;
    }
};
CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
], CommonService);



/***/ }),

/***/ "./src/app/shared/action.service.ts":
/*!******************************************!*\
  !*** ./src/app/shared/action.service.ts ***!
  \******************************************/
/*! exports provided: ActionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionService", function() { return ActionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



let ActionService = class ActionService {
    constructor() {
        this.sidebar_open = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.status_tracker = true;
    }
    toggle_sidebar() {
        this.sidebar_open.next(!this.status_tracker);
        this.status_tracker = !this.status_tracker;
    }
};
ActionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ActionService);



/***/ }),

/***/ "./src/app/shared/components/sidebar/sidebar.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/shared/components/sidebar/sidebar.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#sidebar-wrapper{\r\n    width: 220px;\r\n    height: 100vh;\r\n    background-color: #119ebd;\r\n    position: fixed;\r\n    z-index: 15000;\r\n    transition: .2s linear;\r\n    max-width: calc(100vw - 15px);\r\n    color: white;\r\n}\r\n\r\n#option-list li{\r\n    margin:9px 0;\r\n}\r\n\r\n#sidebar-inner{\r\n    margin: 20px 10px;\r\n\tposition: relative;\r\n}\r\n\r\n#sidebar-contents{\r\n    position: absolute;\r\n    top: 10px;\r\n    width: 100%;\r\n}\r\n\r\n#nav-wrapper{\r\n    position: relative;\r\n}\r\n\r\n#nav-container{\r\n    position: absolute;\r\n    right: 20px;\r\n}\r\n\r\n#backdrop{\r\n    position: fixed;\r\n    width: 100vw;\r\n    height: 100vh;\r\n    background-color: black;\r\n    opacity: 0.6;\r\n    visibility: hidden;\r\n    z-index: 11000;\r\n}\r\n\r\n#icon-wrapper{\r\n    width: 50px;\r\n    height: 50px;\r\n    margin: 0 auto;\r\n    position: relative;\r\n}\r\n\r\n#icon-inner{\r\n    width: inherit;\r\n    height: inherit;\r\n    border-radius: 200px;\r\n    overflow: hidden;\r\n}\r\n\r\n#icon-inner img{\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n\r\n#username{\r\n    font-size: 16px;\r\n    text-align: center;\r\n}\r\n\r\n#link-wrapper{\r\n    margin-top: 30px;\r\n}\r\n\r\n@media screen and (min-width: 768px){\r\n    #sidebar-wrapper, #backdrop{\r\n        display: none;\r\n    }\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvc2lkZWJhci9zaWRlYmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osYUFBYTtJQUNiLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2YsY0FBYztJQUNkLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxpQkFBaUI7Q0FDcEIsa0JBQWtCO0FBQ25COztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztBQUNmOztBQUVBO0lBQ0ksZUFBZTtJQUNmLFlBQVk7SUFDWixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osY0FBYztJQUNkLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2Ysb0JBQW9CO0lBQ3BCLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJO1FBQ0ksYUFBYTtJQUNqQjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvc2lkZWJhci9zaWRlYmFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc2lkZWJhci13cmFwcGVye1xyXG4gICAgd2lkdGg6IDIyMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxMTllYmQ7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB6LWluZGV4OiAxNTAwMDtcclxuICAgIHRyYW5zaXRpb246IC4ycyBsaW5lYXI7XHJcbiAgICBtYXgtd2lkdGg6IGNhbGMoMTAwdncgLSAxNXB4KTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuI29wdGlvbi1saXN0IGxpe1xyXG4gICAgbWFyZ2luOjlweCAwO1xyXG59XHJcblxyXG4jc2lkZWJhci1pbm5lcntcclxuICAgIG1hcmdpbjogMjBweCAxMHB4O1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuI3NpZGViYXItY29udGVudHN7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDEwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuI25hdi13cmFwcGVye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4jbmF2LWNvbnRhaW5lcntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4jYmFja2Ryb3B7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB3aWR0aDogMTAwdnc7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBvcGFjaXR5OiAwLjY7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICB6LWluZGV4OiAxMTAwMDtcclxufVxyXG5cclxuI2ljb24td3JhcHBlcntcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbiNpY29uLWlubmVye1xyXG4gICAgd2lkdGg6IGluaGVyaXQ7XHJcbiAgICBoZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMDBweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcbiNpY29uLWlubmVyIGltZ3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4jdXNlcm5hbWV7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbiNsaW5rLXdyYXBwZXJ7XHJcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCl7XHJcbiAgICAjc2lkZWJhci13cmFwcGVyLCAjYmFja2Ryb3B7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgIH1cclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/shared/components/sidebar/sidebar.component.html":
/*!******************************************************************!*\
  !*** ./src/app/shared/components/sidebar/sidebar.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"backdrop\" (click)=\"toggle_sidebar()\" [style.visibility]=\"sidebar_open?'visible':'hidden'\"></div>\n<div id=\"sidebar-wrapper\" [style.background-color]=\"color\" [style.right]=\"sidebar_open?'0px':'-240px'\">\n  <div id=\"sidebar-inner\">\n    <div id=\"sidebar-contents\">\n      <div id=\"icon-wrapper\">\n        <div id=\"icon-inner\">\n          <img (click)=\"navigate('user/account/profile')\" src=\"../../../../assets/user_icon.png\" id=\"icon\">\n        </div>\n      </div>\n      <div id=\"username\">Bisque</div>\n      <div id=\"link-wrapper\">\n        <ul id=\"option-list\">\n          <li>\n            <form #shopCreationForm=\"ngForm\" (ngSubmit)=\"createShop(shopCreationForm)\">\n                <input type=\"text\" placeholder=\"shop name\" required minlength=\"5\" name=\"shopName\" ngModel>\n                <input type=\"submit\" value=\"ok\">\n            </form>\n          </li>\n          <li><span (click)=\"navigate('/create-shop')\">Create Shop</span></li>\n          <li><span (click)=\"navigate('/admin')\">Manage Shop</span></li>\n          <li (click)=\"navigate('/user/account/finance')\">Finance</li>\n          <li (click)=\"navigate('/user/account/settings')\">Settings</li>\n          <li (click)=\"logout()\">Sign Out</li>\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/shared/components/sidebar/sidebar.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/components/sidebar/sidebar.component.ts ***!
  \****************************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _action_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../action.service */ "./src/app/shared/action.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_core_registration_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/registration/authentication.service */ "./src/app/core/registration/authentication.service.ts");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm2015/ng.apollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_6__);







let SidebarComponent = class SidebarComponent {
    constructor(action_service, router, auth, apollo) {
        this.action_service = action_service;
        this.router = router;
        this.auth = auth;
        this.apollo = apollo;
    }
    ngOnInit() {
        this.action_service.sidebar_open.subscribe((status) => {
            this.sidebar_open = status;
        });
    }
    toggle_sidebar() {
        this.action_service.toggle_sidebar();
    }
    navigate(route) {
        this.action_service.toggle_sidebar();
        this.router.navigate([route]);
    }
    logout() {
        // this.auth.sign_out()
        // this.router.navigate(['/user'])
    }
    createShop(form) {
        if (form.valid) {
            console.log(form.value.shopName);
            this.apollo.mutate({
                mutation: graphql_tag__WEBPACK_IMPORTED_MODULE_6___default.a `
         mutation CreateShop($name:String!){
            createShop(name:$name){
              name
            }
          }
        `,
                variables: {
                    name: form.value.shopName
                }
            }).subscribe(({ data }) => {
                console.log(data);
            }, (error) => {
                console.log(error);
            });
        }
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], SidebarComponent.prototype, "color", void 0);
SidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sidebar',
        template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/shared/components/sidebar/sidebar.component.html"),
        styles: [__webpack_require__(/*! ./sidebar.component.css */ "./src/app/shared/components/sidebar/sidebar.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_action_service__WEBPACK_IMPORTED_MODULE_2__["ActionService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        src_app_core_registration_authentication_service__WEBPACK_IMPORTED_MODULE_4__["RegistrationService"],
        apollo_angular__WEBPACK_IMPORTED_MODULE_5__["Apollo"]])
], SidebarComponent);



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/sidebar/sidebar.component */ "./src/app/shared/components/sidebar/sidebar.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");






let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"]
        ],
        exports: [
            _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]
        ]
    })
], SharedModule);



/***/ })

}]);
//# sourceMappingURL=common.js.map