(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-admin-admin-module"],{

/***/ "./src/app/admin/admin-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/admin-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order/order.component */ "./src/app/admin/order/order.component.ts");
/* harmony import */ var _offer_offer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./offer/offer.component */ "./src/app/admin/offer/offer.component.ts");
/* harmony import */ var _admin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin.component */ "./src/app/admin/admin.component.ts");






const routes = [
    {
        path: '', component: _admin_component__WEBPACK_IMPORTED_MODULE_5__["AdminComponent"], children: [
            { path: 'orders', component: _order_order_component__WEBPACK_IMPORTED_MODULE_3__["OrderComponent"] },
            { path: 'offers', component: _offer_offer_component__WEBPACK_IMPORTED_MODULE_4__["OfferComponent"] },
            { path: 'catalogue', loadChildren: './catalogue/catalogue.module#CatalogueModule' },
            { path: '', redirectTo: 'catalogue', pathMatch: 'full' },
        ]
    }
];
let AdminRoutingModule = class AdminRoutingModule {
};
AdminRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AdminRoutingModule);



/***/ }),

/***/ "./src/app/admin/admin.component.css":
/*!*******************************************!*\
  !*** ./src/app/admin/admin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWluLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<app-sidebar [color]=\"'rgb(10, 15, 80)'\"></app-sidebar>\n<div class=\"main-grid\">\n    <div class=\"main-content-area\">\n        <router-outlet></router-outlet>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AdminComponent = class AdminComponent {
    constructor() { }
    ngOnInit() {
    }
};
AdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-admin',
        template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
        styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AdminComponent);



/***/ }),

/***/ "./src/app/admin/admin.module.ts":
/*!***************************************!*\
  !*** ./src/app/admin/admin.module.ts ***!
  \***************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-routing.module */ "./src/app/admin/admin-routing.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _admin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order/order.component */ "./src/app/admin/order/order.component.ts");
/* harmony import */ var _offer_offer_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./offer/offer.component */ "./src/app/admin/offer/offer.component.ts");
/* harmony import */ var _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./navigation/navigation.component */ "./src/app/admin/navigation/navigation.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");










let AdminModule = class AdminModule {
};
AdminModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _admin_component__WEBPACK_IMPORTED_MODULE_5__["AdminComponent"],
            _order_order_component__WEBPACK_IMPORTED_MODULE_6__["OrderComponent"],
            _offer_offer_component__WEBPACK_IMPORTED_MODULE_7__["OfferComponent"],
            _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_8__["AdminNavigationComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _admin_routing_module__WEBPACK_IMPORTED_MODULE_3__["AdminRoutingModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"]
        ]
    })
], AdminModule);



/***/ }),

/***/ "./src/app/admin/navigation/navigation.component.css":
/*!***********************************************************!*\
  !*** ./src/app/admin/navigation/navigation.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul{\r\n    list-style: none;\r\n    height:100%;\r\n}\r\n\r\nli{\r\n    display: inline-block;\r\n    margin: 0 5px;\r\n    height: calc( 100% - 2px);\r\n}\r\n\r\nli>div{\r\n    margin-top: 20px;\r\n}\r\n\r\nli:hover{\r\n    color: lightblue;\r\n    cursor: pointer;\r\n}\r\n\r\nnav{\r\n    position: fixed;\r\n    width: 100%;\r\n    background-color: rgb(10, 15, 80);\r\n    z-index: 5000;\r\n    color: white;\r\n    height:80px;\r\n}\r\n\r\n.active-route{\r\n    border-bottom: 2px solid white;\r\n}\r\n\r\n@media screen and (min-width: 768px){\r\n    #menu-icon{\r\n        display: none;\r\n    }\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztBQUNmOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGFBQWE7SUFDYix5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixXQUFXO0lBQ1gsaUNBQWlDO0lBQ2pDLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVztBQUNmOztBQUdBO0lBQ0ksOEJBQThCO0FBQ2xDOztBQUVBO0lBQ0k7UUFDSSxhQUFhO0lBQ2pCO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi9uYXZpZ2F0aW9uL25hdmlnYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInVse1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIGhlaWdodDoxMDAlO1xyXG59XHJcblxyXG5saXtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIG1hcmdpbjogMCA1cHg7XHJcbiAgICBoZWlnaHQ6IGNhbGMoIDEwMCUgLSAycHgpO1xyXG59XHJcblxyXG5saT5kaXZ7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcblxyXG5saTpob3ZlcntcclxuICAgIGNvbG9yOiBsaWdodGJsdWU7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbm5hdntcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDEwLCAxNSwgODApO1xyXG4gICAgei1pbmRleDogNTAwMDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGhlaWdodDo4MHB4O1xyXG59XHJcblxyXG5cclxuLmFjdGl2ZS1yb3V0ZXtcclxuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCB3aGl0ZTtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY4cHgpe1xyXG4gICAgI21lbnUtaWNvbntcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/admin/navigation/navigation.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/navigation/navigation.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\n    <ul>\n      <li routerLink=\"catalogue\" routerLinkActive=\"active-route\" class=\"route\"><div>catalogue</div></li>\n      <li routerLink=\"orders\" routerLinkActive=\"active-route\" class=\"route\"><div>orders</div></li>\n      <li routerLink=\"offers\" routerLinkActive=\"active-route\" class=\"route\"><div>offers</div></li>\n      <li style=\"color: white; float:right; clear:both;\"><div id=\"menu-icon\" class=\"fa fa-bars\" (click)=\"toggle_sidebar()\"></div></li>\n    </ul>\n</nav>"

/***/ }),

/***/ "./src/app/admin/navigation/navigation.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/navigation/navigation.component.ts ***!
  \**********************************************************/
/*! exports provided: AdminNavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminNavigationComponent", function() { return AdminNavigationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_action_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/action.service */ "./src/app/shared/action.service.ts");



let AdminNavigationComponent = class AdminNavigationComponent {
    constructor(action_service) {
        this.action_service = action_service;
    }
    ngOnInit() {
    }
    toggle_sidebar() {
        this.action_service.toggle_sidebar();
    }
};
AdminNavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navigation',
        template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/admin/navigation/navigation.component.html"),
        styles: [__webpack_require__(/*! ./navigation.component.css */ "./src/app/admin/navigation/navigation.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_action_service__WEBPACK_IMPORTED_MODULE_2__["ActionService"]])
], AdminNavigationComponent);



/***/ }),

/***/ "./src/app/admin/offer/offer.component.css":
/*!*************************************************!*\
  !*** ./src/app/admin/offer/offer.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#offer-table{\r\n    width: 100%;\r\n    border-spacing:0;\r\n}\r\n\r\n.table-qty{\r\n    width: 15%;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    padding-right: 5px;\r\n}\r\n\r\n.table-item{\r\n    width: 60%;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    padding-right: 10px;\r\n}\r\n\r\n.table-amount{\r\n    width: 25%;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    padding-right: 10px;\r\n}\r\n\r\nthead{\r\n    font-weight: 400;\r\n    border-bottom: 1.2px solid rgb(179, 179, 184) ;\r\n    background-color: rgb(10, 15, 80);\r\n    color: white;\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vb2ZmZXIvb2ZmZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7SUFDWCxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixrQkFBa0I7QUFDdEI7O0FBR0E7SUFDSSxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixtQkFBbUI7QUFDdkI7O0FBR0E7SUFDSSxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixtQkFBbUI7QUFDdkI7O0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsOENBQThDO0lBQzlDLGlDQUFpQztJQUNqQyxZQUFZOztBQUVoQiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL29mZmVyL29mZmVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjb2ZmZXItdGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1zcGFjaW5nOjA7XHJcbn1cclxuXHJcbi50YWJsZS1xdHl7XHJcbiAgICB3aWR0aDogMTUlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG59XHJcblxyXG5cclxuLnRhYmxlLWl0ZW17XHJcbiAgICB3aWR0aDogNjAlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcbi50YWJsZS1hbW91bnR7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxufVxyXG50aGVhZHtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBib3JkZXItYm90dG9tOiAxLjJweCBzb2xpZCByZ2IoMTc5LCAxNzksIDE4NCkgO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDEwLCAxNSwgODApO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG5cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/admin/offer/offer.component.html":
/*!**************************************************!*\
  !*** ./src/app/admin/offer/offer.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table id=\"offer-table\">\n  <thead>\n    <tr>\n      <td class=\"table-item\">item</td>\n      <td class=\"table-qty\">qty</td>\n      <td class=\"table-amount\">amount</td>\n    </tr>\n  </thead>\n  <tbody *ngIf=\"offers\">\n    <tr *ngFor=\"let offer of offers.products\">\n      <td class=\"table-item\">{{offer.title}}</td>\n      <td class=\"table-qty\">{{offer.quantity}}</td>\n      <td class=\"table-amount\">{{offer.offer}}</td>\n    </tr>\n  </tbody>\n</table>\n<div *ngIf=\"!offers;\" style=\"color: lightgrey; text-align:center;margin-top: 20px;width:100%\"><span>you have no offers</span></div>\n"

/***/ }),

/***/ "./src/app/admin/offer/offer.component.ts":
/*!************************************************!*\
  !*** ./src/app/admin/offer/offer.component.ts ***!
  \************************************************/
/*! exports provided: OfferComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfferComponent", function() { return OfferComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/common.service */ "./src/app/admin/services/common.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let OfferComponent = class OfferComponent {
    constructor(common) {
        this.common = common;
        this.uri = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].dbUrl;
    }
    ngOnInit() {
        this.offers = this.common.getShopOffers()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(({ data }) => { return data.shopOffers; }));
    }
};
OfferComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-offer',
        template: __webpack_require__(/*! ./offer.component.html */ "./src/app/admin/offer/offer.component.html"),
        styles: [__webpack_require__(/*! ./offer.component.css */ "./src/app/admin/offer/offer.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
], OfferComponent);



/***/ }),

/***/ "./src/app/admin/order/order.component.css":
/*!*************************************************!*\
  !*** ./src/app/admin/order/order.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".order-nav{\r\n    margin: 0; \r\n    padding:0;\r\n    position: fixed;\r\n    top: 55px;\r\n    z-index: 5500;\r\n    color: white;\r\n}\r\n\r\n.order-nav li{\r\n    display: inline-block;\r\n    margin: 0 10px;\r\n\r\n}\r\n\r\n.table-order-number{\r\n    width: 25%;\r\n}\r\n\r\n.table-status{\r\n    widows: 15%;\r\n}\r\n\r\n.table-item{\r\n    width: 60%;\r\n}\r\n\r\n.table-item, .table-status, .table-order-number{\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n}\r\n\r\nthead{\r\n    font-weight: 400;\r\n    border-bottom: 1.2px solid rgb(179, 179, 184) ;\r\n    background-color: rgb(10, 15, 80);\r\n    color: white;\r\n\r\n}\r\n\r\ntable{\r\n    width:100%;\r\n    border-spacing: 0;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vb3JkZXIvb3JkZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFNBQVM7SUFDVCxTQUFTO0lBQ1QsZUFBZTtJQUNmLFNBQVM7SUFDVCxhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixjQUFjOztBQUVsQjs7QUFFQTtJQUNJLFVBQVU7QUFDZDs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFVBQVU7QUFDZDs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQix1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsOENBQThDO0lBQzlDLGlDQUFpQztJQUNqQyxZQUFZOztBQUVoQjs7QUFFQTtJQUNJLFVBQVU7SUFDVixpQkFBaUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi9vcmRlci9vcmRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm9yZGVyLW5hdntcclxuICAgIG1hcmdpbjogMDsgXHJcbiAgICBwYWRkaW5nOjA7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB0b3A6IDU1cHg7XHJcbiAgICB6LWluZGV4OiA1NTAwO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4ub3JkZXItbmF2IGxpe1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOiAwIDEwcHg7XHJcblxyXG59XHJcblxyXG4udGFibGUtb3JkZXItbnVtYmVye1xyXG4gICAgd2lkdGg6IDI1JTtcclxufVxyXG5cclxuLnRhYmxlLXN0YXR1c3tcclxuICAgIHdpZG93czogMTUlO1xyXG59XHJcblxyXG4udGFibGUtaXRlbXtcclxuICAgIHdpZHRoOiA2MCU7XHJcbn1cclxuXHJcbi50YWJsZS1pdGVtLCAudGFibGUtc3RhdHVzLCAudGFibGUtb3JkZXItbnVtYmVye1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG59XHJcblxyXG50aGVhZHtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBib3JkZXItYm90dG9tOiAxLjJweCBzb2xpZCByZ2IoMTc5LCAxNzksIDE4NCkgO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDEwLCAxNSwgODApO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG5cclxufVxyXG5cclxudGFibGV7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYm9yZGVyLXNwYWNpbmc6IDA7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/admin/order/order.component.html":
/*!**************************************************!*\
  !*** ./src/app/admin/order/order.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table\">\n  <thead>\n    <tr>\n      <td class=\"table-order-number\">#</td>\n      <td class=\"table-item\">location</td>\n      <td class=\"table-status\">status</td>\n    </tr>\n  </thead>\n    <tbody *ngIf=\"orders;\">\n      <tr *ngFor=\"let order of orders; index as i\">\n        <td class=\"table-order-number\">{{i+1}}</td>\n        <td class=\"table-item\">{{order.billing_address.city}}, &nbsp; {{order.billing_address.street}}</td>\n        <td class=\"table-status\">{{order.status}}</td>\n    </tbody>\n</table>\n<div *ngIf=\"!orders;\" style=\"text-align: center; color:lightgrey;margin-top: 20px;width:100%\"><span>you have no orders</span></div>\n"

/***/ }),

/***/ "./src/app/admin/order/order.component.ts":
/*!************************************************!*\
  !*** ./src/app/admin/order/order.component.ts ***!
  \************************************************/
/*! exports provided: OrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderComponent", function() { return OrderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/common.service */ "./src/app/admin/services/common.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let OrderComponent = class OrderComponent {
    constructor(common) {
        this.common = common;
        this.uri = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].dbUrl;
    }
    ngOnInit() {
        this.orders = this.common.getShopOrders()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(({ data, loading }) => {
            return data.shopOffers;
        }));
    }
};
OrderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order',
        template: __webpack_require__(/*! ./order.component.html */ "./src/app/admin/order/order.component.html"),
        styles: [__webpack_require__(/*! ./order.component.css */ "./src/app/admin/order/order.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
], OrderComponent);



/***/ })

}]);
//# sourceMappingURL=src-app-admin-admin-module.js.map