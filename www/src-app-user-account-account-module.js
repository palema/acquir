(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-user-account-account-module"],{

/***/ "./src/app/user/account/account-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/user/account/account-routing.module.ts ***!
  \********************************************************/
/*! exports provided: AccountRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountRoutingModule", function() { return AccountRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/user/account/profile/profile.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/user/account/settings/settings.component.ts");
/* harmony import */ var _finance_menu_finance_menu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./finance-menu/finance-menu.component */ "./src/app/user/account/finance-menu/finance-menu.component.ts");






const routes = [
    { path: '', children: [
            { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_3__["ProfileComponent"] },
            { path: 'settings', component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_4__["SettingsComponent"] },
            { path: 'finance', component: _finance_menu_finance_menu_component__WEBPACK_IMPORTED_MODULE_5__["FinanceMenuComponent"] },
            { path: '', redirectTo: 'profile', pathMatch: 'full' }
        ] }
];
let AccountRoutingModule = class AccountRoutingModule {
};
AccountRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AccountRoutingModule);



/***/ }),

/***/ "./src/app/user/account/account.module.ts":
/*!************************************************!*\
  !*** ./src/app/user/account/account.module.ts ***!
  \************************************************/
/*! exports provided: AccountModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountModule", function() { return AccountModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _account_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account-routing.module */ "./src/app/user/account/account-routing.module.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/user/account/profile/profile.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/user/account/settings/settings.component.ts");
/* harmony import */ var _finance_menu_finance_menu_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./finance-menu/finance-menu.component */ "./src/app/user/account/finance-menu/finance-menu.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");








let AccountModule = class AccountModule {
};
AccountModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _profile_profile_component__WEBPACK_IMPORTED_MODULE_4__["ProfileComponent"], _settings_settings_component__WEBPACK_IMPORTED_MODULE_5__["SettingsComponent"], _finance_menu_finance_menu_component__WEBPACK_IMPORTED_MODULE_6__["FinanceMenuComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _account_routing_module__WEBPACK_IMPORTED_MODULE_3__["AccountRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"]
        ]
    })
], AccountModule);



/***/ }),

/***/ "./src/app/user/account/finance-menu/finance-menu.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/user/account/finance-menu/finance-menu.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".list-item{\r\n    position: relative;\r\n    margin: 10px 0;\r\n}\r\n.list-item input{\r\n    border: none;\r\n    border-bottom : 1px solid gainsboro;\r\n    background-color: inherit\r\n}\r\n.list-right{\r\n    position: absolute;\r\n    right: 0;\r\n    width: 120px;\r\n    height: 20px;\r\n}\r\n.section{\r\n    background-color: whitesmoke;\r\n    margin-bottom: 10px;\r\n    padding-bottom: 10px;\r\n}\r\ninput{\r\n    outline: none;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9hY2NvdW50L2ZpbmFuY2UtbWVudS9maW5hbmNlLW1lbnUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtJQUNsQixjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osbUNBQW1DO0lBQ25DO0FBQ0o7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsWUFBWTtJQUNaLFlBQVk7QUFDaEI7QUFFQTtJQUNJLDRCQUE0QjtJQUM1QixtQkFBbUI7SUFDbkIsb0JBQW9CO0FBQ3hCO0FBRUE7SUFDSSxhQUFhO0FBQ2pCIiwiZmlsZSI6InNyYy9hcHAvdXNlci9hY2NvdW50L2ZpbmFuY2UtbWVudS9maW5hbmNlLW1lbnUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5saXN0LWl0ZW17XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW46IDEwcHggMDtcclxufVxyXG4ubGlzdC1pdGVtIGlucHV0e1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYm9yZGVyLWJvdHRvbSA6IDFweCBzb2xpZCBnYWluc2Jvcm87XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBpbmhlcml0XHJcbn1cclxuXHJcbi5saXN0LXJpZ2h0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi5zZWN0aW9ue1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuaW5wdXR7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/user/account/finance-menu/finance-menu.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/user/account/finance-menu/finance-menu.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id='userBalanceWindow'>\n  <ul>\n    <li class=\"section\">\n      <div class=\"list-item\" style=\"margin: 0\">\n          <span><span class=\"fa fa-credit-card\"></span>balance</span>\n          <span class=\"list-right\" style=\"text-align: right\"><span style=\"font-size: 11px\">N$</span>400</span>\n      </div>\n    </li>\n    <li class=\"section\"><span\n        class=\"fa fa-exchange\"></span>transfer\n      <div class=\"list-item\">\n        <span>to :</span><input class=\"list-right\" type=\"text\">\n      </div>\n      <div class=\"list-item\">\n          <span>amount :</span><input class=\"list-right\"  type=\"text\">\n      </div>\n      <div class=\"list-item\">\n          <span>password :</span> <input class=\"list-right\"  type=\"text\">\n      </div>\n      <div class=\"list-item\" style=\" margin-top: 15px; margin-bottom: 0;\">\n          <button style=\"width: 100%; bottom:0; height: 25px; font-size: 14px; line-height: 0\">proceed</button>\n        </div>\n    </li>\n    <li class=\"section\">\n        <span class=\"fa fa-long-arrow-down\"></span><span>deposit</span>\n        <div class=\"list-item\">\n            <span>card no. :</span> <input class=\"list-right\"  type=\"text\">\n        </div>\n        <div class=\"list-item\">\n            <span>password :</span> <input class=\"list-right\"  type=\"text\">\n        </div>\n\n        <div class=\"list-item\" style=\" margin-top: 15px; margin-bottom: 0;\">\n          <button style=\"width: 100%; height: 25px; font-size: 14px; line-height: 0\">proceed</button>\n        </div>\n\n    </li>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/user/account/finance-menu/finance-menu.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/user/account/finance-menu/finance-menu.component.ts ***!
  \*********************************************************************/
/*! exports provided: FinanceMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceMenuComponent", function() { return FinanceMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FinanceMenuComponent = class FinanceMenuComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
], FinanceMenuComponent.prototype, "show", void 0);
FinanceMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'finance-menu',
        template: __webpack_require__(/*! ./finance-menu.component.html */ "./src/app/user/account/finance-menu/finance-menu.component.html"),
        styles: [__webpack_require__(/*! ./finance-menu.component.css */ "./src/app/user/account/finance-menu/finance-menu.component.css")]
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FinanceMenuComponent);



/***/ }),

/***/ "./src/app/user/account/profile/profile.component.css":
/*!************************************************************!*\
  !*** ./src/app/user/account/profile/profile.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#general-info-wrapper{\r\n    background-color: whitesmoke;\r\n}\r\n\r\n#general-info-list{\r\n    padding: 0 10px;\r\n}\r\n\r\n#general-info-list li{\r\n display: inline-block;\r\n font-size: 13px;\r\n font-weight: 300;\r\n width: 50%;\r\n padding-right: 8px;\r\n margin-bottom: 14px;\r\n}\r\n\r\n.fa-plus-square-o{\r\n    padding : 5px;\r\n    border-radius: 3px;\r\n}\r\n\r\n.fa-plus-square-o:hover, .fa-plus-square-o:active{\r\n    background-color: rgb(221, 221, 221);\r\n    transition: 2.s linear\r\n}\r\n\r\n#general-info-list li input{\r\n    width: 100%;\r\n    border: none;\r\n    outline: none;\r\n    background-color: inherit;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9hY2NvdW50L3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksNEJBQTRCO0FBQ2hDOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtDQUNDLHFCQUFxQjtDQUNyQixlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLFVBQVU7Q0FDVixrQkFBa0I7Q0FDbEIsbUJBQW1CO0FBQ3BCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLG9DQUFvQztJQUNwQztBQUNKOztBQUVBO0lBQ0ksV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2IseUJBQXlCO0lBQ3pCLGdCQUFnQjtJQUNoQix1QkFBdUI7QUFDM0IiLCJmaWxlIjoic3JjL2FwcC91c2VyL2FjY291bnQvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjZ2VuZXJhbC1pbmZvLXdyYXBwZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZXNtb2tlO1xyXG59XHJcblxyXG4jZ2VuZXJhbC1pbmZvLWxpc3R7XHJcbiAgICBwYWRkaW5nOiAwIDEwcHg7XHJcbn1cclxuXHJcbiNnZW5lcmFsLWluZm8tbGlzdCBsaXtcclxuIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuIGZvbnQtc2l6ZTogMTNweDtcclxuIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiB3aWR0aDogNTAlO1xyXG4gcGFkZGluZy1yaWdodDogOHB4O1xyXG4gbWFyZ2luLWJvdHRvbTogMTRweDtcclxufVxyXG5cclxuLmZhLXBsdXMtc3F1YXJlLW97XHJcbiAgICBwYWRkaW5nIDogNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG59XHJcblxyXG4uZmEtcGx1cy1zcXVhcmUtbzpob3ZlciwgLmZhLXBsdXMtc3F1YXJlLW86YWN0aXZle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIyMSwgMjIxLCAyMjEpO1xyXG4gICAgdHJhbnNpdGlvbjogMi5zIGxpbmVhclxyXG59XHJcblxyXG4jZ2VuZXJhbC1pbmZvLWxpc3QgbGkgaW5wdXR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBpbmhlcml0O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/user/account/profile/profile.component.html":
/*!*************************************************************!*\
  !*** ./src/app/user/account/profile/profile.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"general-info-wrapper\">\n        <div><span class=\"fa fa-info\" style=\"margin-right: 4px;\"></span>general</div>\n        <ul id=\"general-info-list\">\n                <li>\n                        <div>username</div>\n                        <input type=\"text\" readonly style=\"font-size: 13px; font-weight: 350\"\n                                [value]=\"username?username:'unknown'\">\n                </li>\n                <li>\n                        <div>lastname</div>\n                        <input type=\"text\" readonly style=\"font-size: 13px; font-weight: 350\"\n                                [value]=\"lastname?lastname: 'unknown'\">\n                </li>\n                <li>\n                        <div>firstname</div>\n                        <input type=\"text\" style=\"font-size: 13px; font-weight: 350\"\n                                [value]=\"firstname?firstname: 'unknown'\">\n                </li>\n                <li>\n                        <div>gender</div>\n                        <input type=\"text\" style=\"font-size: 13px; font-weight: 350\" [value]=\"gender?gender: 'unknown'\">\n                </li>\n                <li>\n                        <div>age</div>\n                        <input type=\"text\" style=\"font-size: 13px; font-weight: 350\" [value]=\"age?age: 'unknown'\">\n                </li>\n                <li>\n                        <div>height</div>\n                        <input type=\"text\" style=\"font-size: 13px; font-weight: 350\"\n                                [value]=\"height? height: 'unknown'\">\n                </li>\n                <li>\n                        <div>email</div>\n                        <input type=\"text\" style=\"font-size: 13px; font-weight: 350\" [value]=\"email? email: 'unknown'\">\n                </li>\n        </ul>\n</div>\n\n<div style=\"position: relative\">\n        <span class=\"fa fa-puzzle-piece\"></span>preferences\n        <span style=\"position: absolute; right : 5px; font-size : 13px\">empty\n                <span class=\"fa fa-plus-square-o\"></span></span>\n</div>\n<div style=\"position: relative\">\n        <span class=\"fa fa-bell-o\"></span>notifications\n        <span style=\"position: absolute; right : 5px; font-size : 13px\">empty\n                <span class=\"fa fa-plus-square-o\"></span></span>\n</div>"

/***/ }),

/***/ "./src/app/user/account/profile/profile.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/user/account/profile/profile.component.ts ***!
  \***********************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let ProfileComponent = class ProfileComponent {
    constructor(http) {
        this.http = http;
    }
    ngOnInit() {
        this.fetch_user_data();
    }
    fetch_user_data() {
        this.http.get('http://localhost:4000/user/info/get', { withCredentials: true }).subscribe((rs) => {
            this.user_info = rs;
        });
    }
    get username() {
        return this.user_info ? this.user_info.username : '';
    }
    get lastname() {
        return this.user_info ? this.user_info.lastname : '';
    }
    get firstname() {
        return this.user_info ? this.user_info.firstname : '';
    }
    get email() {
        return this.user_info ? this.user_info.email : '';
    }
    get age() {
        return this.user_info ? this.user_info.age : '';
    }
    get height() {
        return this.user_info ? this.user_info.height : '';
    }
    get gender() {
        return this.user_info ? this.user_info.gender : '';
    }
};
ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__(/*! ./profile.component.html */ "./src/app/user/account/profile/profile.component.html"),
        styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/user/account/profile/profile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], ProfileComponent);



/***/ }),

/***/ "./src/app/user/account/settings/settings.component.css":
/*!**************************************************************!*\
  !*** ./src/app/user/account/settings/settings.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvYWNjb3VudC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/user/account/settings/settings.component.html":
/*!***************************************************************!*\
  !*** ./src/app/user/account/settings/settings.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"position: relative\">\n  <span class=\"fa fa-ban\"></span>blacklist\n  <span style=\"position: absolute; right : 5px; font-size : 13px; font-weight: 300\">empty\n    <span class=\"fa fa-plus-square-o\"></span></span>\n</div>\n<div style=\"position: relative\">\n  <span class=\"fa fa-check-circle-o\"></span>whitelist\n  <span style=\"position: absolute; right : 5px; font-size : 13px; font-weight: 300\">empty\n    <span class=\"fa fa-plus-square-o\"></span></span>\n</div>"

/***/ }),

/***/ "./src/app/user/account/settings/settings.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/user/account/settings/settings.component.ts ***!
  \*************************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SettingsComponent = class SettingsComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
], SettingsComponent.prototype, "show", void 0);
SettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-settings',
        template: __webpack_require__(/*! ./settings.component.html */ "./src/app/user/account/settings/settings.component.html"),
        styles: [__webpack_require__(/*! ./settings.component.css */ "./src/app/user/account/settings/settings.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SettingsComponent);



/***/ })

}]);
//# sourceMappingURL=src-app-user-account-account-module.js.map