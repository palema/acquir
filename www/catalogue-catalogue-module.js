(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["catalogue-catalogue-module"],{

/***/ "./src/app/admin/catalogue/add/add.component.css":
/*!*******************************************************!*\
  !*** ./src/app/admin/catalogue/add/add.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NhdGFsb2d1ZS9hZGQvYWRkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/catalogue/add/add.component.html":
/*!********************************************************!*\
  !*** ./src/app/admin/catalogue/add/add.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-form>\n\n  <!-- <div style=\"float: left; width:50%; padding-right: 5px;\" class=\"info-item\" >\n    <input type=\"number\" placeholder=\"price\" class=\"price-input\" style=\"width: 100%;\" >\n  </div>\n  <div style=\"float: left; width:50%; padding-left: 5px;\" class=\"info-item\">\n    <input type=\"text\" placeholder=\"qty\" class=\"brand-input\" style=\"width: 100%;\">\n  </div> -->\n</app-form>"

/***/ }),

/***/ "./src/app/admin/catalogue/add/add.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin/catalogue/add/add.component.ts ***!
  \******************************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AddComponent = class AddComponent {
    constructor() {
    }
    ngOnInit() { }
    submit_form(form) {
        console.log('dss');
    }
};
AddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add',
        template: __webpack_require__(/*! ./add.component.html */ "./src/app/admin/catalogue/add/add.component.html"),
        styles: [__webpack_require__(/*! ./add.component.css */ "./src/app/admin/catalogue/add/add.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AddComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/angle-image/angle-image.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/admin/catalogue/angle-image/angle-image.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fa-trash-o{\r\n    position: absolute;\r\n    right: 10px;\r\n    color: red;\r\n    z-index: 1000;\r\n}\r\n\r\n.fa-trash-o:hover{\r\n    cursor: pointer;\r\n}\r\n\r\n.image-header{\r\n    font-size: 13px;\r\n}\r\n\r\n.angle-image img{\r\n    background-color: whitesmoke;\r\n    height: 250px;\r\n    width: 200px;\r\n}\r\n\r\nimg{\r\n    height: inherit;\r\n    width: inherit;\r\n}\r\n\r\n#right-indicator:hover, #right-indicator:active, #left-indicator:hover, #left-indicator:active{\r\n    color: white;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL2FuZ2xlLWltYWdlL2FuZ2xlLWltYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFVBQVU7SUFDVixhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSw0QkFBNEI7SUFDNUIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsY0FBYztBQUNsQjs7QUFFQTtJQUNJLFlBQVk7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi9jYXRhbG9ndWUvYW5nbGUtaW1hZ2UvYW5nbGUtaW1hZ2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mYS10cmFzaC1ve1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgei1pbmRleDogMTAwMDtcclxufVxyXG5cclxuLmZhLXRyYXNoLW86aG92ZXJ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5pbWFnZS1oZWFkZXJ7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbi5hbmdsZS1pbWFnZSBpbWd7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZXNtb2tlO1xyXG4gICAgaGVpZ2h0OiAyNTBweDtcclxuICAgIHdpZHRoOiAyMDBweDtcclxufVxyXG5cclxuaW1ne1xyXG4gICAgaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgd2lkdGg6IGluaGVyaXQ7XHJcbn1cclxuXHJcbiNyaWdodC1pbmRpY2F0b3I6aG92ZXIsICNyaWdodC1pbmRpY2F0b3I6YWN0aXZlLCAjbGVmdC1pbmRpY2F0b3I6aG92ZXIsICNsZWZ0LWluZGljYXRvcjphY3RpdmV7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/admin/catalogue/angle-image/angle-image.component.html":
/*!************************************************************************!*\
  !*** ./src/app/admin/catalogue/angle-image/angle-image.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"angle_image_group\">\n    <div id=\"aImg-container\">\n        <h5 class=\"image-header\">Supporting Images</h5>\n          <div id=\"image-slide\" [formGroup]=\"image_group\" style=\"position: relative; height: 250px; background-color: whitesmoke;\">\n            <ul id=\"display-area\" formArrayName=\"angle_image\" style=\"height: 250px; overflow: hidden; white-space: nowrap;\" (click)=\"angle_images_input.click()\">\n             <div *ngIf=\"angle_img.controls.length>0; else select_img\">\n              <div class=\"angle-image-wrapper\" style=\"position: relative; display: inline-block;width: 100%;\" *ngFor=\"let image of angle_img.controls; index as i\" [formGroupName]=\"i\">\n                      <div id=\"delete-icon\" style=\"position: absolute; right: 0; top : 0; z-index: 20; padding: 3px;\" (click)=\"delete(i, $event)\">\n                          <div style=\"border-radius: 50px; background-color: gray; padding: 5px; font-size: 14px; box-shadow: 2px 1px 2px rgb(143, 143, 143);\" class=\"fa fa-trash-o\"></div>\n                        </div>\n                    <img src=\"{{image.controls.src.value}}\" class=\"angle-image\" style=\"max-width: 100%; max-height: 250px;\">\n                  </div>\n             </div>\n             <ng-template #select_img>\n              <div style=\"height: 100%; width: 100%\">\n                select image\n              </div>\n             </ng-template>\n            </ul>\n            <div id=\"left-indicator\" style=\"position: absolute; left: 0; top: 45%; z-index: 20; background-color: grey; padding: 3px;\" (click)=\"slide_prev()\">\n              <div class=\"fa fa-arrow-left\"></div>\n            </div>\n            <div id=\"right-indicator\" style=\"position: absolute; right: 0; top : 45%; z-index: 20; background-color: grey; padding: 3px;\" (click)=\"slide_next()\">\n              <div class=\"fa fa-arrow-right\"></div>\n            </div>\n  \n          </div>\n    </div>\n  </div>\n  \n  <input #angle_images_input type=\"file\" \n  multiple accept=\"image/*\" style=\"display:none\" (change)=\"file_upload($event)\">\n  "

/***/ }),

/***/ "./src/app/admin/catalogue/angle-image/angle-image.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/admin/catalogue/angle-image/angle-image.component.ts ***!
  \**********************************************************************/
/*! exports provided: AngleImageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngleImageComponent", function() { return AngleImageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let AngleImageComponent = class AngleImageComponent {
    constructor(fb) {
        this.fb = fb;
        this.position = 0;
        this.item_width = 0;
        this.area_width = 0;
        this.total_width = 0;
    }
    ngOnInit() {
        this.area_width = document.getElementById('display-area').clientWidth;
        this.item_width = document.getElementById('aImg-container').clientWidth;
    }
    slide_prev() {
        var files = document.getElementsByClassName('angle-image-wrapper');
        if (files && this.position < 0) {
            this.position += this.item_width;
            for (var i = 0; i < files.length; i++) {
                files[i].style.left = this.position + 'px';
            }
        }
    }
    slide_next() {
        var files = document.getElementsByClassName('angle-image-wrapper');
        this.total_width = this.item_width * files.length;
        if (files && (files.length * this.item_width) - this.area_width > 0 && this.total_width > -this.position + this.item_width) {
            this.position -= this.item_width;
            for (var i = 0; i < files.length; i++) {
                files[i].style.left = this.position + 'px';
            }
        }
    }
    get angle_img() {
        return this.image_group.controls['angle_image'];
    }
    file_upload(event) {
        var files = event.target.files;
        for (var file of files) {
            if (file.type.startsWith('image/')) {
                const reader = new FileReader();
                reader.onload = ((aImg) => {
                    return (e) => {
                        this.angle_img.push(this.fb.group({
                            name: [file.name],
                            src: [e.target.result],
                            file: [file]
                        }));
                        setTimeout(() => {
                            this.slide_next();
                        }, 500);
                    };
                })(file);
                reader.readAsDataURL(file);
            }
        }
    }
    delete(i, event) {
        event.stopPropagation();
        this.angle_img.removeAt(i);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('group'),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"])
], AngleImageComponent.prototype, "image_group", void 0);
AngleImageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-angle-image',
        template: __webpack_require__(/*! ./angle-image.component.html */ "./src/app/admin/catalogue/angle-image/angle-image.component.html"),
        styles: [__webpack_require__(/*! ./angle-image.component.css */ "./src/app/admin/catalogue/angle-image/angle-image.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], AngleImageComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/catalogue.component.css":
/*!*********************************************************!*\
  !*** ./src/app/admin/catalogue/catalogue.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".catalogue-nav{    \r\n    position: fixed;\r\n    top:55px;\r\n    z-index: 5500;\r\n    color:white;\r\n}\r\n\r\n.catalogue-nav li{\r\n    display: inline-block;\r\n    margin: 0 10px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL2NhdGFsb2d1ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZUFBZTtJQUNmLFFBQVE7SUFDUixhQUFhO0lBQ2IsV0FBVztBQUNmOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi9jYXRhbG9ndWUvY2F0YWxvZ3VlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2F0YWxvZ3VlLW5hdnsgICAgXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB0b3A6NTVweDtcclxuICAgIHotaW5kZXg6IDU1MDA7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxufVxyXG5cclxuLmNhdGFsb2d1ZS1uYXYgbGl7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBtYXJnaW46IDAgMTBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/admin/catalogue/catalogue.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/catalogue/catalogue.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/catalogue/catalogue.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/catalogue/catalogue.component.ts ***!
  \********************************************************/
/*! exports provided: CatalogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogueComponent", function() { return CatalogueComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CatalogueComponent = class CatalogueComponent {
    constructor() { }
    ngOnInit() {
    }
};
CatalogueComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-catalogue',
        template: __webpack_require__(/*! ./catalogue.component.html */ "./src/app/admin/catalogue/catalogue.component.html"),
        styles: [__webpack_require__(/*! ./catalogue.component.css */ "./src/app/admin/catalogue/catalogue.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], CatalogueComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/catalogue.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/admin/catalogue/catalogue.module.ts ***!
  \*****************************************************/
/*! exports provided: CatalogueModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogueModule", function() { return CatalogueModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _catalogue_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./catalogue.component */ "./src/app/admin/catalogue/catalogue.component.ts");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add/add.component */ "./src/app/admin/catalogue/add/add.component.ts");
/* harmony import */ var _specs_specs_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./specs/specs.component */ "./src/app/admin/catalogue/specs/specs.component.ts");
/* harmony import */ var _size_quantity_size_quantity_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./size-quantity/size-quantity.component */ "./src/app/admin/catalogue/size-quantity/size-quantity.component.ts");
/* harmony import */ var _view_view_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./view/view.component */ "./src/app/admin/catalogue/view/view.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/admin/catalogue/edit/edit.component.ts");
/* harmony import */ var _remove_remove_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./remove/remove.component */ "./src/app/admin/catalogue/remove/remove.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _catalogue_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./catalogue.routing.module */ "./src/app/admin/catalogue/catalogue.routing.module.ts");
/* harmony import */ var _department_department_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./department/department.component */ "./src/app/admin/catalogue/department/department.component.ts");
/* harmony import */ var _angle_image_angle_image_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./angle-image/angle-image.component */ "./src/app/admin/catalogue/angle-image/angle-image.component.ts");
/* harmony import */ var _form_form_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./form/form.component */ "./src/app/admin/catalogue/form/form.component.ts");
















let CatalogueModule = class CatalogueModule {
};
CatalogueModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _catalogue_component__WEBPACK_IMPORTED_MODULE_3__["CatalogueComponent"],
            _add_add_component__WEBPACK_IMPORTED_MODULE_4__["AddComponent"],
            _size_quantity_size_quantity_component__WEBPACK_IMPORTED_MODULE_6__["SizeQuantityComponent"],
            _specs_specs_component__WEBPACK_IMPORTED_MODULE_5__["SpecsComponent"],
            _view_view_component__WEBPACK_IMPORTED_MODULE_7__["ViewComponent"],
            _edit_edit_component__WEBPACK_IMPORTED_MODULE_8__["EditComponent"],
            _remove_remove_component__WEBPACK_IMPORTED_MODULE_9__["RemoveComponent"],
            _department_department_component__WEBPACK_IMPORTED_MODULE_13__["DepartmentComponent"],
            _angle_image_angle_image_component__WEBPACK_IMPORTED_MODULE_14__["AngleImageComponent"],
            _form_form_component__WEBPACK_IMPORTED_MODULE_15__["FormComponent"],
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"],
            _catalogue_routing_module__WEBPACK_IMPORTED_MODULE_12__["CatalogueRoutingModule"],
        ]
    })
], CatalogueModule);



/***/ }),

/***/ "./src/app/admin/catalogue/catalogue.routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/catalogue/catalogue.routing.module.ts ***!
  \*************************************************************/
/*! exports provided: CatalogueRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogueRoutingModule", function() { return CatalogueRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _catalogue_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./catalogue.component */ "./src/app/admin/catalogue/catalogue.component.ts");
/* harmony import */ var _view_view_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./view/view.component */ "./src/app/admin/catalogue/view/view.component.ts");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add/add.component */ "./src/app/admin/catalogue/add/add.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/admin/catalogue/edit/edit.component.ts");
/* harmony import */ var _remove_remove_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./remove/remove.component */ "./src/app/admin/catalogue/remove/remove.component.ts");








const routes = [
    { path: '', component: _catalogue_component__WEBPACK_IMPORTED_MODULE_3__["CatalogueComponent"], children: [
            { path: 'view', component: _view_view_component__WEBPACK_IMPORTED_MODULE_4__["ViewComponent"] },
            { path: 'add', component: _add_add_component__WEBPACK_IMPORTED_MODULE_5__["AddComponent"] },
            { path: 'edit', component: _edit_edit_component__WEBPACK_IMPORTED_MODULE_6__["EditComponent"] },
            { path: 'remove', component: _remove_remove_component__WEBPACK_IMPORTED_MODULE_7__["RemoveComponent"] },
            { path: '', redirectTo: 'view', pathMatch: 'full' },
        ] },
];
let CatalogueRoutingModule = class CatalogueRoutingModule {
};
CatalogueRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], CatalogueRoutingModule);



/***/ }),

/***/ "./src/app/admin/catalogue/department/department.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/admin/catalogue/department/department.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NhdGFsb2d1ZS9kZXBhcnRtZW50L2RlcGFydG1lbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/catalogue/department/department.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/admin/catalogue/department/department.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [formGroup]=\"department_group\">\n  <div formArrayName=\"department\" class=\"form-element\">\n    <div class=\"department-header\">\n      <span style=\"font-size: 13px;\">Departments</span>\n    </div>\n    <span *ngFor=\"let department of department.value; index as z\" [formGroupName]=\"z\"\n      style=\"font-size: 12px; padding: 4px; border-radius: 10px; background-color: gainsboro;\">\n      <input \n        class=\"department-input\" \n        [style.width]=\"department.name.length+'ch'\"\n        style=\"\n          border:none; \n          background-color:inherit; \n          margin: 5px;\" \n        readonly\n        type=\"text\" \n        [value]=\"department.name\" \n        formControlName=\"name\">\n      <span class=\"fa fa-trash-o\" style=\"padding: 3px; background-color: gray; border-radius: 100%;\" (click)=\"remove_department(z)\"></span>\n    </span>\n  </div>\n</div>\n<div id=\"department-space\" style=\"margin-top: 20px;\">\n  <div>\n    <span \n      *ngFor=\"let d of dbDepartments | async\" \n      (click)=\"add_department(d.name)\" \n      style=\"\n        font-size: 12px; \n        padding: 4px; \n        border-radius: 10px; \n        background-color: gainsboro;\n        margin-bottom: 5px;\">\n      {{d.name}}\n    </span>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/catalogue/department/department.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/admin/catalogue/department/department.component.ts ***!
  \********************************************************************/
/*! exports provided: DepartmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepartmentComponent", function() { return DepartmentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/admin/services/common.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let DepartmentComponent = class DepartmentComponent {
    constructor(fb, common) {
        this.fb = fb;
        this.common = common;
    }
    ngOnInit() {
        this.departmentTree = new DepartmentNode(this.common, null);
        this.getDepartment();
    }
    get department() {
        return this.department_group.controls['department'];
    }
    add_department(name) {
        this.department.push(this.department_template(name));
        //get subdepartments
    }
    department_template(name) {
        return this.fb.group({
            name: [name, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([])],
        });
    }
    remove_department(index) {
        this.department.removeAt(index);
    }
    getDepartment() {
        this.dbDepartments = this.common.getRootDepartment()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(({ data }) => {
            for (var i = 0; i < data.rootDepartment.length; i++) {
                console.log(data.rootDepartment[i].name, 'list');
                this.departmentTree.addSubdepartment = new DepartmentNode(this.common, data.rootDepartment[i].name);
            }
            return data.rootDepartment;
        }));
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('group'),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"])
], DepartmentComponent.prototype, "department_group", void 0);
DepartmentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-department',
        template: __webpack_require__(/*! ./department.component.html */ "./src/app/admin/catalogue/department/department.component.html"),
        styles: [__webpack_require__(/*! ./department.component.css */ "./src/app/admin/catalogue/department/department.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
], DepartmentComponent);

//department data structure to hold users choice of product departments
class DepartmentNode {
    constructor(common, departmentName) {
        this.common = common;
        this.departmentName = departmentName;
        this.subDepartments = [];
    }
    get name() {
        return this.departmentName;
    }
    get subDeps() {
        if (this.subDepartments == []) {
            this.common.getRootDepartment()
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(({ data }) => {
                return data.rootDepartment;
            }));
        }
        else
            return this.subDepartments;
    }
    set addSubdepartment(department) {
        //this.subDepartments.push("department")
    }
}


/***/ }),

/***/ "./src/app/admin/catalogue/edit/edit.component.css":
/*!*********************************************************!*\
  !*** ./src/app/admin/catalogue/edit/edit.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-grid{\r\n    display: grid;\r\n    grid-template-columns: 30px 1fr 250px;\r\n    grid-template-rows: 90px 1fr;\r\n    grid-template-areas: \". . .\"\r\n                         \". main-content-area .\"\r\n}\r\n\r\n.main-content-area{\r\n    grid-area : main-content-area;\r\n}\r\n\r\n.product-grid{\r\n    display: grid;\r\n    grid-template-columns: 1fr 1fr;\r\n    grid-template-rows: 50px 1fr 1fr;\r\n    grid-template-areas: \"general-info general-info\"\r\n                         \"description files\"\r\n                         \"specs department\"\r\n}\r\n\r\n.general-info{\r\n    grid-area: general-info;\r\n}\r\n\r\n.specs{\r\n    grid-area: specs;\r\n}\r\n\r\n.size-quantity{\r\n    position: relative;\r\n    grid-area: size-quantity;\r\n}\r\n\r\n.department{\r\n    grid-area: department;\r\n}\r\n\r\n.files{\r\n    grid-area: files;\r\n}\r\n\r\n.description{\r\n    grid-area: description;\r\n}\r\n\r\n.display-image-wrapper, .angle-images-wrapper{\r\n    width: 49%;\r\n    height: 250px;\r\n    float: left;\r\n}\r\n\r\nimg{\r\n    width: 100%;\r\n    height: 200px;\r\n}\r\n\r\ntextarea{\r\n    width: 90%;\r\n    height: 60%;\r\n}\r\n\r\n.general-info input{\r\n    margin-right: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL2VkaXQvZWRpdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtJQUNiLHFDQUFxQztJQUNyQyw0QkFBNEI7SUFDNUI7O0FBRUo7O0FBRUE7SUFDSSw2QkFBNkI7QUFDakM7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsOEJBQThCO0lBQzlCLGdDQUFnQztJQUNoQzs7O0FBR0o7O0FBRUE7SUFDSSx1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsd0JBQXdCO0FBQzVCOztBQUVBO0lBQ0kscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0ksVUFBVTtJQUNWLGFBQWE7SUFDYixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsYUFBYTtBQUNqQjs7QUFDQTtJQUNJLFVBQVU7SUFDVixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi9jYXRhbG9ndWUvZWRpdC9lZGl0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1ncmlke1xyXG4gICAgZGlzcGxheTogZ3JpZDtcclxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMzBweCAxZnIgMjUwcHg7XHJcbiAgICBncmlkLXRlbXBsYXRlLXJvd3M6IDkwcHggMWZyO1xyXG4gICAgZ3JpZC10ZW1wbGF0ZS1hcmVhczogXCIuIC4gLlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBcIi4gbWFpbi1jb250ZW50LWFyZWEgLlwiXHJcbn1cclxuXHJcbi5tYWluLWNvbnRlbnQtYXJlYXtcclxuICAgIGdyaWQtYXJlYSA6IG1haW4tY29udGVudC1hcmVhO1xyXG59XHJcblxyXG4ucHJvZHVjdC1ncmlke1xyXG4gICAgZGlzcGxheTogZ3JpZDtcclxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMWZyIDFmcjtcclxuICAgIGdyaWQtdGVtcGxhdGUtcm93czogNTBweCAxZnIgMWZyO1xyXG4gICAgZ3JpZC10ZW1wbGF0ZS1hcmVhczogXCJnZW5lcmFsLWluZm8gZ2VuZXJhbC1pbmZvXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGVzY3JpcHRpb24gZmlsZXNcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgXCJzcGVjcyBkZXBhcnRtZW50XCJcclxufVxyXG5cclxuLmdlbmVyYWwtaW5mb3tcclxuICAgIGdyaWQtYXJlYTogZ2VuZXJhbC1pbmZvO1xyXG59XHJcblxyXG4uc3BlY3N7XHJcbiAgICBncmlkLWFyZWE6IHNwZWNzO1xyXG59XHJcblxyXG4uc2l6ZS1xdWFudGl0eXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGdyaWQtYXJlYTogc2l6ZS1xdWFudGl0eTtcclxufVxyXG5cclxuLmRlcGFydG1lbnR7XHJcbiAgICBncmlkLWFyZWE6IGRlcGFydG1lbnQ7XHJcbn1cclxuXHJcbi5maWxlc3tcclxuICAgIGdyaWQtYXJlYTogZmlsZXM7XHJcbn0gXHJcblxyXG4uZGVzY3JpcHRpb257XHJcbiAgICBncmlkLWFyZWE6IGRlc2NyaXB0aW9uO1xyXG59XHJcblxyXG4uZGlzcGxheS1pbWFnZS13cmFwcGVyLCAuYW5nbGUtaW1hZ2VzLXdyYXBwZXJ7XHJcbiAgICB3aWR0aDogNDklO1xyXG4gICAgaGVpZ2h0OiAyNTBweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG5pbWd7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMjAwcHg7XHJcbn1cclxudGV4dGFyZWF7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgaGVpZ2h0OiA2MCU7XHJcbn1cclxuXHJcbi5nZW5lcmFsLWluZm8gaW5wdXR7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/admin/catalogue/edit/edit.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/catalogue/edit/edit.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-form>\n  <!-- <input class=\"submit-btn\" type=\"submit\" style=\"width: 100%; margin-top: 15px; box-shadow: none;\" value=\"save changes\"> -->\n</app-form>"

/***/ }),

/***/ "./src/app/admin/catalogue/edit/edit.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/catalogue/edit/edit.component.ts ***!
  \********************************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let EditComponent = class EditComponent {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
    }
};
EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit',
        template: __webpack_require__(/*! ./edit.component.html */ "./src/app/admin/catalogue/edit/edit.component.html"),
        styles: [__webpack_require__(/*! ./edit.component.css */ "./src/app/admin/catalogue/edit/edit.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], EditComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/form/form.component.css":
/*!*********************************************************!*\
  !*** ./src/app/admin/catalogue/form/form.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#image-display-area{\r\n    background-color: whitesmoke;\r\n    height:  250px;\r\n}\r\n\r\n.image-header{\r\n    font-size: 13px;\r\n}\r\n\r\nform{\r\n    font-size: 12px;\r\n}\r\n\r\ninput{\r\n    border: none;\r\n    border-bottom: 1px solid lightgrey;\r\n\r\n}\r\n\r\n#display-image-wrapper, #angle-images-wrapper{\r\n    text-align: center;\r\n    width: 100%;\r\n    margin: 15px 0;\r\n}\r\n\r\n#price-wrapper, #title-wrapper, #quantity-wrapper, #description-wrapper{\r\n    float: left;\r\n    margin-bottom: 15px;\r\n}\r\n\r\n#description-wrapper, #description-inner{\r\n    width : 100%;\r\n}\r\n\r\n#price-input, #title-input, #quantity-input, #description-input{\r\n    width: 100%;\r\n}\r\n\r\n#description-inner{\r\n    width: 98%;\r\n}\r\n\r\n#title-wrapper{\r\n    width: 100%;\r\n}\r\n\r\n#price-wrapper, #quantity-wrapper{\r\n    width: 50%;\r\n}\r\n\r\n#price-inner{\r\n    margin-left: 5px;\r\n}\r\n\r\n#quantity-inner{\r\n    margin-right: 5px;\r\n}\r\n\r\n#features-wrapper, #department-wrapper{\r\n    margin-bottom: 15px;\r\n}\r\n\r\n#files-wrapper{\r\n    clear: both;\r\n    margin-bottom: 15px;\r\n}\r\n\r\n.error-msg{\r\n    color:red;\r\n    font-size: 12px;\r\n}\r\n\r\n.success-msg{\r\n    color:rgb(21, 209, 21);\r\n    font-size: 12px;\r\n}\r\n\r\n.submit-btn{\r\n    border-radius: 20px;\r\n}\r\n\r\n.submit-btn:active, .submit-btn:hover{\r\n    background-color: grey;\r\n    color : white;\r\n}\r\n\r\n@media screen and (min-width:900px){\r\n    #price-wrapper, #title-wrapper, #quantity-wrapper, #description-wrapper{\r\n        float: left;\r\n    }\r\n\r\n    #price-input, #title-input, #quantity-input{\r\n        width: 100%;\r\n    }\r\n\r\n    #title-inner, #quantity-inner, #price-inner{\r\n        margin-right: 5px;\r\n    }\r\n\r\n\r\n    #title-wrapper{\r\n        width: 30%;\r\n    }\r\n\r\n    #price-wrapper, #quantity-wrapper{\r\n        width: 15%;\r\n    }\r\n\r\n    #description-wrapper{\r\n        width : 40%;\r\n    }\r\n\r\n    #files-wrapper{\r\n        clear: both;\r\n    }\r\n\r\n    #display-image-wrapper, #angle-images-wrapper, \r\n    #features-wrapper, #department-wrapper{\r\n        float: left;\r\n        width: 50%;\r\n    }\r\n\r\n    #display-image-inner, #features-inner{\r\n        margin-right : 5px;\r\n    }\r\n\r\n    #angle-image-inner, #department-inner{\r\n        margin-left : 5px;\r\n    }\r\n\r\n\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL2Zvcm0vZm9ybS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksNEJBQTRCO0lBQzVCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLFlBQVk7SUFDWixrQ0FBa0M7O0FBRXRDOztBQUdBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksV0FBVztJQUNYLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxXQUFXO0FBQ2Y7O0FBR0E7SUFDSSxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxVQUFVO0FBQ2Q7O0FBR0E7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxtQkFBbUI7QUFDdkI7O0FBSUE7SUFDSSxXQUFXO0lBQ1gsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksU0FBUztJQUNULGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxzQkFBc0I7SUFDdEIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLHNCQUFzQjtJQUN0QixhQUFhO0FBQ2pCOztBQUVBO0lBQ0k7UUFDSSxXQUFXO0lBQ2Y7O0lBRUE7UUFDSSxXQUFXO0lBQ2Y7O0lBRUE7UUFDSSxpQkFBaUI7SUFDckI7OztJQUdBO1FBQ0ksVUFBVTtJQUNkOztJQUVBO1FBQ0ksVUFBVTtJQUNkOztJQUVBO1FBQ0ksV0FBVztJQUNmOztJQUVBO1FBQ0ksV0FBVztJQUNmOztJQUVBOztRQUVJLFdBQVc7UUFDWCxVQUFVO0lBQ2Q7O0lBRUE7UUFDSSxrQkFBa0I7SUFDdEI7O0lBRUE7UUFDSSxpQkFBaUI7SUFDckI7OztBQUdKIiwiZmlsZSI6InNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL2Zvcm0vZm9ybS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2ltYWdlLWRpc3BsYXktYXJlYXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlc21va2U7XHJcbiAgICBoZWlnaHQ6ICAyNTBweDtcclxufVxyXG5cclxuLmltYWdlLWhlYWRlcntcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuZm9ybXtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG5cclxuaW5wdXR7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmV5O1xyXG5cclxufVxyXG5cclxuXHJcbiNkaXNwbGF5LWltYWdlLXdyYXBwZXIsICNhbmdsZS1pbWFnZXMtd3JhcHBlcntcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luOiAxNXB4IDA7XHJcbn1cclxuXHJcbiNwcmljZS13cmFwcGVyLCAjdGl0bGUtd3JhcHBlciwgI3F1YW50aXR5LXdyYXBwZXIsICNkZXNjcmlwdGlvbi13cmFwcGVye1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG59XHJcblxyXG4jZGVzY3JpcHRpb24td3JhcHBlciwgI2Rlc2NyaXB0aW9uLWlubmVye1xyXG4gICAgd2lkdGggOiAxMDAlO1xyXG59XHJcblxyXG4jcHJpY2UtaW5wdXQsICN0aXRsZS1pbnB1dCwgI3F1YW50aXR5LWlucHV0LCAjZGVzY3JpcHRpb24taW5wdXR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbiNkZXNjcmlwdGlvbi1pbm5lcntcclxuICAgIHdpZHRoOiA5OCU7XHJcbn1cclxuXHJcbiN0aXRsZS13cmFwcGVye1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbiNwcmljZS13cmFwcGVyLCAjcXVhbnRpdHktd3JhcHBlcntcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxuXHJcblxyXG4jcHJpY2UtaW5uZXJ7XHJcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG59XHJcblxyXG4jcXVhbnRpdHktaW5uZXJ7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuI2ZlYXR1cmVzLXdyYXBwZXIsICNkZXBhcnRtZW50LXdyYXBwZXJ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG59XHJcblxyXG5cclxuXHJcbiNmaWxlcy13cmFwcGVye1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG59XHJcblxyXG4uZXJyb3ItbXNne1xyXG4gICAgY29sb3I6cmVkO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG4uc3VjY2Vzcy1tc2d7XHJcbiAgICBjb2xvcjpyZ2IoMjEsIDIwOSwgMjEpO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG4uc3VibWl0LWJ0bntcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbi5zdWJtaXQtYnRuOmFjdGl2ZSwgLnN1Ym1pdC1idG46aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5O1xyXG4gICAgY29sb3IgOiB3aGl0ZTtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDo5MDBweCl7XHJcbiAgICAjcHJpY2Utd3JhcHBlciwgI3RpdGxlLXdyYXBwZXIsICNxdWFudGl0eS13cmFwcGVyLCAjZGVzY3JpcHRpb24td3JhcHBlcntcclxuICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgIH1cclxuXHJcbiAgICAjcHJpY2UtaW5wdXQsICN0aXRsZS1pbnB1dCwgI3F1YW50aXR5LWlucHV0e1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgICN0aXRsZS1pbm5lciwgI3F1YW50aXR5LWlubmVyLCAjcHJpY2UtaW5uZXJ7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICB9XHJcblxyXG5cclxuICAgICN0aXRsZS13cmFwcGVye1xyXG4gICAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICB9XHJcblxyXG4gICAgI3ByaWNlLXdyYXBwZXIsICNxdWFudGl0eS13cmFwcGVye1xyXG4gICAgICAgIHdpZHRoOiAxNSU7XHJcbiAgICB9XHJcblxyXG4gICAgI2Rlc2NyaXB0aW9uLXdyYXBwZXJ7XHJcbiAgICAgICAgd2lkdGggOiA0MCU7XHJcbiAgICB9XHJcblxyXG4gICAgI2ZpbGVzLXdyYXBwZXJ7XHJcbiAgICAgICAgY2xlYXI6IGJvdGg7XHJcbiAgICB9XHJcblxyXG4gICAgI2Rpc3BsYXktaW1hZ2Utd3JhcHBlciwgI2FuZ2xlLWltYWdlcy13cmFwcGVyLCBcclxuICAgICNmZWF0dXJlcy13cmFwcGVyLCAjZGVwYXJ0bWVudC13cmFwcGVye1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICB9XHJcblxyXG4gICAgI2Rpc3BsYXktaW1hZ2UtaW5uZXIsICNmZWF0dXJlcy1pbm5lcntcclxuICAgICAgICBtYXJnaW4tcmlnaHQgOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2FuZ2xlLWltYWdlLWlubmVyLCAjZGVwYXJ0bWVudC1pbm5lcntcclxuICAgICAgICBtYXJnaW4tbGVmdCA6IDVweDtcclxuICAgIH1cclxuXHJcblxyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/admin/catalogue/form/form.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/catalogue/form/form.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"product_form\">\n  <div class=\"general-info\">\n    <div id=\"title-wrapper\">\n      <div id=\"title-inner\">\n        <input type=\"text\" placeholder=\"title\" id=\"title-input\" formControlName=\"title\">\n        <div class=\"error-msg\" *ngIf=\"title.touched && title.invalid\">*field is required</div>\n      </div>\n    </div>\n    <div id=\"quantity-wrapper\">\n      <div id=\"quantity-inner\">\n        <input type=\"number\" formControlName=\"quantity\" id=\"quantity-input\" placeholder=\"qty\">\n        <div class=\"error-msg\" *ngIf=\"quantity.touched && quantity.invalid\">*field is required</div>\n      </div>\n    </div>\n    <div id=\"price-wrapper\">\n      <div id=\"price-inner\">\n        <div>\n          <label>N$\n            <input style=\"width: 80%;\" type=\"number\" formControlName=\"list_price\" id=\"price-input\" placeholder=\"price\">\n          </label>\n        </div>\n        <div class=\"error-msg\" *ngIf=\"list_price.touched && list_price.invalid\">*field is required</div>\n      </div>\n    </div>\n    <ng-content select=\".info-item\"></ng-content>\n    <div id=\"description-wrapper\">\n      <div id=\"description-inner\">\n        <textarea placeholder=\"short description of product...\" id=\"description-input\" rows=\"4\"\n          formControlName=\"description\"></textarea>\n        <div class=\"error-msg\" *ngIf=\"description.touched && description.invalid\">*field is required</div>\n      </div>\n    </div>\n  </div>\n\n  <div id=\"files-wrapper\">\n    <input #display_image_input type=\"file\" accept=\"image/*\" style=\"display:none\" (change)=\"file_upload($event)\">\n    <div id=\"display-image-wrapper\">\n      <div id=\"display-image-inner\">\n        <h5 class=\"image-header\">Main Image</h5>\n        <div id=\"image-display-area\" (click)=\"display_image_input.click()\">\n          <div *ngIf=\"display_image.value; else select_product\">\n            <img id=\"main-image\" src=\"{{display_image.value.src}}\" alt=\"\" style=\"max-height: 250px; max-width: 100%;\">\n          </div>\n          <ng-template #select_product>\n            <div>click here to add image</div>\n          </ng-template>\n        </div>\n      </div>\n    </div>\n    <div id=\"angle-images-wrapper\">\n      <div id=\"angle-image-inner\">\n        <app-angle-image [group]=\"product_form\"></app-angle-image>\n      </div>\n    </div>\n  </div>\n  <div style=\"margin-bottom : 15px;\">\n    <div id=\"department-wrapper\">\n      <div id=\"department-inner\">\n        <app-department [group]=\"product_form\"></app-department>\n      </div>\n    </div>\n    <div id=\"features-wrapper\">\n      <div id=\"features-inner\">\n        <app-specs [group]=\"product_form\"></app-specs>\n      </div>\n    </div>\n  </div>\n  <ng-content select=\".submit-btn\"></ng-content>\n  <div>\n    <div *ngIf=\"submit_result.msg\" style=\"text-align: center; clear: both;\">\n      <div *ngIf=\"submit_result.status; else failed\" class=\"success-msg\">ggg{{submit_result.msg}}</div>\n      <ng-template #failed>\n        <div class=\"error-msg\">{{submit_result.msg}}</div>\n      </ng-template>\n    </div>\n  </div>\n  <div style=\"text-align: center; clear: both; padding-top: 20px\">\n    <input class=\"submit-btn\" type=\"submit\" style=\"border: none; padding: 3px 15px;\" value=\"add product\"\n      (click)=\"submit_form()\">\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/admin/catalogue/form/form.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/catalogue/form/form.component.ts ***!
  \********************************************************/
/*! exports provided: FormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormComponent", function() { return FormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/es2015/index.js");
/* harmony import */ var _services_catalogue_management_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/catalogue-management.service */ "./src/app/admin/services/catalogue-management.service.ts");





let FormComponent = class FormComponent {
    constructor(fb, storage, ctM) {
        this.fb = fb;
        this.storage = storage;
        this.ctM = ctM;
        this.submit_result = { status: false, msg: null };
    }
    ngOnInit() {
        this.product_form = this.fb.group({
            title: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)])],
            description: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            list_price: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            department: this.fb.array([]),
            quantity: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(1)])],
            specs: this.fb.array([]),
            display_image: [null],
            angle_image: this.fb.array([])
        });
    }
    get display_image() {
        return this.product_form.get('display_image');
    }
    get title() {
        return this.product_form.get('title');
    }
    get description() {
        return this.product_form.get('description');
    }
    get list_price() {
        return this.product_form.get('list_price');
    }
    get quantity() {
        return this.product_form.get('quantity');
    }
    submit_form() {
        var department = [];
        for (var i = 0; i < this.product_form.value.department.length; i++) {
            department.push(this.product_form.value.department[i].name);
        }
        this.ctM.addHandler(this.product_form.value)
            .subscribe(({ data }) => {
            console.log(data);
        }, (error) => {
            console.log(error);
        });
        // var filepath = this.storage.ref(this.display_image.value.name)
        // this.storage.upload(this.display_image.value.name, this.display_image.value.file).snapshotChanges().pipe(
        //   finalize(() => {
        //     filepath.getDownloadURL().subscribe((url) => {
        //       console.log(url)
        //     })
        //   })
        // ).subscribe()
    }
    //for main image
    file_upload(event) {
        var file = event.target.files[0];
        const reader = new FileReader();
        if (file) {
            reader.onload = ((dImg) => {
                return (e) => {
                    this.display_image.setValue({
                        name: file.name,
                        src: e.target.result,
                        file: file
                    });
                };
            })(file);
            reader.readAsDataURL(file);
        }
    }
};
FormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-form',
        template: __webpack_require__(/*! ./form.component.html */ "./src/app/admin/catalogue/form/form.component.html"),
        styles: [__webpack_require__(/*! ./form.component.css */ "./src/app/admin/catalogue/form/form.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"], _services_catalogue_management_service__WEBPACK_IMPORTED_MODULE_4__["CatalogueManagementService"]])
], FormComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/remove/remove.component.css":
/*!*************************************************************!*\
  !*** ./src/app/admin/catalogue/remove/remove.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".checkbox{\r\n    width: 30px;\r\n}\r\n\r\ntable{\r\n    width: 100%;\r\n}\r\n\r\nthead{\r\n    border-bottom: 1px solid grey;\r\n}\r\n\r\nthead th{\r\n    font-weight: 350;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL3JlbW92ZS9yZW1vdmUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLDZCQUE2QjtBQUNqQzs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NhdGFsb2d1ZS9yZW1vdmUvcmVtb3ZlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2hlY2tib3h7XHJcbiAgICB3aWR0aDogMzBweDtcclxufVxyXG5cclxudGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGhlYWR7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcclxufVxyXG5cclxudGhlYWQgdGh7XHJcbiAgICBmb250LXdlaWdodDogMzUwO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/admin/catalogue/remove/remove.component.html":
/*!**************************************************************!*\
  !*** ./src/app/admin/catalogue/remove/remove.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<main class=\"main-grid\">\n  <div class=\"main-content-area\">\n    <table>\n      <thead>\n        <th class=\"checkbox\"><input type=\"checkbox\" id=\"checkall\"></th>\n        <th>Title</th>\n      </thead>\n      <tbody>\n        <tr>\n          <td><input type=\"checkbox\"></td>\n          <td></td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</main>"

/***/ }),

/***/ "./src/app/admin/catalogue/remove/remove.component.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/catalogue/remove/remove.component.ts ***!
  \************************************************************/
/*! exports provided: RemoveComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemoveComponent", function() { return RemoveComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let RemoveComponent = class RemoveComponent {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
    }
};
RemoveComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-remove',
        template: __webpack_require__(/*! ./remove.component.html */ "./src/app/admin/catalogue/remove/remove.component.html"),
        styles: [__webpack_require__(/*! ./remove.component.css */ "./src/app/admin/catalogue/remove/remove.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], RemoveComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/size-quantity/size-quantity.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/admin/catalogue/size-quantity/size-quantity.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NhdGFsb2d1ZS9zaXplLXF1YW50aXR5L3NpemUtcXVhbnRpdHkuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/catalogue/size-quantity/size-quantity.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/admin/catalogue/size-quantity/size-quantity.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [formGroup]=\"size_quantity_group\">\n<div formArrayName=\"count\">\n    <div class=\"size-quantity-header\">size and quantity<span class=\"fa fa-plus-square-o\" (click)=\"add_count()\"></span></div>\n  <div *ngFor=\"let count of count.controls; index as j\" [formGroupName]=\"j\">\n    <div class=\"size-quantity-wrapper\">\n        <input class=\"size-input\" type=\"text\" placeholder=\"size\" formControlName=\"size\">\n        <input class=\"quantity-input\" type=\"number\" placeholder=\"qty\" formControlName=\"quantity\">\n        <span class=\"fa fa-trash-o\" (click)=\"remove_count(j)\"></span>\n    </div>\n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/catalogue/size-quantity/size-quantity.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/admin/catalogue/size-quantity/size-quantity.component.ts ***!
  \**************************************************************************/
/*! exports provided: SizeQuantityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SizeQuantityComponent", function() { return SizeQuantityComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let SizeQuantityComponent = class SizeQuantityComponent {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        this.add_count();
    }
    add_count() {
        var count = this.fb.group({
            size: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([])],
            quantity: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([])]
        });
        this.count.push(count);
    }
    remove_count(index) {
        this.count.removeAt(index);
    }
    get count() {
        return this.size_quantity_group.controls['count'];
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('group'),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"])
], SizeQuantityComponent.prototype, "size_quantity_group", void 0);
SizeQuantityComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-size-quantity',
        template: __webpack_require__(/*! ./size-quantity.component.html */ "./src/app/admin/catalogue/size-quantity/size-quantity.component.html"),
        styles: [__webpack_require__(/*! ./size-quantity.component.css */ "./src/app/admin/catalogue/size-quantity/size-quantity.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], SizeQuantityComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/specs/specs.component.css":
/*!***********************************************************!*\
  !*** ./src/app/admin/catalogue/specs/specs.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".name-wrapper{\r\n    display: inline-block;\r\n    width: 60%;\r\n}\r\n\r\n.value-wrapper{\r\n    display: inline-block;\r\n    width: calc(40% - 20px);\r\n}\r\n\r\n.trash-wrapper{\r\n    width: 20px;\r\n    display: inline-block;\r\n}\r\n\r\n.value, .name, .trash{\r\n    width: 100%;\r\n}\r\n\r\n.trash{\r\n    text-align: right;\r\n}\r\n\r\n.plus-icon{\r\n    float: right;\r\n    clear: both;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL3NwZWNzL3NwZWNzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxxQkFBcUI7SUFDckIsVUFBVTtBQUNkOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osV0FBVztBQUNmIiwiZmlsZSI6InNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL3NwZWNzL3NwZWNzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmFtZS13cmFwcGVye1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgd2lkdGg6IDYwJTtcclxufVxyXG5cclxuLnZhbHVlLXdyYXBwZXJ7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB3aWR0aDogY2FsYyg0MCUgLSAyMHB4KTtcclxufVxyXG5cclxuLnRyYXNoLXdyYXBwZXJ7XHJcbiAgICB3aWR0aDogMjBweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLnZhbHVlLCAubmFtZSwgLnRyYXNoe1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi50cmFzaHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG4ucGx1cy1pY29ue1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/admin/catalogue/specs/specs.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/catalogue/specs/specs.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"specs_group\" [formGroup]=\"specs_group\">\n  <div formArrayName=\"specs\" class=\"form-element\">\n    <div class=\"specs\"><span style=\"font-size: 13px;\">Specifications</span> <span class=\"fa fa-plus-square-o plus-icon\" (click)=\"add_spec()\"></span></div>\n    <div class=\"specs-array\" *ngFor=\"let spec of specs.controls; index as k\" [formGroupName]=\"k\">\n      <div class=\"specs-input\">\n        <div class=\"name-wrapper\">\n          <input class=\"name\" type=\"text\" placeholder=\"spec name\" formControlName=\"name\">\n        </div>\n        <div class=\"value-wrapper\">\n          <input class=\"value\" type=\"text\" placeholder=\"value\" formControlName=\"value\">\n        </div>\n        <div class=\"trash-wrapper\">\n          <span class=\"fa fa-trash-o trash\" (click)=\"delete_spec(k)\"></span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/catalogue/specs/specs.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/catalogue/specs/specs.component.ts ***!
  \**********************************************************/
/*! exports provided: SpecsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecsComponent", function() { return SpecsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let SpecsComponent = class SpecsComponent {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        this.add_spec();
    }
    get specs() {
        return this.specs_group.controls['specs'];
    }
    add_spec() {
        this.specs.push(this.specs_template());
    }
    specs_template() {
        return this.fb.group({
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([])],
            value: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([])],
        });
    }
    delete_spec(index) {
        this.specs.removeAt(index);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('group'),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"])
], SpecsComponent.prototype, "specs_group", void 0);
SpecsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-specs',
        template: __webpack_require__(/*! ./specs.component.html */ "./src/app/admin/catalogue/specs/specs.component.html"),
        styles: [__webpack_require__(/*! ./specs.component.css */ "./src/app/admin/catalogue/specs/specs.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], SpecsComponent);



/***/ }),

/***/ "./src/app/admin/catalogue/view/view.component.css":
/*!*********************************************************!*\
  !*** ./src/app/admin/catalogue/view/view.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".product-table{\r\n    width: 100%;\r\n    margin-top: 20px;\r\n    border-spacing: 0px;\r\n}\r\n\r\n#add-wrapper{\r\n    float: right;\r\n    clear: both;\r\n    border-radius: 5px;\r\n    background-color:rgb(10, 15, 80);\r\n    color: white;\r\n    padding: 5px 7px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.table-remove, .table-edit{\r\n    width: 15%;\r\n}\r\n\r\n.table-remove, .table-edit{\r\n    text-align: center;\r\n}\r\n\r\n.fa-edit:hover, .fa-remove:hover{\r\n    color : rgb(179, 179, 184);\r\n    cursor: pointer;\r\n}\r\n\r\n.table-quantity{\r\n    width: 25%;\r\n    padding-right: 10px;\r\n}\r\n\r\n.table-title{\r\n    width : 60%;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    padding-right: 10px;\r\n}\r\n\r\nthead{\r\n    font-weight: 400;\r\n    border-bottom: 1.2px solid rgb(179, 179, 184) ;\r\n    background-color: rgb(10, 15, 80);\r\n    color: white;\r\n\r\n}\r\n\r\ntbody {\r\n    font-weight: 200\r\n}\r\n\r\ntbody tr:hover{\r\n    cursor: pointer;\r\n}\r\n\r\n.table-sku, .table-price{\r\n    display: none;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vY2F0YWxvZ3VlL3ZpZXcvdmlldy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLGdCQUFnQjtJQUNoQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQixnQ0FBZ0M7SUFDaEMsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxVQUFVO0FBQ2Q7O0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSwwQkFBMEI7SUFDMUIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLFVBQVU7SUFDVixtQkFBbUI7QUFDdkI7O0FBR0E7SUFDSSxXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsOENBQThDO0lBQzlDLGlDQUFpQztJQUNqQyxZQUFZOztBQUVoQjs7QUFFQTtJQUNJO0FBQ0o7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksYUFBYTtBQUNqQiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NhdGFsb2d1ZS92aWV3L3ZpZXcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9kdWN0LXRhYmxle1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgYm9yZGVyLXNwYWNpbmc6IDBweDtcclxufVxyXG5cclxuI2FkZC13cmFwcGVye1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJnYigxMCwgMTUsIDgwKTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDVweCA3cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVtb3ZlLCAudGFibGUtZWRpdHtcclxuICAgIHdpZHRoOiAxNSU7XHJcbn1cclxuLnRhYmxlLXJlbW92ZSwgLnRhYmxlLWVkaXR7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5mYS1lZGl0OmhvdmVyLCAuZmEtcmVtb3ZlOmhvdmVye1xyXG4gICAgY29sb3IgOiByZ2IoMTc5LCAxNzksIDE4NCk7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi50YWJsZS1xdWFudGl0eXtcclxuICAgIHdpZHRoOiAyNSU7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuLnRhYmxlLXRpdGxle1xyXG4gICAgd2lkdGggOiA2MCU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG50aGVhZHtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBib3JkZXItYm90dG9tOiAxLjJweCBzb2xpZCByZ2IoMTc5LCAxNzksIDE4NCkgO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDEwLCAxNSwgODApO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG5cclxufVxyXG5cclxudGJvZHkge1xyXG4gICAgZm9udC13ZWlnaHQ6IDIwMFxyXG59XHJcblxyXG50Ym9keSB0cjpob3ZlcntcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLnRhYmxlLXNrdSwgLnRhYmxlLXByaWNle1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/admin/catalogue/view/view.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/catalogue/view/view.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div routerLink=\"/admin/catalogue/add\" id=\"add-wrapper\"><span class=\"fa fa-plus\"></span></div>\n<table class=\"product-table\">\n  <thead class=\"table-head\">\n    <tr>\n      <td class=\"table-sku\">sku</td>\n      <td class=\"table-title\">title</td>\n      <td class=\"table-price\">price</td>\n      <td class=\"table-quantity\">quantity</td>\n      <td class=\"table-edit\" style=\"width: 15px\"></td>\n      <td class=\"table-remove\" style=\"width: 15px\"></td>\n    </tr>\n  </thead>\n  <tbody *ngIf=\"product\">>\n    <tr *ngFor=\"let product of products | async\">\n      <td class=\"table-sku\">{{product.sku}}</td>\n      <td class=\"table-title\">{{product.title}}</td>\n      <td class=\"table-price\">{{product.list_price}}</td>\n      <td class=\"table-quantity\">{{product.quantity}}</td>\n      <td class=\"table-edit\"><span class=\"fa fa-edit\" routerLink=\"/admin/catalogue/edit\"></span></td>\n      <td class=\"table-remove\"><span class=\"fa fa-trash-o\" (click)=\"delete_handler(product.sku)\"></span></td>\n    </tr>\n  </tbody>\n</table>\n<div *ngIf=\"!product\" style=\"color: lightgray; margin-top:10px; text-align:center; margin-top: 20px; width:100%\"><span>stock up your shop</span></div>\n"

/***/ }),

/***/ "./src/app/admin/catalogue/view/view.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/catalogue/view/view.component.ts ***!
  \********************************************************/
/*! exports provided: ViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewComponent", function() { return ViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/admin/services/common.service.ts");




let ViewComponent = class ViewComponent {
    constructor(common) {
        this.common = common;
    }
    ngOnInit() {
        this.getProducts();
    }
    delete_handler(sku) {
        //document.alert("Are you sure?")
        console.log(sku);
    }
    getProducts() {
        this.products = this.common.getShopProducts()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(({ data }) => {
            console.log(data, " dsfs");
            return data.shopProducts;
        }));
    }
};
ViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product',
        template: __webpack_require__(/*! ./view.component.html */ "./src/app/admin/catalogue/view/view.component.html"),
        styles: [__webpack_require__(/*! ./view.component.css */ "./src/app/admin/catalogue/view/view.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
], ViewComponent);



/***/ }),

/***/ "./src/app/admin/services/catalogue-management.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/admin/services/catalogue-management.service.ts ***!
  \****************************************************************/
/*! exports provided: CatalogueManagementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogueManagementService", function() { return CatalogueManagementService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm2015/ng.apollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);




let CatalogueManagementService = class CatalogueManagementService {
    constructor(apollo) {
        this.apollo = apollo;
        this.addMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
  
    mutation addProduct(
      $title:String!, 
      $quantity:Int!, 
      $list_price: Float!, 
      $display_image: String!,
      $department: [String!]!,
      $features: [Feature]
      ){
        addProduct(
          title:$title,
          quantity:$quantity, 
          list_price:$list_price, 
          display_image:$display_image,
          department:$department,
          features:$features
        ){
          title
        }
      }
  `;
        this.editMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
  mutation editProduct($sku:String){
    editProduct(sku:$sku){
      title
    }
  }
`;
        this.deleteMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
    mutation deleteProductMutation($sku:string){
      deleteProduct(sku:$sku){
        sku
      }
    }
  `;
    }
    addHandler(addForm) {
        return this.apollo.mutate({
            mutation: this.addMutation,
            variables: {
                title: 'rr',
                list_price: 55,
                display_image: 'gdsdg',
                quantity: 3,
                department: ['yah'],
                features: [{ name: "ee", value: "ee", unit: "ee" }]
            }
        });
    }
    deleteHandler(sku) {
        return this.apollo.mutate({
            mutation: this.deleteMutation,
            variables: { sku }
        });
    }
};
CatalogueManagementService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
], CatalogueManagementService);



/***/ })

}]);
//# sourceMappingURL=catalogue-catalogue-module.js.map