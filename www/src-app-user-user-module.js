(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-user-user-module"],{

/***/ "./src/app/user/components/home/home.component.css":
/*!*********************************************************!*\
  !*** ./src/app/user/components/home/home.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-banner{\r\n  margin-top: 10px;\r\n  height: 239px;\r\n  background: url(http://localhost:3572/api/file/11112.png) 0px -10px no-repeat , linear-gradient(to right, #71d1dd , #8f999b);\r\n  position: relative;\r\n  overflow: hidden;\r\n}\r\n\r\n\r\n.adv-details{\r\n  position: absolute;\r\n  display: inline-block;\r\n  right: 10px;\r\n  top: 50px;\r\n  color: white;\r\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\r\n  font-style: italic;\r\n  text-align: center;\r\n}\r\n\r\n\r\n.adv-prod-details{\r\n  position: absolute;\r\n  left: 420px;\r\n  top: 150px;\r\n  display: inline-block;\r\n  color: white;\r\n  font-family: sans-serif;\r\n  font-style: italic;\r\n  display: none;\r\n}\r\n\r\n\r\n.adv-title{\r\n  display: none;\r\n}\r\n\r\n\r\n.shop-now-btn{\r\n  border-radius: 5px; \r\n  background-color: #0064f5;\r\n  border-color: #0064f5;\r\n  color: white;\r\n  font-family: sans-serif;\r\n  margin-left: -10px;\r\n}\r\n\r\n\r\n@media screen and (min-width : 769px){\r\n  .new-banner{\r\n    margin-top: 10px;\r\n    height: 239px;\r\n    background: url(http://localhost:3572/api/file/11112.png) 80px -10px no-repeat , linear-gradient(to right, #71d1dd , #8f999b);\r\n    position: relative;\r\n    overflow: hidden;\r\n  }\r\n\r\n  .adv-prod-details, .adv-title{\r\n    display: inline-block;\r\n  }\r\n\r\n  .adv-details{\r\n    right: 100px;\r\n  }\r\n\r\n  \r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYiw0SEFBNEg7RUFDNUgsa0JBQWtCO0VBQ2xCLGdCQUFnQjtBQUNsQjs7O0FBR0E7RUFDRSxrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxTQUFTO0VBQ1QsWUFBWTtFQUNaLGdEQUFnRDtFQUNoRCxrQkFBa0I7RUFDbEIsa0JBQWtCO0FBQ3BCOzs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsVUFBVTtFQUNWLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osdUJBQXVCO0VBQ3ZCLGtCQUFrQjtFQUNsQixhQUFhO0FBQ2Y7OztBQUVBO0VBQ0UsYUFBYTtBQUNmOzs7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQix5QkFBeUI7RUFDekIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWix1QkFBdUI7RUFDdkIsa0JBQWtCO0FBQ3BCOzs7QUFHQTtFQUNFO0lBQ0UsZ0JBQWdCO0lBQ2hCLGFBQWE7SUFDYiw2SEFBNkg7SUFDN0gsa0JBQWtCO0lBQ2xCLGdCQUFnQjtFQUNsQjs7RUFFQTtJQUNFLHFCQUFxQjtFQUN2Qjs7RUFFQTtJQUNFLFlBQVk7RUFDZDs7O0FBR0YiLCJmaWxlIjoic3JjL2FwcC91c2VyL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmV3LWJhbm5lcntcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMjM5cHg7XHJcbiAgYmFja2dyb3VuZDogdXJsKGh0dHA6Ly9sb2NhbGhvc3Q6MzU3Mi9hcGkvZmlsZS8xMTExMi5wbmcpIDBweCAtMTBweCBuby1yZXBlYXQgLCBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM3MWQxZGQgLCAjOGY5OTliKTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuXHJcbi5hZHYtZGV0YWlsc3tcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHJpZ2h0OiAxMHB4O1xyXG4gIHRvcDogNTBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5hZHYtcHJvZC1kZXRhaWxze1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiA0MjBweDtcclxuICB0b3A6IDE1MHB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zdHlsZTogaXRhbGljO1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi5hZHYtdGl0bGV7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuXHJcbi5zaG9wLW5vdy1idG57XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4OyBcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA2NGY1O1xyXG4gIGJvcmRlci1jb2xvcjogIzAwNjRmNTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG59XHJcblxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OXB4KXtcclxuICAubmV3LWJhbm5lcntcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBoZWlnaHQ6IDIzOXB4O1xyXG4gICAgYmFja2dyb3VuZDogdXJsKGh0dHA6Ly9sb2NhbGhvc3Q6MzU3Mi9hcGkvZmlsZS8xMTExMi5wbmcpIDgwcHggLTEwcHggbm8tcmVwZWF0ICwgbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjNzFkMWRkICwgIzhmOTk5Yik7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIH1cclxuXHJcbiAgLmFkdi1wcm9kLWRldGFpbHMsIC5hZHYtdGl0bGV7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICAuYWR2LWRldGFpbHN7XHJcbiAgICByaWdodDogMTAwcHg7XHJcbiAgfVxyXG5cclxuICBcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/user/components/home/home.component.html":
/*!**********************************************************!*\
  !*** ./src/app/user/components/home/home.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngFor=\"let topic of topics | async\">\n    <h5 style=\"margin-bottom: 30px\">{{topic.topic}}</h5>\n    <app-slider>\n      <product \n        *ngFor=\"let product of topic.products\" \n        [position]=\"current_position\" \n        [product]=\"product\"\n        (width)=\"set_variables($event)\">\n      </product>\n    </app-slider>\n</div>\n\n<div class=\"new-banner advert-banner\">\n  <div class=\"adv-prod-details\">\n    <div>Lenovo</div>\n    <div>HDD 500GB</div>\n    <div>RAM 4GB</div>\n  </div>\n\n  <div class=\"adv-details\">\n    <p class=\"adv-title\">Hot new Deals at Incredible connections</p>\n    <div class=\"adv-offer\">\n\n      dd\n      <div>WAS <del>5999</del></div>\n      <div>NOW 4999</div>\n    </div><br>\n    <div class=\"btn-container\"><button class=\"shop-now-btn\">SHOP NOW</button></div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/user/components/home/home.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user/components/home/home.component.ts ***!
  \********************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm2015/ng.apollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let HomeComponent = class HomeComponent {
    constructor(apollo) {
        this.apollo = apollo;
    }
    ngOnInit() {
        console.log("before home");
        this.topics = this.apollo.watchQuery({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a `
          query homeTopics($screenWidth:Int, $screenHeight:Int, $scroll: Int) {
            home(screenWidth:$screenWidth, screenHeight:$screenHeight, scroll: $scroll){
              topic
              products{
                title
                display_image
                list_price
              }
            }
          }
        `,
            variables: {
                screenWidth: 1,
                screenHeight: 1,
                scroll: 1
            }
        })
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(({ data }) => {
            console.log(data, ' from home query');
            return data.home;
        }));
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        template: __webpack_require__(/*! ./home.component.html */ "./src/app/user/components/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/user/components/home/home.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
], HomeComponent);



/***/ }),

/***/ "./src/app/user/components/list/list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/user/components/list/list.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#list-title{\r\n  position: relative;\r\n}\r\n\r\n\r\n.list-item-wrapper{\r\n  width: 50%;\r\n  height: 150px;\r\n  margin-bottom: 5px;\r\n  text-align: center;\r\n  position: relative;\r\n  float: left;\r\n}\r\n\r\n\r\n.list-item-inner{\r\n  background : bisque;\r\n  height: inherit;\r\n  margin: 5px;\r\n}\r\n\r\n\r\n#list-title-wrapper{\r\n  height: 100px;\r\n  background-color : rosybrown;\r\n  margin-bottom: 20px;\r\n  padding: 20px 10px;\r\n}\r\n\r\n\r\n#list-wrapper{\r\n  text-align: center;\r\n}\r\n\r\n\r\n@media screen and (min-width : 764px){\r\n  .list-item-wrapper{\r\n    width: calc(100% / 3);\r\n  }\r\n\r\n}\r\n\r\n\r\n@media screen and (min-width : 964px){\r\n  .list-item-wrapper{\r\n    width: calc(100% / 4);\r\n  }\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL2xpc3QvbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0FBQ3BCOzs7QUFHQTtFQUNFLFVBQVU7RUFDVixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsV0FBVztBQUNiOzs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsV0FBVztBQUNiOzs7QUFFQTtFQUNFLGFBQWE7RUFDYiw0QkFBNEI7RUFDNUIsbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUNwQjs7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7OztBQUdBO0VBQ0U7SUFDRSxxQkFBcUI7RUFDdkI7O0FBRUY7OztBQUVBO0VBQ0U7SUFDRSxxQkFBcUI7RUFDdkI7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvY29tcG9uZW50cy9saXN0L2xpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNsaXN0LXRpdGxle1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuXHJcbi5saXN0LWl0ZW0td3JhcHBlcntcclxuICB3aWR0aDogNTAlO1xyXG4gIGhlaWdodDogMTUwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbi5saXN0LWl0ZW0taW5uZXJ7XHJcbiAgYmFja2dyb3VuZCA6IGJpc3F1ZTtcclxuICBoZWlnaHQ6IGluaGVyaXQ7XHJcbiAgbWFyZ2luOiA1cHg7XHJcbn1cclxuXHJcbiNsaXN0LXRpdGxlLXdyYXBwZXJ7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yIDogcm9zeWJyb3duO1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgcGFkZGluZzogMjBweCAxMHB4O1xyXG59XHJcblxyXG4jbGlzdC13cmFwcGVye1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA3NjRweCl7XHJcbiAgLmxpc3QtaXRlbS13cmFwcGVye1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAvIDMpO1xyXG4gIH1cclxuXHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA5NjRweCl7XHJcbiAgLmxpc3QtaXRlbS13cmFwcGVye1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAvIDQpO1xyXG4gIH1cclxufVxyXG5cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/user/components/list/list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/user/components/list/list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"(data$ | async) as data\">\r\n    <div *ngIf=\"data\">\r\n        <div id=\"list-title-wrapper\">\r\n            <div id=\"list-title\">{{data.heading}}</div>\r\n        </div>\r\n        <div id=\"list-wrapper\">\r\n            <div class=\"list-item-wrapper\" *ngFor=\"let item of data.data\">\r\n                <div class=\"list-item-inner\" (click)=\"visit(item.name, data.heading)\">\r\n                   <div>{{item.name}}</div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/user/components/list/list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user/components/list/list.component.ts ***!
  \********************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/user/services/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ListComponent = class ListComponent {
    constructor(product_service, router) {
        this.product_service = product_service;
        this.router = router;
    }
    ngOnInit() {
        this.product_service.set_page_variables(null);
        this.data$ = this.product_service.list;
    }
    visit(name, field) {
        this.product_service.view_variables(name, field);
        this.router.navigate(['/user/view']);
    }
};
ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-departments',
        template: __webpack_require__(/*! ./list.component.html */ "./src/app/user/components/list/list.component.html"),
        styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/user/components/list/list.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], ListComponent);



/***/ }),

/***/ "./src/app/user/components/navigation/navigation.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/user/components/navigation/navigation.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#navbar-wrapper{\r\n  background-color: #119ebd;\r\n  height: 80px;\r\n  position: fixed;\r\n  width: 100vw;\r\n  z-index: 8000;\r\n  color: white;\r\n}\r\n\r\n#nav-inner{\r\n  margin: 0 10px;\r\n  margin-top: 20px;\r\n  position: relative;\r\n}\r\n\r\n#user-links li{\r\n  display: inline-block;\r\n  margin-right: 10px;\r\n}\r\n\r\n#dropdown-menu{\r\n  position: absolute; \r\n  top: 30px; \r\n  right: -7px;  \r\n  width: 140px; \r\n  background-color: lightgrey;\r\n  visibility: hidden;\r\n}\r\n\r\n#left-column, #center-column, #right-column{\r\n  position: relative;\r\n}\r\n\r\n#left-column, #center-column{\r\n  float: left;\r\n  margin-right: 10px;\r\n}\r\n\r\n#right-column{\r\n  float: right;\r\n}\r\n\r\n#registration-links, #search-icon, .registration-link-item{\r\n  display: inline-block;\r\n  margin-left: 10px;\r\n}\r\n\r\n#return-wrapper{\r\n  float: left;\r\n  margin-right: 10px;\r\n  color: lightgray;\r\n  margin-top: 5px;\r\n}\r\n\r\n#search-input{\r\n  width: calc(100% - 50px);\r\n  border:none;\r\n  border-bottom: 1px solid rgb(172, 172, 172);\r\n  outline: none;\r\n  background-color: inherit;\r\n}\r\n\r\n#nav-links{\r\n  display: inline-block;\r\n  margin: 0;\r\n}\r\n\r\n.search-inactive{\r\n  display: none;\r\n}\r\n\r\n#acquire:hover, .link-item:hover, .registration-link-item:hover, .user-link-item:hover{\r\n  cursor: pointer;\r\n  color: rgb(4, 4, 58);\r\n}\r\n\r\n#user-icon{\r\n  display: none;\r\n}\r\n\r\n@media screen and (min-width:768px){\r\n  #search-icon{\r\n    display: none;\r\n  }\r\n\r\n  #dropdown-menu{\r\n    visibility: visible;\r\n  }\r\n\r\n  #nav-inner{\r\n    margin-right: 24px;\r\n  }\r\n\r\n  #nav-links{\r\n    position: absolute;\r\n    bottom: -25px;\r\n  }\r\n\r\n  #search-wrapper{\r\n    width: 400px;\r\n  }\r\n\r\n  #search-input{\r\n    border: 1px solid rgb(172, 172, 172);\r\n    background-color: whitesmoke;\r\n  }\r\n\r\n  #search-input:focus{\r\n    border-color : white;\r\n\r\n  }\r\n\r\n  #menu-icon{\r\n    display: none;\r\n  }\r\n\r\n  #user-icon{\r\n    display: inline;\r\n  }\r\n}\r\n\r\n@media screen and (min-width:964px){\r\n\r\n  #navbar-wrapper{\r\n    height: 70px;\r\n  }\r\n\r\n\r\n  #acquire{\r\n    font-size: 20px;\r\n  }\r\n\r\n  #left-column{\r\n    margin-right: 5%;\r\n    margin-left: 10px;\r\n  }\r\n\r\n  #right-column{\r\n    margin-right: 20px;\r\n  }\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixlQUFlO0VBQ2YsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLHFCQUFxQjtFQUNyQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFdBQVc7RUFDWCxZQUFZO0VBQ1osMkJBQTJCO0VBQzNCLGtCQUFrQjtBQUNwQjs7QUFHQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxxQkFBcUI7RUFDckIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLHdCQUF3QjtFQUN4QixXQUFXO0VBQ1gsMkNBQTJDO0VBQzNDLGFBQWE7RUFDYix5QkFBeUI7QUFDM0I7O0FBR0E7RUFDRSxxQkFBcUI7RUFDckIsU0FBUztBQUNYOztBQUVBO0VBQ0UsYUFBYTtBQUNmOztBQUVBO0VBQ0UsZUFBZTtFQUNmLG9CQUFvQjtBQUN0Qjs7QUFFQTtFQUNFLGFBQWE7QUFDZjs7QUFFQTtFQUNFO0lBQ0UsYUFBYTtFQUNmOztFQUVBO0lBQ0UsbUJBQW1CO0VBQ3JCOztFQUVBO0lBQ0Usa0JBQWtCO0VBQ3BCOztFQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLGFBQWE7RUFDZjs7RUFFQTtJQUNFLFlBQVk7RUFDZDs7RUFFQTtJQUNFLG9DQUFvQztJQUNwQyw0QkFBNEI7RUFDOUI7O0VBRUE7SUFDRSxvQkFBb0I7O0VBRXRCOztFQUVBO0lBQ0UsYUFBYTtFQUNmOztFQUVBO0lBQ0UsZUFBZTtFQUNqQjtBQUNGOztBQUVBOztFQUVFO0lBQ0UsWUFBWTtFQUNkOzs7RUFHQTtJQUNFLGVBQWU7RUFDakI7O0VBRUE7SUFDRSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0VBQ25COztFQUVBO0lBQ0Usa0JBQWtCO0VBQ3BCO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC91c2VyL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbmF2YmFyLXdyYXBwZXJ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzExOWViZDtcclxuICBoZWlnaHQ6IDgwcHg7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHdpZHRoOiAxMDB2dztcclxuICB6LWluZGV4OiA4MDAwO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuI25hdi1pbm5lcntcclxuICBtYXJnaW46IDAgMTBweDtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuI3VzZXItbGlua3MgbGl7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuI2Ryb3Bkb3duLW1lbnV7XHJcbiAgcG9zaXRpb246IGFic29sdXRlOyBcclxuICB0b3A6IDMwcHg7IFxyXG4gIHJpZ2h0OiAtN3B4OyAgXHJcbiAgd2lkdGg6IDE0MHB4OyBcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG59XHJcblxyXG5cclxuI2xlZnQtY29sdW1uLCAjY2VudGVyLWNvbHVtbiwgI3JpZ2h0LWNvbHVtbntcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbiNsZWZ0LWNvbHVtbiwgI2NlbnRlci1jb2x1bW57XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG4jcmlnaHQtY29sdW1ue1xyXG4gIGZsb2F0OiByaWdodDtcclxufVxyXG5cclxuI3JlZ2lzdHJhdGlvbi1saW5rcywgI3NlYXJjaC1pY29uLCAucmVnaXN0cmF0aW9uLWxpbmstaXRlbXtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbn1cclxuXHJcbiNyZXR1cm4td3JhcHBlcntcclxuICBmbG9hdDogbGVmdDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgY29sb3I6IGxpZ2h0Z3JheTtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbiNzZWFyY2gtaW5wdXR7XHJcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDUwcHgpO1xyXG4gIGJvcmRlcjpub25lO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMTcyLCAxNzIsIDE3Mik7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBpbmhlcml0O1xyXG59XHJcblxyXG5cclxuI25hdi1saW5rc3tcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4uc2VhcmNoLWluYWN0aXZle1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbiNhY3F1aXJlOmhvdmVyLCAubGluay1pdGVtOmhvdmVyLCAucmVnaXN0cmF0aW9uLWxpbmstaXRlbTpob3ZlciwgLnVzZXItbGluay1pdGVtOmhvdmVye1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBjb2xvcjogcmdiKDQsIDQsIDU4KTtcclxufVxyXG5cclxuI3VzZXItaWNvbntcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOjc2OHB4KXtcclxuICAjc2VhcmNoLWljb257XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgI2Ryb3Bkb3duLW1lbnV7XHJcbiAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xyXG4gIH1cclxuXHJcbiAgI25hdi1pbm5lcntcclxuICAgIG1hcmdpbi1yaWdodDogMjRweDtcclxuICB9XHJcblxyXG4gICNuYXYtbGlua3N7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IC0yNXB4O1xyXG4gIH1cclxuXHJcbiAgI3NlYXJjaC13cmFwcGVye1xyXG4gICAgd2lkdGg6IDQwMHB4O1xyXG4gIH1cclxuXHJcbiAgI3NlYXJjaC1pbnB1dHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigxNzIsIDE3MiwgMTcyKTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlc21va2U7XHJcbiAgfVxyXG5cclxuICAjc2VhcmNoLWlucHV0OmZvY3Vze1xyXG4gICAgYm9yZGVyLWNvbG9yIDogd2hpdGU7XHJcblxyXG4gIH1cclxuXHJcbiAgI21lbnUtaWNvbntcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAjdXNlci1pY29ue1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDo5NjRweCl7XHJcblxyXG4gICNuYXZiYXItd3JhcHBlcntcclxuICAgIGhlaWdodDogNzBweDtcclxuICB9XHJcblxyXG5cclxuICAjYWNxdWlyZXtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICB9XHJcblxyXG4gICNsZWZ0LWNvbHVtbntcclxuICAgIG1hcmdpbi1yaWdodDogNSU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICB9XHJcblxyXG4gICNyaWdodC1jb2x1bW57XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/user/components/navigation/navigation.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/user/components/navigation/navigation.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav id=\"navbar-wrapper\" (window:load)=\"check_screen_width()\" (window:resize)=\"check_screen_width()\">\n  <div id=\"nav-inner\">\n    <div id=\"left-column\">\n      <div routerLink=\"home\" id=\"acquire\" *ngIf=\"!show_search\">acquire</div>\n    </div>\n    <div id=\"center-column\" [style.float]=\"show_search?'none':'left'\">\n      <div id=\"return-wrapper\" *ngIf=\"show_search\">\n        <span class=\"fa fa-arrow-left\" (click)=\"toggle_show_search()\"></span>\n      </div>\n      <div id=\"search-wrapper\" *ngIf=\"show_search || screen_width > 764\" style=\"border-radius: 20px; background-color : whitesmoke; padding-left: 20px; padding-right: 5px; height: 25px\">\n        <form [formGroup]=\"search_form\" (ngSubmit)=\"send_query()\" style=\"height: inherit\">\n          <input type=\"text\" id=\"search-input\" placeholder=\"I'm looking for...\" formControlName=\"query\"\n          [value]=\"!query ? last_search : query.value\" style=\"width : calc(100% - 20px); background-color: inherit; border : none; height: inherit;\">\n          <span style=\"color:lightgray\" class=\"fa fa-search\" (click)=\"send_query()\"></span>\n        </form>\n      </div>\n  \n      <ul id=\"nav-links\" *ngIf=\"!show_search\">\n        <li \n          *ngFor=\"let window of windows_list; index as i\" \n          style=\"display: inline-block; margin-right: 10px;\" \n          (click)=\"visit_window(i)\">{{window.name}}\n        </li>\n      </ul>\n  \n    </div>\n    \n    <div id=\"right-column\" *ngIf=\"!show_search\">\n      <div id=\"search-icon\" (click)=\"toggle_show_search()\"><span class=\"fa fa-search\"></span></div>\n      <div id=\"registration-links\">\n        <ng-template [ngIf]=\"(login_status_check$ | async)\" let-login_status>\n          <div *ngIf=\"!login_status.status; else logout\">\n              <div routerLink=\"/sign-in\" class=\"registration-link-item\">sign In</div>\n              <button routerLink=\"/sign-up\" *ngIf=\"screen_width > 764\" class=\"fa fa-user registration-link-item\">Sign Up</button>\n          </div>\n        </ng-template>\n        <ng-template #logout>\n          <ul id=\"user-links\">\n            <li>\n              <span class=\"fa fa-bell user-link-item\"></span>\n            </li>\n            <li (click)=\"sidebar_pane_handler(0)\">\n              <span class=\"fa fa-shopping-basket user-link-item\"></span>\n            </li>\n            <li>\n              <div style=\"position: relative\">\n                <div>\n                  <span id=\"user-icon\" class=\"fa fa-user user-link-item\" (click)=\"toggle_user_options($event)\"></span> \n                  <div id=\"menu-icon\"><span class=\"fa fa-bars\" (click)=\"toggle_sidebar()\"></span></div>\n                </div>\n                <div *ngIf=\"user_options_state\" id=\"dropdown-menu\">\n                  <span style=\" position: absolute; right: 7px; top: -12px; color : rgb(195, 195, 195);\" class=\"fa fa-caret-up\"></span>\n                  <ul style=\"padding: 5px 10px;\">\n                    <li class=\"user-link-item\" routerLink = \"account/profile\">profile</li>\n                    <li class=\"user-link-item\" routerLink = \"/admin\">manage shop</li>\n                    <li class=\"user-link-item\" (click)=\"logout_handler()\" class=\"user-link-item\">logout</li>\n                  </ul>\n                </div>\n              </div>\n            </li>\n          </ul>\n        </ng-template>\n      </div>\n    </div>\n  </div>\n</nav>\n\n"

/***/ }),

/***/ "./src/app/user/components/navigation/navigation.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/user/components/navigation/navigation.component.ts ***!
  \********************************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/user/services/product.service.ts");
/* harmony import */ var src_app_core_registration_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/registration/authentication.service */ "./src/app/core/registration/authentication.service.ts");
/* harmony import */ var _services_sidebar_menu_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/sidebar-menu.service */ "./src/app/user/services/sidebar-menu.service.ts");
/* harmony import */ var _services_search_handler_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/search-handler.service */ "./src/app/user/services/search-handler.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_shared_action_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/action.service */ "./src/app/shared/action.service.ts");









let NavigationComponent = class NavigationComponent {
    constructor(product_service, fb, auth, search, sidebar_service, router, action_service) {
        this.product_service = product_service;
        this.fb = fb;
        this.auth = auth;
        this.search = search;
        this.sidebar_service = sidebar_service;
        this.router = router;
        this.action_service = action_service;
        this._user_options_state = false;
        this.windows_list = [
            {
                name: "department", state: false, options: [
                    { name: "accessories", keywords: ['accessory'] }, { name: "men's fashion", keywords: ['men'] }
                ]
            },
            {
                name: "shop", state: false, options: [
                    { name: "markhams", keywords: ['markhams'] }, { name: "pick n pay", keywords: ['pick n pay'] }
                ]
            },
        ];
        this._active_window = -1;
        this.last_search = localStorage.getItem('last_search_term');
        this.show_search = false;
        this.visible_class = "options-visible";
        this.search_form = this.fb.group({
            query: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].minLength(1)])]
        });
    }
    ngOnInit() {
        this.login_status_check$ = false; //this.auth.login_status
        document.addEventListener('click', (evt) => {
            this._user_options_state = false;
        });
    }
    get query() {
        return this.search_form.get('query');
    }
    get user_options_state() {
        return this._user_options_state;
    }
    toggle_user_options(event) {
        event.stopPropagation();
        this._user_options_state = !this.user_options_state;
    }
    toggle_show_search() {
        this.show_search = !this.show_search;
    }
    check_screen_width() {
        this.screen_width = document.querySelector('#navbar-wrapper').clientWidth;
    }
    toggle_navigation_list(index, event) {
        if (this.windows_list[index].state && event.type == "click") {
            this.windows_list[index].state = false;
            this.active_window = -1;
            return;
        }
        this.windows_list.forEach(i => {
            i.state = false;
        });
        this.windows_list[index].state = true;
        this._active_window = index;
    }
    hide_list() {
        this._active_window = -1;
    }
    set active_window(index) {
        this._active_window = index;
    }
    get active_window() {
        return this._active_window;
    }
    send_query() {
        if (this.search_form.valid) {
            this.search.set_query(this.search_form.value.query);
            localStorage.setItem('last_search_term', this.search_form.value.query);
            this.router.navigate(['user/search']);
        }
    }
    visit_window(window, option) {
        this.product_service.set_page_variables(this.windows_list[window].name);
        this.router.navigate(['user/list']);
    }
    logout_handler() {
        //this.auth.sign_out()
    }
    toggle_sidebar() {
        this.action_service.toggle_sidebar();
    }
};
NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navigation',
        template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/user/components/navigation/navigation.component.html"),
        styles: [__webpack_require__(/*! ./navigation.component.css */ "./src/app/user/components/navigation/navigation.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
        src_app_core_registration_authentication_service__WEBPACK_IMPORTED_MODULE_3__["RegistrationService"], _services_search_handler_service__WEBPACK_IMPORTED_MODULE_5__["SearchHandlerService"],
        _services_sidebar_menu_service__WEBPACK_IMPORTED_MODULE_4__["SidebarMenuService"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], src_app_shared_action_service__WEBPACK_IMPORTED_MODULE_8__["ActionService"]])
], NavigationComponent);



/***/ }),

/***/ "./src/app/user/components/negotiate/negotiate.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/user/components/negotiate/negotiate.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#offer-title{\r\n    font-size: 15px;\r\n    font-weight: 300;\r\n    margin-top: 15px;\r\n}\r\n\r\n#negotiate-title{\r\n    font-size: 15px;\r\n    font-weight: 300;\r\n}\r\n\r\n.input-item{\r\n    position: relative;\r\n}\r\n\r\n.input-item input{\r\n    position: absolute;\r\n    right: 5px;\r\n    width: 100px;\r\n    border : none;\r\n    border-bottom: 1px solid gainsboro;\r\n    background-color: inherit;\r\n    font-size: 13px;\r\n    font-weight: 300;\r\n    text-align: right;\r\n}\r\n\r\n.input-item label{\r\n    font-size: 12px;\r\n    font-weight: 300;\r\n}\r\n\r\n.input-item input:focus{\r\n    outline: none;\r\n}\r\n\r\n#quantity-input:focus, offer-input:focus{\r\n    border-bottom: 1px solid lightskyblue;\r\n}\r\n\r\n#submit-button-wrapper{\r\n    height: 25px;\r\n    margin-top : 30px;\r\n    text-align: center;\r\n    \r\n}\r\n\r\n#submit-button{\r\n    height: inherit;\r\n    font-size: 14px;\r\n    font-weight: 300;\r\n    line-height: 0;\r\n    border-radius: 20px;\r\n    border: none;\r\n    padding: 5px 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL25lZ290aWF0ZS9uZWdvdGlhdGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtJQUNaLGFBQWE7SUFDYixrQ0FBa0M7SUFDbEMseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxxQ0FBcUM7QUFDekM7O0FBRUE7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGtCQUFrQjs7QUFFdEI7O0FBQ0E7SUFDSSxlQUFlO0lBQ2YsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixpQkFBaUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC91c2VyL2NvbXBvbmVudHMvbmVnb3RpYXRlL25lZ290aWF0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI29mZmVyLXRpdGxle1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuXHJcbiNuZWdvdGlhdGUtdGl0bGV7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG59XHJcblxyXG4uaW5wdXQtaXRlbXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLmlucHV0LWl0ZW0gaW5wdXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogNXB4O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgYm9yZGVyIDogbm9uZTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBnYWluc2Jvcm87XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBpbmhlcml0O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG4uaW5wdXQtaXRlbSBsYWJlbHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbn1cclxuXHJcbi5pbnB1dC1pdGVtIGlucHV0OmZvY3Vze1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuI3F1YW50aXR5LWlucHV0OmZvY3VzLCBvZmZlci1pbnB1dDpmb2N1c3tcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodHNreWJsdWU7XHJcbn1cclxuXHJcbiNzdWJtaXQtYnV0dG9uLXdyYXBwZXJ7XHJcbiAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICBtYXJnaW4tdG9wIDogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIFxyXG59XHJcbiNzdWJtaXQtYnV0dG9ue1xyXG4gICAgaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHBhZGRpbmc6IDVweCAxMHB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/user/components/negotiate/negotiate.component.html":
/*!********************************************************************!*\
  !*** ./src/app/user/components/negotiate/negotiate.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"prior_negotiations;\">\n  <h6 style=\"font-size: 15; font-weight: 300\">negotiations</h6>\n  <ul>\n    <li *ngFor=\"let entry of prior_negotiations.products; index as i\" [id]=\"'neg-'+i\">\n      <div style=\"position:relative;\">\n        <div style=\"font-size: 13px; font-weight: 250; display: inline;\">{{entry.title}}</div>\n        <div style=\"font-size: 11px; position: absolute; right : 10px; top: 20%; display: inline-block\">\n          <div style=\"width : 40px; overflow: hidden; text-overflow: ellipsis; float:left; white-space: nowrap;\">\n            {{entry.shop}}</div>\n          <span class=\" fa fa-square\" style=\"color : red; margin: 0 10px;\"></span>\n          <span class=\"fa fa-pencil\" style=\"cursor: pointer\"\n            (click)=\"change_negotiation('neg-'+i, entry.sku, entry.shop, entry.quantity, entry.offer)\"></span>\n        </div>\n      </div>\n    </li>\n  </ul>\n</div>\n<form [formGroup]=\"negotiate\" (ngSubmit)=\"form_handler()\">\n  <fieldset>\n    <div class=\"input-item\">\n      <label for=\"initial-price\">price</label>\n      <input id=\"initial-price\" type=\"text\" [value]=\"negotiation_item$? negotiation_item$.price : ''\" readonly>\n    </div>\n    <div id=\"offer-wrapper\">\n      <div id=\"offer-title\">offer</div>\n      <div class=\"input-item\">\n        <label for=\"offer-input\">amount</label>\n        <input id=\"offer-input\" type=\"number\" formControlName=\"offer\">\n      </div>\n      <div class=\"input-item\">\n        <label for=\"quantity-input\">qty</label>\n        <input id=\"quantity-input\" type=\"number\" formControlName=\"quantity\">\n      </div>\n\n      <div *ngIf=\"form_result\" style=\"font-size: 12px; font-weight: 250; text-align: center\">\n        <div *ngIf=\"form_result.status; else failed\"\n          style=\"font-size: 12px; font-weight: 250; color: rgb(10, 126, 10); margin-top: 10px\">\n          offer {{form_result.message}}, wait for response\n        </div>\n        <ng-template>\n          <div #failed style=\"color: red; margin-top: 10px\">could not place offer. Retry</div>\n        </ng-template>\n      </div>\n\n      <div id=\"submit-button-wrapper\">\n        <input id=\"submit-button\" type=\"submit\" value=\"proceed\" [disabled]=\"quantity.invalid || offer.invalid\">\n      </div>\n\n    </div>\n\n  </fieldset>\n</form>"

/***/ }),

/***/ "./src/app/user/components/negotiate/negotiate.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/user/components/negotiate/negotiate.component.ts ***!
  \******************************************************************/
/*! exports provided: NegotiateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NegotiateComponent", function() { return NegotiateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_user_services_transaction_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/user/services/transaction.service */ "./src/app/user/services/transaction.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");






let NegotiateComponent = class NegotiateComponent {
    constructor(fb, http, transact) {
        this.fb = fb;
        this.http = http;
        this.transact = transact;
        this.dbUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].dbUrl;
    }
    ngOnInit() {
        this.negotiate = this.fb.group({
            sku: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            offer: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            quantity: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            shop: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            type: ['new']
        });
        this.transact.negotiation_item.subscribe((res) => {
            this.negotiation_item$ = res;
        });
        this.fetch_prior_negotiations();
    }
    get sku() {
        return this.negotiate.get('sku');
    }
    get offer() {
        return this.negotiate.get('offer');
    }
    get quantity() {
        return this.negotiate.get('quantity');
    }
    get shop() {
        return this.negotiate.get('shop');
    }
    get type() {
        return this.negotiate.get('type');
    }
    form_handler() {
        if (this.negotiate.value.type === 'new') {
            this.sku.setValue(this.negotiation_item$.sku);
            this.shop.setValue(this.negotiation_item$.shop);
            if (this.negotiate.valid) {
                this.http.post(this.dbUrl + '/negotiate/new', this.negotiate.value, { withCredentials: true })
                    .subscribe((response) => {
                    response.message = 'set';
                    this.form_result = response;
                    if (response.status) {
                        this.negotiate.reset();
                        setTimeout(() => {
                            this.form_result = null;
                        }, 4000);
                    }
                });
            }
        }
        else if (this.negotiate.value.type === 'counter') {
            if (this.negotiate.valid) {
                this.http.post(this.dbUrl + '/negotiate/b/update', this.negotiate.value, { withCredentials: true })
                    .subscribe((response) => {
                    response.message = 'updated';
                    this.form_result = response;
                    if (response.status) {
                        this.negotiate.reset();
                        setTimeout(() => {
                            this.form_result = null;
                        }, 4000);
                    }
                });
            }
        }
    }
    fetch_prior_negotiations() {
        this.http.get(this.dbUrl + '/negotiate/get', { withCredentials: true }).subscribe((rs) => {
            if (rs) {
                this.prior_negotiations = rs;
            }
        });
    }
    change_negotiation(parent, sku, shop, quantity, offer) {
        this.negotiate.reset();
        this.show_form = true;
        this.sku.setValue(sku);
        this.shop.setValue(shop);
        this.quantity.setValue(quantity);
        this.offer.setValue(offer);
        this.type.setValue('counter');
        var form = document.getElementById('negotiate-outer-wrapper');
        document.getElementById(parent).appendChild(form);
    }
};
NegotiateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-negotiate',
        template: __webpack_require__(/*! ./negotiate.component.html */ "./src/app/user/components/negotiate/negotiate.component.html"),
        styles: [__webpack_require__(/*! ./negotiate.component.css */ "./src/app/user/components/negotiate/negotiate.component.css")]
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_app_user_services_transaction_service__WEBPACK_IMPORTED_MODULE_4__["TransactionService"]])
], NegotiateComponent);



/***/ }),

/***/ "./src/app/user/components/order-form/order-form.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/user/components/order-form/order-form.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#form-box {\r\n    padding: 10px 7px;\r\n}\r\n\r\n.form-error-msg{\r\n    color: red;\r\n    font-size: 12px;\r\n}\r\n\r\n.form-success-msg{\r\n    color: green;\r\n    font-size: 18px;\r\n}\r\n\r\n.title-wrapper{\r\n    width : 70%;\r\n    display: inline-block;\r\n}\r\n\r\n.title-wrapper input{\r\n    border: none;\r\n    font-weight: 200;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    width: 100%;\r\n}\r\n\r\n.quantity-wrapper{\r\n    width : 20%;\r\n    display: inline-block;\r\n}\r\n\r\n.quantity-wrapper input{\r\n    width: 100%;\r\n}\r\n\r\n.delete-wrapper{\r\n    width : 10%;\r\n    display: inline-block;\r\n}\r\n\r\n.delete-wrapper button{\r\n    padding: 0;\r\n    width: 100%;\r\n}\r\n\r\n.delete:hover{\r\n    color: red;\r\n}\r\n\r\n#total-cost-wrapper{\r\n    position: relative;\r\n}\r\n\r\n#total-cost-input{\r\n    position: absolute;\r\n    right: 0;\r\n    width: 100px;\r\n}\r\n\r\n#address-info{\r\n    position: relative;\r\n    margin-top: 24px;\r\n}\r\n\r\n#city-input, #erf, #street{\r\n    width: 100%;\r\n}\r\n\r\n#street-wrapper{\r\n    width: 70%;\r\n    float: left;\r\n}\r\n\r\n#erf-wrapper{\r\n    width: 30%;\r\n    float: right;\r\n}\r\n\r\n#submit{\r\n    width: 100%;\r\n    margin-top: 20px;\r\n}\r\n\r\nul{\r\n    list-style: none;\r\n    padding : 0;\r\n}\r\n\r\n.order-items-list-hidden{\r\n    height: 0;\r\n    overflow: hidden;\r\n}\r\n\r\n.order-instance-title:hover, #orders-header:hover{\r\n    cursor: pointer;\r\n}\r\n\r\n.order-list-closed{\r\n    height: 0;\r\n    overflow: hidden;\r\n    margin: 0;\r\n}\r\n\r\n.order-instance{\r\n    margin: 0 10px;\r\n}\r\n\r\n.order-item{\r\n    margin-left: 10px\r\n}\r\n\r\n#form_title{\r\n    text-align: center;\r\n    font-weight: 300;\r\n    font-size: 15px;\r\n\r\n}\r\n\r\n.form_number{\r\n    text-align: center;\r\n    font-weight: 300;\r\n    font-size: 14px;\r\n    margin-bottom: 15px;\r\n}\r\n\r\n#address_inputs_wrapper{\r\n    font-weight : 240;\r\n    font-size : 14px;\r\n}\r\n\r\n#orders-header{\r\n    position: relative;\r\n    font-size: 15px;\r\n    font-weight: 300;\r\n    background-color:whitesmoke;\r\n    margin: 0;\r\n    height: 30px;\r\n    border-bottom: 1px solid gainsboro;    \r\n}\r\n\r\n.order-items-list-hidden{\r\n    display: none;\r\n    overflow: hidden;\r\n}\r\n\r\n.order-instance-title, #orders-header{\r\n    cursor: pointer;\r\n}\r\n\r\n.order-list-closed{\r\n    display: none;\r\n}\r\n\r\n.order-instance{\r\n    margin: 0 10px;\r\n}\r\n\r\n.order-item{\r\n    margin-left: 10px;\r\n    font-weight: 250;\r\n    font-size: 12px;\r\n}\r\n\r\n.order-instance-title{\r\n    font-weight: 290;\r\n    font-size: 13px;\r\n    position: relative;\r\n}\r\n\r\n#orders-wrapper .fa-plus-square-o {\r\n    position: absolute;\r\n    right: 10px;\r\n    border-radius: 3px;\r\n    padding: 4px;\r\n}\r\n\r\n#orders-wrapper .fa:hover, #orders-wrapper .fa:active{\r\n    background-color: rgb(228, 225, 225);\r\n    transition: 2.s linear\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL29yZGVyLWZvcm0vb3JkZXItZm9ybS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksVUFBVTtJQUNWLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osZUFBZTtBQUNuQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQix1QkFBdUI7SUFDdkIsV0FBVztBQUNmOztBQUVBO0lBQ0ksV0FBVztJQUNYLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsV0FBVztBQUNmOztBQUVBO0lBQ0ksVUFBVTtBQUNkOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFVBQVU7SUFDVixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztBQUNmOztBQUVBO0lBQ0ksU0FBUztJQUNULGdCQUFnQjtBQUNwQjs7QUFDQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxTQUFTO0lBQ1QsZ0JBQWdCO0lBQ2hCLFNBQVM7QUFDYjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBRUE7SUFDSTtBQUNKOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlOztBQUVuQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQiwyQkFBMkI7SUFDM0IsU0FBUztJQUNULFlBQVk7SUFDWixrQ0FBa0M7QUFDdEM7O0FBR0E7SUFDSSxhQUFhO0lBQ2IsZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksb0NBQW9DO0lBQ3BDO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC91c2VyL2NvbXBvbmVudHMvb3JkZXItZm9ybS9vcmRlci1mb3JtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjZm9ybS1ib3gge1xyXG4gICAgcGFkZGluZzogMTBweCA3cHg7XHJcbn1cclxuXHJcbi5mb3JtLWVycm9yLW1zZ3tcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuXHJcbi5mb3JtLXN1Y2Nlc3MtbXNne1xyXG4gICAgY29sb3I6IGdyZWVuO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG59XHJcblxyXG4udGl0bGUtd3JhcHBlcntcclxuICAgIHdpZHRoIDogNzAlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG4udGl0bGUtd3JhcHBlciBpbnB1dHtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGZvbnQtd2VpZ2h0OiAyMDA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnF1YW50aXR5LXdyYXBwZXJ7XHJcbiAgICB3aWR0aCA6IDIwJTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLnF1YW50aXR5LXdyYXBwZXIgaW5wdXR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmRlbGV0ZS13cmFwcGVye1xyXG4gICAgd2lkdGggOiAxMCU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5kZWxldGUtd3JhcHBlciBidXR0b257XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5kZWxldGU6aG92ZXJ7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4jdG90YWwtY29zdC13cmFwcGVye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4jdG90YWwtY29zdC1pbnB1dHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG59XHJcblxyXG4jYWRkcmVzcy1pbmZve1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogMjRweDtcclxufVxyXG5cclxuI2NpdHktaW5wdXQsICNlcmYsICNzdHJlZXR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuI3N0cmVldC13cmFwcGVye1xyXG4gICAgd2lkdGg6IDcwJTtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4jZXJmLXdyYXBwZXJ7XHJcbiAgICB3aWR0aDogMzAlO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4jc3VibWl0e1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcblxyXG51bHtcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICBwYWRkaW5nIDogMDtcclxufVxyXG5cclxuLm9yZGVyLWl0ZW1zLWxpc3QtaGlkZGVue1xyXG4gICAgaGVpZ2h0OiAwO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4ub3JkZXItaW5zdGFuY2UtdGl0bGU6aG92ZXIsICNvcmRlcnMtaGVhZGVyOmhvdmVye1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4ub3JkZXItbGlzdC1jbG9zZWR7XHJcbiAgICBoZWlnaHQ6IDA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4ub3JkZXItaW5zdGFuY2V7XHJcbiAgICBtYXJnaW46IDAgMTBweDtcclxufVxyXG5cclxuLm9yZGVyLWl0ZW17XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweFxyXG59XHJcblxyXG4jZm9ybV90aXRsZXtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcblxyXG59XHJcblxyXG4uZm9ybV9udW1iZXJ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxufVxyXG5cclxuI2FkZHJlc3NfaW5wdXRzX3dyYXBwZXJ7XHJcbiAgICBmb250LXdlaWdodCA6IDI0MDtcclxuICAgIGZvbnQtc2l6ZSA6IDE0cHg7XHJcbn1cclxuXHJcbiNvcmRlcnMtaGVhZGVye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6d2hpdGVzbW9rZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBnYWluc2Jvcm87ICAgIFxyXG59XHJcblxyXG5cclxuLm9yZGVyLWl0ZW1zLWxpc3QtaGlkZGVue1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuLm9yZGVyLWluc3RhbmNlLXRpdGxlLCAjb3JkZXJzLWhlYWRlcntcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLm9yZGVyLWxpc3QtY2xvc2Vke1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLm9yZGVyLWluc3RhbmNle1xyXG4gICAgbWFyZ2luOiAwIDEwcHg7XHJcbn1cclxuXHJcbi5vcmRlci1pdGVte1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICBmb250LXdlaWdodDogMjUwO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG4ub3JkZXItaW5zdGFuY2UtdGl0bGV7XHJcbiAgICBmb250LXdlaWdodDogMjkwO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4jb3JkZXJzLXdyYXBwZXIgLmZhLXBsdXMtc3F1YXJlLW8ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICBwYWRkaW5nOiA0cHg7XHJcbn1cclxuXHJcbiNvcmRlcnMtd3JhcHBlciAuZmE6aG92ZXIsICNvcmRlcnMtd3JhcHBlciAuZmE6YWN0aXZle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIyOCwgMjI1LCAyMjUpO1xyXG4gICAgdHJhbnNpdGlvbjogMi5zIGxpbmVhclxyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/user/components/order-form/order-form.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/user/components/order-form/order-form.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"orders-wrapper\">\n  <h6 id=\"orders-header\">\n    <span (click)=\"toggle_panel()\">orders</span>\n    <span (click)=\"move_form('form-area')\" class=\"fa fa-plus-square-o\"></span>\n  </h6>\n  <div id=\"form-area\">\n    <form id=\"order-form\" [formGroup]=\"order_form\" style=\"width: 100%; margin-bottom: 20px\"\n      (ngSubmit)=\"order_handler()\">\n      <div id=\"form_title\">order form</div>\n      <div class=\"form_number\" *ngIf=\"order_number.value; else no_title\">#{{order_number.value}}</div>\n      <ng-template #no_title>\n        <div class=\"form_number\">new form</div>\n      </ng-template>\n      <div *ngIf=\" item_forms.value.length ; else no_products\">\n        <div formArrayName=\"items\" style=\"margin-bottom: 10px;\">\n          <div *ngFor=\"let item of item_forms.controls; let i=index\" [formGroupName]=\"i\" style=\"margin-bottom: 7px;\">\n            <div *ngIf=\"transact.item_forms.value[i].title\">\n              <div class=\"title-wrapper\">\n                <input type=\"text\" formControlName=\"title\" readonly style=\"background-color: inherit\" />\n              </div>\n              <div class=\"quantity-wrapper\">\n                <input type=\"number\" min=1 placeholder=\"quantity\" formControlName=\"quantity\" (click)=\"calculate_total()\" />\n              </div>\n              <div class=\"delete-wrapper\">\n                <button (click)=\"remove_item(i)\"><span class=\"fa fa-trash\"></span></button>\n              </div>\n            </div>\n          </div>\n\n        </div>\n        <div id=\"total-cost-wrapper\">\n          <span>total</span>\n          <input type=\"text\" name=\"total\" [value]=\"total\" id=\"total-cost-input\" />\n        </div>\n      </div>\n      <ng-template #no_products>\n        <div style=\"height: 120px; text-align: center; padding : 20px 5px;\n               background-color: rgb(251, 248, 248); font-size: 13px; font-weight: 200; color : gray\">\n          <div>click on the product you wish to purchase</div>\n          <div>It'll be automatically added here</div>\n        </div>\n      </ng-template>\n\n\n      <div id=\"address-info\">\n        <fieldset formGroupName=\"billing_address\">\n          <legend style=\"font-size: 15px; font-weight: 240;\">billing address</legend>\n          <div id=\"address_inputs_wrapper\">\n            <div style=\"margin-bottom: 16px; \">\n              <input type=\"text\" placeholder=\"city\" id=\"city-input\" formControlName=\"city\" />\n              <div *ngIf=\"city.invalid && city.touched\" class=\"form-error-msg\">billing address is required</div>\n            </div>\n\n            <div>\n              <div id=\"street-wrapper\">\n                <input type=\"text\" placeholder=\"street\" formControlName=\"street\" id=\"street\" />\n                <div *ngIf=\"street.touched && street.invalid\" class=\"form-error-msg\">*required</div>\n              </div>\n              <div id=\"erf-wrapper\">\n                <input type=\"text\" placeholder=\"erf\" formControlName=\"erf\" id=\"erf\" />\n              <div *ngIf=\"erf.touched && erf.invalid\" class=\"form-error-msg\">*required</div>\n              </div>\n            </div>\n\n          </div>\n        </fieldset>\n        <div style=\"margin-top: 15px\">\n          <input style=\"border: 1px solid gainsboro; \n                width: 100%; font-size: 13; font-weight: 240; text-align: center;\" placeholder=\"password\" type=\"password\"\n            formControlName=\"password\">\n          <div *ngIf=\"password.touched && password.invalid\" class=\"form-error-msg\">password is required</div>\n        </div>\n        <div>\n          <div *ngIf=\"order_feedback.set\" [ngClass]=\"order_feedback.status?'form-success-msg':'form-error-msg'\"\n            style=\"position: relative; font-size: 12px; text-align: center; top: 10px\">{{order_feedback.message}}</div>\n          <input type=\"submit\" [disabled]=\"!order_form.valid\" value=\"complete purchase\" id=\"submit\" />\n        </div>\n      </div>\n    </form>\n  </div>\n\n\n  <ul *ngIf=\"orders\" [class.order-list-closed]=\"panel_state\">\n    <li *ngFor=\"let order of orders.orders;\" class=\"order-instance\">\n      <div class=\"order-instance-title\">\n        <span (click)=\"toggle_order_items()\"># : {{order.order_number}}</span>\n        <span class=\"fa fa-plus-square-o\" title=\"add item to form\"\n          (click)=\"move_form(order.order_number, order.order_number, order.billing_address)\"></span>\n      </div>\n      <div [class.order-items-list-hidden]=\"items_state\">\n        <ul>\n          <li class=\"order-item\" *ngFor=\"let item of order.order_items\">\n            {{item.sku}} &nbsp; <span>quantity :{{item.quantity}}</span>\n          </li>\n        </ul>\n      </div>\n      <div [id]=\"order.order_number\">\n\n      </div>\n\n    </li>\n  </ul>\n\n</div>"

/***/ }),

/***/ "./src/app/user/components/order-form/order-form.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/user/components/order-form/order-form.component.ts ***!
  \********************************************************************/
/*! exports provided: OrderFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderFormComponent", function() { return OrderFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_user_services_transaction_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/user/services/transaction.service */ "./src/app/user/services/transaction.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");





let OrderFormComponent = class OrderFormComponent {
    constructor(fb, transact, http) {
        this.fb = fb;
        this.transact = transact;
        this.http = http;
        this.order_form = this.transact.order_form;
        this.order_feedback = { status: false, message: '', set: false };
        this.total = 0;
    }
    ngOnInit() {
        this.order_form = this.transact.order_form;
        this.fetch_orders();
    }
    get item_forms() {
        return this.order_form.get('items');
    }
    order_handler() {
        this.transact.order_processor(this.order_form).subscribe((res) => {
            if (res.status === true) {
                this.order_form.reset();
                this.total = 0;
            }
            this.order_feedback.message = res.message;
            this.order_feedback.status = res.status;
            this.order_feedback.set = true;
            setTimeout(() => {
                this.order_feedback.set = false;
            }, 2000);
        });
    }
    remove_item(i) {
        this.total -= this.order_form.value.items[i].qty * this.order_form.value.items[i].price; //decreaments total when item is removed
        this.transact.item_forms.removeAt(i);
    }
    calculate_total() {
        var total = 0;
        this.order_form.value.items.forEach((value) => {
            total += value.quantity * value.price;
        });
        this.total = total;
    }
    get city() {
        return this.order_form.get('billing_address').get('city');
    }
    get street() {
        return this.order_form.get('billing_address').get('street');
    }
    get erf() {
        return this.order_form.get('billing_address').get('erf');
    }
    get order_number() {
        return this.order_form.get('order_number');
    }
    get password() {
        return this.order_form.get('password');
    }
    activate_form(number, address) {
        this.transact.clear_form();
        this.transact.change_order_number(number);
        this.transact.change_order_address(address);
    }
    fetch_orders() {
        this.http.get('http://localhost:4000/orders/get', { withCredentials: true })
            .subscribe((response) => {
            this.orders = response;
        });
    }
    reset_form() {
        this.transact.clear_form();
    }
    toggle_order_items() {
        this.items_state = !this.items_state;
    }
    toggle_panel() {
        this.panel_state = !this.panel_state;
    }
    move_form(id, order_no, address) {
        var form = document.getElementById('order-form');
        document.getElementById(id).appendChild(form);
        if (order_no && address) {
            this.activate_form(order_no, address);
            this.toggle_order_items();
        }
        this.reset_form();
    }
};
OrderFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-form',
        template: __webpack_require__(/*! ./order-form.component.html */ "./src/app/user/components/order-form/order-form.component.html"),
        styles: [__webpack_require__(/*! ./order-form.component.css */ "./src/app/user/components/order-form/order-form.component.css")]
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], src_app_user_services_transaction_service__WEBPACK_IMPORTED_MODULE_3__["TransactionService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
], OrderFormComponent);



/***/ }),

/***/ "./src/app/user/components/product-view/product-view.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/user/components/product-view/product-view.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#view-angle-grid{\r\n  display: grid;\r\n  grid-template-columns : 100%;\r\n  grid-template-rows : 20px 1fr 20px;\r\n  grid-template-areas:  \"slide-up\"\r\n                        \"view-angle-inner\"\r\n                        \"slide-down\"\r\n}\r\n\r\n.view-angle-inner{\r\n  grid-area: view-angle-inner;\r\n  overflow: hidden;\r\n  background-color: rgb(231, 231, 231);\r\n  max-height: 197px;\r\n}\r\n\r\n#view-angle-outer{\r\n  max-width: 100px; \r\n  width: 20%; \r\n  height: 100%; \r\n  float: left;\r\n}\r\n\r\n#image-wrapper{\r\n  height: 238px;\r\n  display: inline-block;\r\n}\r\n\r\n#product-info{\r\n  position: relative;\r\n  padding : 10px;\r\n  overflow: hidden;\r\n}\r\n\r\n.slide-up{\r\n  grid-area: slide-up;\r\n  text-align: center;\r\n  background-color: rgb(231, 230, 230);\r\n  border-radius: 4px 4px 0px 0px\r\n\r\n}\r\n\r\n.slide-down{\r\n  grid-area: slide-down;\r\n  text-align: center;\r\n  background-color: rgb(231, 230, 230);\r\n  border-radius: 0px 0px 4px 4px\r\n}\r\n\r\n.view-angle-image-wrapper img {\r\n  width : 100%;\r\n  height: auto;\r\n}\r\n\r\n.view-angle-image-wrapper{\r\n  position: relative;\r\n  transition: .2s linear;\r\n}\r\n\r\n#main-image-wrapper{\r\n  float: left;\r\n  width: 80%;\r\n  height: 100%;\r\n  max-width: 300px;\r\n}\r\n\r\n#main-image-wrapper img{\r\n  height: 100%;\r\n  width: 100%;\r\n}\r\n\r\n.product-description{\r\n  text-align: center;\r\n\r\n}\r\n\r\n.title, .price, .shop {\r\n  margin: 7px 0px;\r\n}\r\n\r\n.title{\r\n  font-size: 18px;\r\n}\r\n\r\n.price{\r\n  font-size: 15px;\r\n  float: left;\r\n\r\n}\r\n\r\n.shop{\r\n  font-size: 13px;\r\n  float: right;\r\n}\r\n\r\n.purchase{\r\n  position: absolute;\r\n  right: 10px;\r\n  bottom: 5px;\r\n}\r\n\r\n.summary{\r\n  clear: both;\r\n}\r\n\r\n.summary p {\r\n  max-height: 55px;\r\n  overflow: hidden;\r\n  text-overflow: ellipsis;\r\n}\r\n\r\nsection {\r\n  margin: 30px 0px;\r\n  float: center;\r\n}\r\n\r\n@media screen and (min-width : 559px){\r\n  .shop{\r\n    float: right;\r\n  }\r\n\r\n  #product-info{\r\n    width: calc(100% - 430px);\r\n    float: right;\r\n  }\r\n \r\n}\r\n\r\n@media screen and (min-width : 924px){\r\n\r\n  .shop{\r\n    float: none;\r\n  }\r\n\r\n  .price{\r\n    float: none;\r\n  }\r\n \r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL3Byb2R1Y3Qtdmlldy9wcm9kdWN0LXZpZXcuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYiw0QkFBNEI7RUFDNUIsa0NBQWtDO0VBQ2xDOzs7QUFHRjs7QUFFQTtFQUNFLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsb0NBQW9DO0VBQ3BDLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsWUFBWTtFQUNaLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGFBQWE7RUFDYixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsb0NBQW9DO0VBQ3BDOztBQUVGOztBQUVBO0VBQ0UscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixvQ0FBb0M7RUFDcEM7QUFDRjs7QUFFQTtFQUNFLFlBQVk7RUFDWixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UsV0FBVztFQUNYLFVBQVU7RUFDVixZQUFZO0VBQ1osZ0JBQWdCO0FBQ2xCOztBQUdBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGtCQUFrQjs7QUFFcEI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUNBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixXQUFXOztBQUViOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsV0FBVztBQUNiOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQix1QkFBdUI7QUFDekI7O0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtBQUNmOztBQUVBO0VBQ0U7SUFDRSxZQUFZO0VBQ2Q7O0VBRUE7SUFDRSx5QkFBeUI7SUFDekIsWUFBWTtFQUNkOztBQUVGOztBQUVBOztFQUVFO0lBQ0UsV0FBVztFQUNiOztFQUVBO0lBQ0UsV0FBVztFQUNiOztBQUVGIiwiZmlsZSI6InNyYy9hcHAvdXNlci9jb21wb25lbnRzL3Byb2R1Y3Qtdmlldy9wcm9kdWN0LXZpZXcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiN2aWV3LWFuZ2xlLWdyaWR7XHJcbiAgZGlzcGxheTogZ3JpZDtcclxuICBncmlkLXRlbXBsYXRlLWNvbHVtbnMgOiAxMDAlO1xyXG4gIGdyaWQtdGVtcGxhdGUtcm93cyA6IDIwcHggMWZyIDIwcHg7XHJcbiAgZ3JpZC10ZW1wbGF0ZS1hcmVhczogIFwic2xpZGUtdXBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcInZpZXctYW5nbGUtaW5uZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcInNsaWRlLWRvd25cIlxyXG59XHJcblxyXG4udmlldy1hbmdsZS1pbm5lcntcclxuICBncmlkLWFyZWE6IHZpZXctYW5nbGUtaW5uZXI7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjMxLCAyMzEsIDIzMSk7XHJcbiAgbWF4LWhlaWdodDogMTk3cHg7XHJcbn1cclxuXHJcbiN2aWV3LWFuZ2xlLW91dGVye1xyXG4gIG1heC13aWR0aDogMTAwcHg7IFxyXG4gIHdpZHRoOiAyMCU7IFxyXG4gIGhlaWdodDogMTAwJTsgXHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbiNpbWFnZS13cmFwcGVye1xyXG4gIGhlaWdodDogMjM4cHg7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG4jcHJvZHVjdC1pbmZve1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBwYWRkaW5nIDogMTBweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4uc2xpZGUtdXB7XHJcbiAgZ3JpZC1hcmVhOiBzbGlkZS11cDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIzMSwgMjMwLCAyMzApO1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweCA0cHggMHB4IDBweFxyXG5cclxufVxyXG5cclxuLnNsaWRlLWRvd257XHJcbiAgZ3JpZC1hcmVhOiBzbGlkZS1kb3duO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjMxLCAyMzAsIDIzMCk7XHJcbiAgYm9yZGVyLXJhZGl1czogMHB4IDBweCA0cHggNHB4XHJcbn1cclxuXHJcbi52aWV3LWFuZ2xlLWltYWdlLXdyYXBwZXIgaW1nIHtcclxuICB3aWR0aCA6IDEwMCU7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG59XHJcblxyXG4udmlldy1hbmdsZS1pbWFnZS13cmFwcGVye1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0cmFuc2l0aW9uOiAuMnMgbGluZWFyO1xyXG59XHJcblxyXG4jbWFpbi1pbWFnZS13cmFwcGVye1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbn1cclxuXHJcblxyXG4jbWFpbi1pbWFnZS13cmFwcGVyIGltZ3tcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5wcm9kdWN0LWRlc2NyaXB0aW9ue1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbn1cclxuXHJcbi50aXRsZSwgLnByaWNlLCAuc2hvcCB7XHJcbiAgbWFyZ2luOiA3cHggMHB4O1xyXG59XHJcbi50aXRsZXtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbi5wcmljZXtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcblxyXG59XHJcblxyXG4uc2hvcHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4ucHVyY2hhc2V7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAxMHB4O1xyXG4gIGJvdHRvbTogNXB4O1xyXG59XHJcblxyXG4uc3VtbWFyeXtcclxuICBjbGVhcjogYm90aDtcclxufVxyXG5cclxuLnN1bW1hcnkgcCB7XHJcbiAgbWF4LWhlaWdodDogNTVweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG59XHJcblxyXG5zZWN0aW9uIHtcclxuICBtYXJnaW46IDMwcHggMHB4O1xyXG4gIGZsb2F0OiBjZW50ZXI7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA1NTlweCl7XHJcbiAgLnNob3B7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgfVxyXG5cclxuICAjcHJvZHVjdC1pbmZve1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDQzMHB4KTtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICB9XHJcbiBcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDkyNHB4KXtcclxuXHJcbiAgLnNob3B7XHJcbiAgICBmbG9hdDogbm9uZTtcclxuICB9XHJcblxyXG4gIC5wcmljZXtcclxuICAgIGZsb2F0OiBub25lO1xyXG4gIH1cclxuIFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/user/components/product-view/product-view.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/user/components/product-view/product-view.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n <div *ngIf=\"(product$ | async) as product\">\r\n    <div id=\"image-wrapper\">\r\n      <div id=\"view-angle-outer\">\r\n          <div id=\"view-angle-grid\">\r\n              <div class=\"slide-up\" (click)=\"slide_up()\">\r\n                <span class=\"fa fa-caret-up\"></span>\r\n              </div>\r\n              <div class=\"view-angle-inner\">\r\n                <div *ngFor=\"let item of items\" class=\"view-angle-image-wrapper\" [style.top]=\"current_item_position+'px'\">\r\n                <img src=\"{{dbUrl}}/file/{{item}}\" class=\"image\" alt=\"\">\r\n                </div>\r\n              </div>\r\n              <div class=\"slide-down\" (click)=\"slide_down()\">\r\n                <span class=\"fa fa-caret-down\"></span>\r\n              </div>\r\n            </div>\r\n      </div>\r\n      <div id=\"main-image-wrapper\">\r\n      <img src=\"{{dbUrl}}/file/{{product.display_image}}\">\r\n      </div>\r\n    </div>\r\n    <div id=\"product-info\">\r\n      <div class=\"title\">{{product.title}}</div>\r\n      <div class=\"price\">{{product.list_price}}</div>\r\n      <div class=\"shop\">{{product.retailer}}</div>\r\n      <div class=\"summary\">\r\n        <div>Summary</div>\r\n        <p>\r\n          It is a long established fact that a\r\n          reader will be distracted by the readable\r\n          content of a page when looking at its layout.\r\n          The point of using Lorem Ipsum is that it has a\r\n        </p>\r\n      </div>\r\n      <div class=\"purchase\">\r\n        <app-transact [product]=\"product\"></app-transact>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"additional-info\">\r\n    <div id=\"recomendations\">\r\n      <app-slider [title]=\"'electronics'\"></app-slider>\r\n    </div>\r\n    <div class=\"product-description\">\r\n      <section>\r\n        <h6>Description</h6>\r\n        <div>\r\n          Contrary to popular belief, Lorem Ipsum is not\r\n          simply random text. It has roots in a piece of\r\n          classical Latin literature from 45 BC, making it over\r\n          2000 years old. Richard McClintock, a Latin professor\r\n          at Hampden-Sydney College in Virginia, looked up one\r\n          of the more obscure Latin words, consectetur, from a\r\n          Lorem Ipsum passage, and going through the cites of\r\n          the word in classical literature, discovered the\r\n          undoubtable source. Lorem Ipsum comes from sec\r\n        </div>\r\n      </section>\r\n      <section>\r\n        <h5>Manufacturer info</h5>\r\n        <div>\r\n          There are many variations of passages of Lorem Ipsum\r\n          available, but the majority have suffered alteration\r\n          in some form, by injected humour, or randomised words\r\n          which don't look even slightly believable. If you are\r\n          going to use a passage of Lorem Ipsum, you need to\r\n          be sure there isn't anything embarrassing hidden in\r\n          the middle of text. All the Lorem Ipsum generators\r\n          on the Internet tend to repeat predefined chunks as\r\n          necessary\r\n        </div>\r\n      </section>\r\n      <section>\r\n        <h5>Dimensions</h5>\r\n        <div>\r\n          There are many variations of passages of Lorem Ipsum\r\n          available, but the majority have suffered alteration\r\n          in some form, by injected humour, or randomised words\r\n          which don't look even slightly believable. If you are\r\n          going to use a passage of Lorem Ipsum, you need to\r\n          be sure there isn't anything embarrassing hidden in\r\n          the middle of text. All the Lorem Ipsum generators\r\n          on the Internet tend to repeat predefined chunks as\r\n          necessary\r\n        </div>\r\n      </section>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/user/components/product-view/product-view.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/user/components/product-view/product-view.component.ts ***!
  \************************************************************************/
/*! exports provided: ProductViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductViewComponent", function() { return ProductViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/user/services/product.service.ts");
/* harmony import */ var _services_sidebar_menu_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/sidebar-menu.service */ "./src/app/user/services/sidebar-menu.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");





let ProductViewComponent = class ProductViewComponent {
    constructor(product_service, sidebar_service) {
        this.product_service = product_service;
        this.sidebar_service = sidebar_service;
        this._screen_size = window.innerWidth;
        this.dbUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].dbUrl;
        this.item_height = 100;
        this.total_items_height = 100 * 9;
        this.box_height = 300;
        this.current_item_position = 0;
        this.items = ["1111.jpg", "1111.jpg", "1111.jpg", "1111.jpg", "1111.jpg", "1111.jpg"];
    }
    ngOnInit() {
        this.product_service.get_product_for_view();
        this.product$ = this.product_service.product_for_view;
    }
    slide_up() {
        if (this.current_item_position < 0) {
            this.current_item_position += this.item_height;
        }
    }
    slide_down() {
        if ((this.current_item_position * -1) < this.box_height) {
            this.current_item_position -= this.item_height;
        }
    }
    get screen_size() {
        return window.innerWidth;
    }
};
ProductViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'product-view',
        template: __webpack_require__(/*! ./product-view.component.html */ "./src/app/user/components/product-view/product-view.component.html"),
        styles: [__webpack_require__(/*! ./product-view.component.css */ "./src/app/user/components/product-view/product-view.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"],
        _services_sidebar_menu_service__WEBPACK_IMPORTED_MODULE_3__["SidebarMenuService"]])
], ProductViewComponent);



/***/ }),

/***/ "./src/app/user/components/product/product.component.css":
/*!***************************************************************!*\
  !*** ./src/app/user/components/product/product.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".slide-item{\r\n    position: relative;\r\n    display: inline-block;\r\n    align-items: center;\r\n    min-width: 145px;\r\n    width: 50%;\r\n    max-width: 200px;\r\n    max-height: inherit;\r\n    height: inherit;\r\n    perspective: 1000px;\r\n    margin: 2px 3px;\r\n    transition: all .3s linear;\r\n    text-align: center;\r\n    border-radius: 30px;\r\n  \r\n  }\r\n\r\n  .title{\r\n    display: none;\r\n  }\r\n\r\n  .product-box{\r\n    width: 100%;\r\n  }\r\n\r\n  .product-name{\r\n    display: inline-block;\r\n  }\r\n\r\n  img:hover{\r\n    cursor: pointer;\r\n  }\r\n\r\n  @media screen and (min-width : 769px){\r\n\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL3Byb2R1Y3QvcHJvZHVjdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUdyQixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLFVBQVU7SUFDVixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFFZixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLDBCQUEwQjtJQUMxQixrQkFBa0I7SUFDbEIsbUJBQW1COztFQUVyQjs7RUFFQTtJQUNFLGFBQWE7RUFDZjs7RUFFQTtJQUNFLFdBQVc7RUFDYjs7RUFHQTtJQUNFLHFCQUFxQjtFQUN2Qjs7RUFFQTtJQUNFLGVBQWU7RUFDakI7O0VBR0E7O0VBRUEiLCJmaWxlIjoic3JjL2FwcC91c2VyL2NvbXBvbmVudHMvcHJvZHVjdC9wcm9kdWN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2xpZGUtaXRlbXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIG1pbi13aWR0aDogMTQ1cHg7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgbWF4LXdpZHRoOiAyMDBweDtcclxuICAgIG1heC1oZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICBoZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICAtd2Via2l0LXBlcnNwZWN0aXZlOiAxMDAwcHg7XHJcbiAgICBwZXJzcGVjdGl2ZTogMTAwMHB4O1xyXG4gICAgbWFyZ2luOiAycHggM3B4O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC4zcyBsaW5lYXI7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gIFxyXG4gIH1cclxuXHJcbiAgLnRpdGxle1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gIC5wcm9kdWN0LWJveHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgXHJcbiAgLnByb2R1Y3QtbmFtZXtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGltZzpob3ZlcntcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcblxyXG5cclxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogNzY5cHgpe1xyXG5cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/user/components/product/product.component.html":
/*!****************************************************************!*\
  !*** ./src/app/user/components/product/product.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"slide-item\" [style.left]=\"position\" [style.width]=\"alter_width?alter_width:''\">\r\n  <div class=\"main-slide-content\">\r\n    <div class=\"product-box\">\r\n      <img \r\n        src=\"{{imgUrl}}/{{product.display_image}}?alt=media\" \r\n        id=\"item_img\" \r\n        style=\"width:100%; height: 180px;\" \r\n        (click)=\"view_product(product.sku)\"/>\r\n    </div>\r\n    <div class=\"product-name\" style=\"margin-bottom: 10px; width: 100%; overflow: hidden; text-overflow: ellipsis;\">\r\n      <span class=\"title\" style=\"float: left; overflow: hidden; text-overflow: ellipsis;\">{{product.title}}</span>\r\n    </div>\r\n    <div>\r\n      <div style=\"overflow: hidden; text-overflow: ellipsis; padding: 0 10px;\">\r\n        <span style=\"float: left\">N$ {{product.list_price}}</span> \r\n        <small style=\"float: right; overflow: hidden; text-overflow: ellipsis;\">{{product.shop}}</small>\r\n      </div>\r\n      <div style=\"float: right; padding-right: 10px;\">\r\n        <app-transact [product]=\"product\"></app-transact>\r\n      </div>\r\n    </div>\r\n  </div>\r\n   <div [style.display]=\"show_form?'block':'none'\" style=\"opacity: 0.6; background-color: black; position: absolute; top:0; height:100%; width:100%\">\r\n      <form #orderForm=ngForm (ngSubmit)=\"orderHandler(orderForm)\">\r\n        <div style=\"margin:10px 0\">\r\n          <input \r\n            type=\"number\" \r\n            style=\"border-radius: 10px; text-align: center;\" \r\n            min=\"1\" \r\n            value=\"1\"\r\n            name=\"quantity\"\r\n            ngModel>\r\n        </div>\r\n        <div>\r\n          <input \r\n            type=\"submit\" \r\n            value=\"ok\" \r\n            style=\"border-radius: 10px; border: none; width: 50px\">\r\n        </div>\r\n      </form>\r\n      <form #bidForm=ngForm (ngSubmit)=\"bidHandler(bidForm)\">\r\n        <div style=\"margin:10px 0\">\r\n          <input \r\n            type=\"number\" \r\n            style=\"border-radius: 10px; text-align: center;\" \r\n            min=\"1\" \r\n            value=\"1\"\r\n            name=\"quantity\"\r\n            ngModel>\r\n        </div>\r\n        <div style=\"margin:10px 0\">\r\n          <input \r\n            type=\"number\" \r\n            style=\"border-radius: 10px; text-align: center;\" \r\n            min=\"1\" \r\n            value=\"1\"\r\n            name=\"offer\"\r\n            placeholder=\"offer\"\r\n            ngModel>\r\n        </div>\r\n        <div>\r\n          <input \r\n            type=\"submit\" \r\n            value=\"ok\" \r\n            style=\"border-radius: 10px; border: none; width: 50px\">\r\n        </div>\r\n      </form>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/user/components/product/product.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/user/components/product/product.component.ts ***!
  \**************************************************************/
/*! exports provided: ProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductComponent", function() { return ProductComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/user/services/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_transaction_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/transaction.service */ "./src/app/user/services/transaction.service.ts");






let ProductComponent = class ProductComponent {
    constructor(product_service, router, transact) {
        this.product_service = product_service;
        this.router = router;
        this.transact = transact;
        this.width = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.imgUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].imgUrl;
        this._click_count = 0;
        this.show_form = false;
    }
    ngOnInit() {
    }
    view_product(sku) {
        ++this._click_count;
        setTimeout(() => {
            if (this._click_count == 1) {
                this.product_service.get_product_for_view(sku);
                this.router.navigate(['user/product']);
            }
            else if (this._click_count == 2) {
                this.transact.addToCartHandler('mint', 1)
                    .subscribe(({ data }) => {
                    console.log(data);
                }, error => {
                    console.log(error);
                });
            }
            else if (this._click_count > 2) {
                this.show_form = true;
            }
        }, 1000);
    }
    orderHandler(form) {
        if (form.valid) {
            this.transact.quickOrderHandler('22', 'trainwreck', form.value.quantity)
                .subscribe(({ data }) => {
                console.log(data);
            }, error => {
                console.log(error);
            });
        }
    }
    bidHandler(form) {
        if (form.valid) {
            this.transact.quickBidHandler('mint', form.value.quantity, 'trainwreck', form.value.offer)
                .subscribe(({ data }) => {
                console.log(data);
            }, error => {
                console.log(error);
            });
        }
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ProductComponent.prototype, "product", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ProductComponent.prototype, "position", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ProductComponent.prototype, "alter_width", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ProductComponent.prototype, "width", void 0);
ProductComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'product',
        template: __webpack_require__(/*! ./product.component.html */ "./src/app/user/components/product/product.component.html"),
        styles: [__webpack_require__(/*! ./product.component.css */ "./src/app/user/components/product/product.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _services_transaction_service__WEBPACK_IMPORTED_MODULE_5__["TransactionService"]])
], ProductComponent);



/***/ }),

/***/ "./src/app/user/components/search/search.component.css":
/*!*************************************************************!*\
  !*** ./src/app/user/components/search/search.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".results-header{\r\n  margin: 0 auto;\r\n  background-color: #119ebd;\r\n  display: flex;\r\n  justify-content: center;\r\n}\r\n\r\n.search-result{\r\n  position: relative;\r\n  height: 20vh;\r\n  max-height: 200px;\r\n  min-height: 120px;\r\n  margin: 10px 0;\r\n}\r\n\r\n.result-image{\r\n  height: 100%;\r\n  width: 30vw;\r\n  max-width: 200px;\r\n  min-width: 130px;\r\n  background-color: rgb(170, 170, 170);\r\n  float: left;\r\n  margin-right: 10px;\r\n  border-radius: 6px;\r\n  overflow: hidden;\r\n}\r\n\r\n.result-image img {\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n.result-info{\r\n  padding: .5em 1em;\r\n  display: inline-block;\r\n  height: 100%;\r\n  max-width: 50%;\r\n\r\n}\r\n\r\n.result-purchase{\r\n  position: absolute;\r\n  right : 10px;\r\n  bottom : 10%;\r\n}\r\n\r\n.result-title{\r\n  font-weight: .5em;\r\n  font-size: 1.1em;\r\n}\r\n\r\n.result-shop{\r\n  font-size: .9em;\r\n}\r\n\r\n.result-price{\r\n  font-size: .8em;\r\n}\r\n\r\n.result-description{\r\n display: none;\r\n}\r\n\r\n@media screen and (min-width:764px){\r\n.search-grid{\r\n   display: grid;\r\n   grid-template-columns: 10% 1fr 10%;\r\n   grid-template-rows: 120px 1fr;\r\n   grid-template-areas: \". . .\"\r\n                        \". search-area .\"\r\n}\r\n\r\n.search-result{\r\n  min-height: 192px;\r\n}\r\n\r\n\r\n\r\n.result-description{\r\n  display: block;\r\n  font-size: .7em;\r\n  color : rgb(131, 128, 128);\r\n}\r\n\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL3NlYXJjaC9zZWFyY2guY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIsYUFBYTtFQUNiLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLG9DQUFvQztFQUNwQyxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osY0FBYzs7QUFFaEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUdBO0VBQ0UsZUFBZTtBQUNqQjs7QUFHQTtDQUNDLGFBQWE7QUFDZDs7QUFDQTtBQUNBO0dBQ0csYUFBYTtHQUNiLGtDQUFrQztHQUNsQyw2QkFBNkI7R0FDN0I7O0FBRUg7O0FBRUE7RUFDRSxpQkFBaUI7QUFDbkI7Ozs7QUFJQTtFQUNFLGNBQWM7RUFDZCxlQUFlO0VBQ2YsMEJBQTBCO0FBQzVCOztBQUVBIiwiZmlsZSI6InNyYy9hcHAvdXNlci9jb21wb25lbnRzL3NlYXJjaC9zZWFyY2guY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yZXN1bHRzLWhlYWRlcntcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTE5ZWJkO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zZWFyY2gtcmVzdWx0e1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBoZWlnaHQ6IDIwdmg7XHJcbiAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgbWluLWhlaWdodDogMTIwcHg7XHJcbiAgbWFyZ2luOiAxMHB4IDA7XHJcbn1cclxuXHJcbi5yZXN1bHQtaW1hZ2V7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAzMHZ3O1xyXG4gIG1heC13aWR0aDogMjAwcHg7XHJcbiAgbWluLXdpZHRoOiAxMzBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTcwLCAxNzAsIDE3MCk7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4ucmVzdWx0LWltYWdlIGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5yZXN1bHQtaW5mb3tcclxuICBwYWRkaW5nOiAuNWVtIDFlbTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC13aWR0aDogNTAlO1xyXG5cclxufVxyXG5cclxuLnJlc3VsdC1wdXJjaGFzZXtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQgOiAxMHB4O1xyXG4gIGJvdHRvbSA6IDEwJTtcclxufVxyXG5cclxuLnJlc3VsdC10aXRsZXtcclxuICBmb250LXdlaWdodDogLjVlbTtcclxuICBmb250LXNpemU6IDEuMWVtO1xyXG59XHJcblxyXG4ucmVzdWx0LXNob3B7XHJcbiAgZm9udC1zaXplOiAuOWVtO1xyXG59XHJcblxyXG5cclxuLnJlc3VsdC1wcmljZXtcclxuICBmb250LXNpemU6IC44ZW07XHJcbn1cclxuXHJcblxyXG4ucmVzdWx0LWRlc2NyaXB0aW9ue1xyXG4gZGlzcGxheTogbm9uZTtcclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOjc2NHB4KXtcclxuLnNlYXJjaC1ncmlke1xyXG4gICBkaXNwbGF5OiBncmlkO1xyXG4gICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDEwJSAxZnIgMTAlO1xyXG4gICBncmlkLXRlbXBsYXRlLXJvd3M6IDEyMHB4IDFmcjtcclxuICAgZ3JpZC10ZW1wbGF0ZS1hcmVhczogXCIuIC4gLlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiLiBzZWFyY2gtYXJlYSAuXCJcclxufVxyXG5cclxuLnNlYXJjaC1yZXN1bHR7XHJcbiAgbWluLWhlaWdodDogMTkycHg7XHJcbn1cclxuXHJcblxyXG5cclxuLnJlc3VsdC1kZXNjcmlwdGlvbntcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBmb250LXNpemU6IC43ZW07XHJcbiAgY29sb3IgOiByZ2IoMTMxLCAxMjgsIDEyOCk7XHJcbn1cclxuXHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/user/components/search/search.component.html":
/*!**************************************************************!*\
  !*** ./src/app/user/components/search/search.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"(search_results() | async) as results; else enter_search\">\r\n  <div class=\"results-header\">search results {{results.length}}</div>\r\n  <div *ngFor=\"let result of results\">\r\n    <div class=\"search-result\">\r\n      <div class=\"result-image\">\r\n        <img (click)=\"view_item(result.sku)\" src=\"{{dbUrl}}/file/{{result.display_image}}\">\r\n      </div>\r\n      <span class=\"result-title\">{{result.title}}</span><br>\r\n      <span class=\"result-shop\">{{result.retailer}}</span><br>\r\n      <span class=\"result-price\">{{result.list_price}}</span><br>\r\n      <div class=\"result-description\">\r\n        make a type specimen book. It has\r\n        survived not only five centuries,\r\n        but also the leap into electronic\r\n        typesetting, remaining essentially\r\n        unchanged. It was popularised in the\r\n        1960s with the release of Letraset\r\n      </div>\r\n      <div class=\"result-purchase\">\r\n        <app-transact [product]=\"result\"></app-transact>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<ng-template #enter_search>\r\n  <div class=\"results-header\">Find your items here</div>\r\n</ng-template>"

/***/ }),

/***/ "./src/app/user/components/search/search.component.ts":
/*!************************************************************!*\
  !*** ./src/app/user/components/search/search.component.ts ***!
  \************************************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_search_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/search-handler.service */ "./src/app/user/services/search-handler.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/user/services/product.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");






let SearchComponent = class SearchComponent {
    constructor(search, router, product_service) {
        this.search = search;
        this.router = router;
        this.product_service = product_service;
        this.dbUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].dbUrl;
    }
    ngOnInit() {
        this.search.set_query();
    }
    search_results() {
        return this.search.get_results();
    }
    view_item(sku) {
        this.product_service.get_product_for_view(sku);
        this.router.navigate(['/user/product']);
    }
};
SearchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'search',
        template: __webpack_require__(/*! ./search.component.html */ "./src/app/user/components/search/search.component.html"),
        styles: [__webpack_require__(/*! ./search.component.css */ "./src/app/user/components/search/search.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_search_handler_service__WEBPACK_IMPORTED_MODULE_2__["SearchHandlerService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"]])
], SearchComponent);



/***/ }),

/***/ "./src/app/user/components/slider/slider.component.css":
/*!*************************************************************!*\
  !*** ./src/app/user/components/slider/slider.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n    .products-slide{\r\n      position: relative;\r\n      overflow: hidden;\r\n    }\r\n    #slide-inner{\r\n      position: relative;\r\n      max-width: 100vw;\r\n      overflow: hidden;\r\n      white-space: nowrap;\r\n      padding: 5px 0;\r\n    }\r\n    dot:hover{\r\n      background-color: #717171;\r\n    }\r\n    /* slide controls */\r\n    .slide-control{\r\n      cursor: pointer;\r\n      position: absolute;\r\n      top: 40%;\r\n      margin-top: -20px;\r\n      background-color: #bbb;\r\n      transition: background-color 0.5s ease;\r\n      padding-top: 10px;\r\n      opacity: 5;\r\n    }\r\n    .slide-control:hover{\r\n      background-color: #717171;\r\n    }\r\n    .slide-control-prev{\r\n      border-radius: 0px 6px 6px 0px;\r\n      padding: 10px;\r\n    }\r\n    .slide-control-next{\r\n      right: 0px;\r\n      border-radius: 6px 0px 0px 6px;\r\n      padding: 10px;\r\n    }\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL3NsaWRlci9zbGlkZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0lBQ0k7TUFDRSxrQkFBa0I7TUFDbEIsZ0JBQWdCO0lBQ2xCO0lBQ0E7TUFDRSxrQkFBa0I7TUFDbEIsZ0JBQWdCO01BQ2hCLGdCQUFnQjtNQUNoQixtQkFBbUI7TUFDbkIsY0FBYztJQUNoQjtJQUVBO01BQ0UseUJBQXlCO0lBQzNCO0lBR0osbUJBQW1CO0lBQ2Y7TUFDRSxlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLFFBQVE7TUFDUixpQkFBaUI7TUFDakIsc0JBQXNCO01BQ3RCLHNDQUFzQztNQUN0QyxpQkFBaUI7TUFDakIsVUFBVTtJQUNaO0lBQ0E7TUFDRSx5QkFBeUI7SUFDM0I7SUFDQTtNQUNFLDhCQUE4QjtNQUM5QixhQUFhO0lBQ2Y7SUFDQTtNQUNFLFVBQVU7TUFDViw4QkFBOEI7TUFDOUIsYUFBYTtJQUNmIiwiZmlsZSI6InNyYy9hcHAvdXNlci9jb21wb25lbnRzL3NsaWRlci9zbGlkZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4gICAgLnByb2R1Y3RzLXNsaWRle1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICB9XHJcbiAgICAjc2xpZGUtaW5uZXJ7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgbWF4LXdpZHRoOiAxMDB2dztcclxuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgcGFkZGluZzogNXB4IDA7XHJcbiAgICB9XHJcblxyXG4gICAgZG90OmhvdmVye1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzE3MTcxO1xyXG4gICAgfVxyXG5cclxuXHJcbi8qIHNsaWRlIGNvbnRyb2xzICovXHJcbiAgICAuc2xpZGUtY29udHJvbHtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHRvcDogNDAlO1xyXG4gICAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2JiYjtcclxuICAgICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAwLjVzIGVhc2U7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgICBvcGFjaXR5OiA1O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlLWNvbnRyb2w6aG92ZXJ7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICM3MTcxNzE7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUtY29udHJvbC1wcmV2e1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAwcHggNnB4IDZweCAwcHg7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUtY29udHJvbC1uZXh0e1xyXG4gICAgICByaWdodDogMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA2cHggMHB4IDBweCA2cHg7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/user/components/slider/slider.component.html":
/*!**************************************************************!*\
  !*** ./src/app/user/components/slider/slider.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"position: relative;\" (window:load)=\"adjust_variables()\" (window:resize)=\"adjust_variables()\">\n       <div class=\"products-slide\" id=\"{{title}}\" style=\"width: 100%\">\n         <div id=\"slide-inner\">\n          <ng-content></ng-content>\n         </div>\n         <div class=\"slide-indicators\">\n            <span class=\"slide-control-prev slide-control\" data-slide=\"prev\" (click)=\"slide_left()\" *ngIf=\"initial_position<0\">\n                <span>&#10094;</span>\n            </span>\n            <span  class=\"slide-control-next slide-control\" data-slide=\"next\" (click)=\"slide_right()\" *ngIf=\"show_right_slider\">\n                <span>&#10095;</span>\n            </span>\n          </div>\n       </div>\n </div>\n\n"

/***/ }),

/***/ "./src/app/user/components/slider/slider.component.ts":
/*!************************************************************!*\
  !*** ./src/app/user/components/slider/slider.component.ts ***!
  \************************************************************/
/*! exports provided: SliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderComponent", function() { return SliderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/user/services/product.service.ts");



let SliderComponent = class SliderComponent {
    constructor(product_service) {
        this.product_service = product_service;
        this.initial_position = 0;
        this.current_position = `${this.initial_position}px`;
        this.items_slider_difference = 0;
        this.show_right_slider = false;
    }
    ngOnInit() {
        if (document.querySelector('#item_img')) {
            this.item_width = document.querySelector('#item_img').clientWidth;
        }
        setTimeout(() => {
            this.adjust_variables();
        }, 500);
        if ('ontouchstart' in document) {
            var start_pos;
            var docs = document.querySelectorAll('#slide-inner');
            docs.forEach(element => {
                element.addEventListener("touchstart", function (e) {
                    var touchObj = e.changedTouches[0];
                    start_pos = touchObj.clientX;
                }, false);
            });
            console.log('touch');
            window.addEventListener('touchmove', (e) => {
                var touchObj = e.changedTouches[0];
                var move_dist = touchObj.clientX - start_pos;
                if (move_dist > 0) {
                    this.slide_left();
                }
                else if (move_dist < 0) {
                    this.slide_right();
                }
            }, false);
            window.addEventListener('touchend', function (e) { });
        }
    }
    slide_left() {
        if (this.initial_position < 0) {
            this.initial_position = this.initial_position + this.item_width;
            this.current_position = `${this.initial_position}px`;
            this.items_slider_difference -= this.item_width;
            this.show_right_slider = true;
        }
        if (this.initial_position > 0) { //resets position to zero
            this.initial_position = 0;
            this.current_position = `${0}px`;
            this.items_slider_difference = this.slider_width - this.total_width_of_items;
            this.show_right_slider = true;
        }
    }
    slide_right() {
        if ((-this.items_slider_difference) > 0) {
            if (-this.items_slider_difference < this.item_width) { //if gap is small only move by gap size
                this.initial_position = this.initial_position + this.items_slider_difference; //plus because diff is negative
                this.current_position = `${this.initial_position}px`;
                this.items_slider_difference += -this.items_slider_difference;
            }
            else {
                this.initial_position = this.initial_position - this.item_width;
                this.current_position = `${this.initial_position}px`;
                this.items_slider_difference += this.item_width;
            }
            if (this.items_slider_difference >= 0) {
                this.show_right_slider = false;
            }
        }
    }
    set_variables(event) {
        var w = event * this.items_count;
        this.item_width = event;
        this.total_width_of_items = w;
    }
    //updates product position variables
    adjust_variables() {
        var new_slide_width = document.querySelector('.products-slide').clientWidth;
        this.item_width = document.querySelector('#item_img').clientWidth;
        this.set_variables(this.item_width);
        this.slider_width = new_slide_width;
        this.items_slider_difference = this.slider_width - this.total_width_of_items;
        if (this.items_slider_difference < 0) { //checks if right slide icon should show
            this.show_right_slider = true;
        }
        if (this.items_slider_difference >= 0) { //adjusts position of items when resizing
            this.initial_position = 0;
            this.current_position = this.initial_position + 'px';
        }
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], SliderComponent.prototype, "title", void 0);
SliderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-slider',
        template: __webpack_require__(/*! ./slider.component.html */ "./src/app/user/components/slider/slider.component.html"),
        styles: [__webpack_require__(/*! ./slider.component.css */ "./src/app/user/components/slider/slider.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"]])
], SliderComponent);



/***/ }),

/***/ "./src/app/user/components/transact/transact.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/user/components/transact/transact.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button{\r\n    border-radius: 20px;\r\n    width: 50px;\r\n    font-size: 12px;\r\n    border: none;\r\n    margin-left: 5px;\r\n}\r\n\r\nbutton:hover{\r\n    background-color: skyblue;\r\n    color: white;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL3RyYW5zYWN0L3RyYW5zYWN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLGVBQWU7SUFDZixZQUFZO0lBQ1osZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVk7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC91c2VyL2NvbXBvbmVudHMvdHJhbnNhY3QvdHJhbnNhY3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImJ1dHRvbntcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbn1cclxuXHJcbmJ1dHRvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNreWJsdWU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/user/components/transact/transact.component.html":
/*!******************************************************************!*\
  !*** ./src/app/user/components/transact/transact.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button title=\"negotiate price\" (click)=\"get_negotiation_form(product.sku, product.list_price, product.retailer)\">Bid</button>\n<button title=\"order item\" (click)=\"get_form(product.sku, product.title, product.list_price)\">Order</button>"

/***/ }),

/***/ "./src/app/user/components/transact/transact.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/user/components/transact/transact.component.ts ***!
  \****************************************************************/
/*! exports provided: TransactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactComponent", function() { return TransactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_sidebar_menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/sidebar-menu.service */ "./src/app/user/services/sidebar-menu.service.ts");
/* harmony import */ var _services_transaction_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/transaction.service */ "./src/app/user/services/transaction.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let TransactComponent = class TransactComponent {
    constructor(transact, sidebar_service, router) {
        this.transact = transact;
        this.sidebar_service = sidebar_service;
        this.router = router;
    }
    ngOnInit() {
    }
    get_form(sku, title, price) {
        this.transact.add_item(sku, title, price);
        this.router.navigate(['/user/order']);
    }
    get_negotiation_form(sku, price, shop) {
        this.transact.set_negotiation_item(sku, price, shop);
        this.sidebar_service.open_sidebar(0);
        this.router.navigate(['/user/bid']);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], TransactComponent.prototype, "product", void 0);
TransactComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-transact',
        template: __webpack_require__(/*! ./transact.component.html */ "./src/app/user/components/transact/transact.component.html"),
        styles: [__webpack_require__(/*! ./transact.component.css */ "./src/app/user/components/transact/transact.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_transaction_service__WEBPACK_IMPORTED_MODULE_3__["TransactionService"],
        _services_sidebar_menu_service__WEBPACK_IMPORTED_MODULE_2__["SidebarMenuService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], TransactComponent);



/***/ }),

/***/ "./src/app/user/components/view/view.component.css":
/*!*********************************************************!*\
  !*** ./src/app/user/components/view/view.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#view-title{\r\n  position: relative;\r\n}\r\n\r\n\r\n.view-item-wrapper{\r\n  width: 50%;\r\n  margin-bottom: 5px;\r\n  text-align: center;\r\n  position: relative;\r\n  float: left;\r\n}\r\n\r\n\r\n.view-item-inner{\r\n  height: inherit;\r\n}\r\n\r\n\r\n#view-title-wrapper{\r\n  height: 100px;\r\n  background-color : rosybrown;\r\n  margin-bottom: 20px;\r\n  padding: 20px 10px;\r\n}\r\n\r\n\r\n#view-wrapper{\r\n  text-align: center;\r\n}\r\n\r\n\r\n@media screen and (min-width : 664px){\r\n  .view-item-wrapper{\r\n    width: calc(100% / 3);\r\n  }\r\n\r\n}\r\n\r\n\r\n@media screen and (min-width : 964px){\r\n  .view-item-wrapper{\r\n    width: calc(100% / 4);\r\n  }\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jb21wb25lbnRzL3ZpZXcvdmlldy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0FBQ3BCOzs7QUFHQTtFQUNFLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2I7OztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLG1CQUFtQjtFQUNuQixrQkFBa0I7QUFDcEI7OztBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCOzs7QUFFQTtFQUNFO0lBQ0UscUJBQXFCO0VBQ3ZCOztBQUVGOzs7QUFFQTtFQUNFO0lBQ0UscUJBQXFCO0VBQ3ZCO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC91c2VyL2NvbXBvbmVudHMvdmlldy92aWV3LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjdmlldy10aXRsZXtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcblxyXG4udmlldy1pdGVtLXdyYXBwZXJ7XHJcbiAgd2lkdGg6IDUwJTtcclxuICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuLnZpZXctaXRlbS1pbm5lcntcclxuICBoZWlnaHQ6IGluaGVyaXQ7XHJcbn1cclxuXHJcbiN2aWV3LXRpdGxlLXdyYXBwZXJ7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yIDogcm9zeWJyb3duO1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgcGFkZGluZzogMjBweCAxMHB4O1xyXG59XHJcblxyXG4jdmlldy13cmFwcGVye1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDY2NHB4KXtcclxuICAudmlldy1pdGVtLXdyYXBwZXJ7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC8gMyk7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDk2NHB4KXtcclxuICAudmlldy1pdGVtLXdyYXBwZXJ7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC8gNCk7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/user/components/view/view.component.html":
/*!**********************************************************!*\
  !*** ./src/app/user/components/view/view.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"(data$ | async) as data\">\n    <div *ngIf=\"data\">\n        <div id=\"view-title-wrapper\">\n            <div id=\"view-title\">{{data.heading}}</div>\n        </div>\n        <div id=\"view-wrapper\">\n            <div class=\"view-item-wrapper\" *ngFor=\"let item of data.data\">\n                <div class=\"view-item-inner\">\n                    <product [product]=\"item\" [alter_width]=\"'inherit'\"></product>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/user/components/view/view.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user/components/view/view.component.ts ***!
  \********************************************************/
/*! exports provided: ViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewComponent", function() { return ViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/user/services/product.service.ts");



let ViewComponent = class ViewComponent {
    constructor(product_service) {
        this.product_service = product_service;
    }
    ngOnInit() {
        this.product_service.view_variables(null, null);
        this.data$ = this.product_service.view;
    }
};
ViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-view',
        template: __webpack_require__(/*! ./view.component.html */ "./src/app/user/components/view/view.component.html"),
        styles: [__webpack_require__(/*! ./view.component.css */ "./src/app/user/components/view/view.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"]])
], ViewComponent);



/***/ }),

/***/ "./src/app/user/services/product.service.ts":
/*!**************************************************!*\
  !*** ./src/app/user/services/product.service.ts ***!
  \**************************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");





let ProductService = class ProductService {
    constructor(http) {
        this.http = http;
        this.uri = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].dbUrl;
        this.list = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.view = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.product_for_view = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
    }
    /**
     *
     * @param str sku of wanted product
     */
    get_product_for_view(str) {
        if (str) {
            localStorage.setItem('LViewed', str);
        }
        var sku = str || localStorage.getItem('LViewed');
        return this.http.get(`${this.uri}/product/findBySku/${sku}`).subscribe((docs) => {
            this.product_for_view.next(docs[0]);
        });
    }
    getProduct() {
        return this.http.get(`${this.uri}/product/all`);
    }
    getProductBySku(sku) {
        return this.http.get(`${this.uri}/product/sku/${sku}`);
    }
    //for departments
    set_page_variables(name) {
        if (name) {
            localStorage.setItem('listItems', name);
        }
        var name = localStorage.getItem('listItems');
        this.http.get(`${this.uri}/${name}`).subscribe((data) => {
            return;
        });
        if (name == "shop") {
            this.http.get(`${this.uri}/shop/findShop`).subscribe((data) => {
                this.list.next({ heading: name, data: data });
                return;
            });
        }
        else if (name == "department") {
            this.http.get(`${this.uri}/common/findDepartment`).subscribe((data) => {
                this.list.next({ heading: name, data: data });
                return;
            });
        }
    }
    view_variables(name, field) {
        if (name) {
            localStorage.setItem('viewItems', name);
            localStorage.setItem('viewField', field);
        }
        var name = localStorage.getItem('viewItems');
        var field = localStorage.getItem('viewField');
        if (field == "department") {
            this.http.get(`${this.uri}/product/findByDepartment/${name}`).subscribe((data) => {
                this.view.next({ heading: name, data: data });
                return;
            });
        }
        else if (field == "shop") {
            this.http.get(`${this.uri}/product/findByShop/${name}`).subscribe((data) => {
                this.view.next({ heading: name, data: data });
                return;
            });
        }
    }
    getProductByDepartment(department) {
        return this.http.get(`${this.uri}/product/findByDepartment/${department}`);
    }
    getProductByTopic(topic) {
        return this.http.get(`${this.uri}/home/find${topic}`);
    }
};
ProductService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], ProductService);



/***/ }),

/***/ "./src/app/user/services/search-handler.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/user/services/search-handler.service.ts ***!
  \*********************************************************/
/*! exports provided: SearchHandlerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchHandlerService", function() { return SearchHandlerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




let SearchHandlerService = class SearchHandlerService {
    constructor(http) {
        this.http = http;
        this.uri = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].dbUrl;
    }
    get_results() {
        return this.results;
    }
    set_query(search_term) {
        var query = search_term || localStorage.getItem('last_search_term');
        this.results = this.http.get(`${this.uri}/common/search/${query}`);
    }
};
SearchHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], SearchHandlerService);



/***/ }),

/***/ "./src/app/user/services/sidebar-menu.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/user/services/sidebar-menu.service.ts ***!
  \*******************************************************/
/*! exports provided: SidebarMenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarMenuService", function() { return SidebarMenuService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SidebarMenuService = class SidebarMenuService {
    constructor() {
        this.sidebar = false;
        this.active_window = 0;
        this.windows = [
            { name: 'shopping_cart', state: true },
            { name: 'user_menu', state: false }
        ];
    }
    toggle_sidebar(window) {
        //if sidebar is already open and active & desired windows same hide
        if (this.sidebar && window == this.active_window) {
            this.sidebar = false;
        }
        else {
            this.open_sidebar(window);
        }
    }
    open_sidebar(window) {
        this.active_window = window;
        this.toggle_windows(window);
        this.sidebar = true; //show sidebar
    }
    toggle_windows(index) {
        this.windows.forEach(pane => {
            pane.state = false;
        });
        this.windows[index].state = true;
    }
    get sidebar_state() {
        return this.sidebar;
    }
    window_state(index) {
        return this.windows[index].state;
    }
};
SidebarMenuService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SidebarMenuService);



/***/ }),

/***/ "./src/app/user/services/transaction.service.ts":
/*!******************************************************!*\
  !*** ./src/app/user/services/transaction.service.ts ***!
  \******************************************************/
/*! exports provided: TransactionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionService", function() { return TransactionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm2015/ng.apollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_7__);








let TransactionService = class TransactionService {
    constructor(http, fb, apollo) {
        this.http = http;
        this.fb = fb;
        this.apollo = apollo;
        this.uri = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].dbUrl;
        this.negotiation_item = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.quickOrderMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_7___default.a `
      mutation QuickOrder($sku:String, $quantity: Int, $shop:String){
        quickOrder(sku:$sku, quantity:$quantity, shop:$shop)
      }
    `;
        this.cartOrderMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_7___default.a `
     mutation CartOrder($sku:String!, $quantity: Int!, $shop:String!){
       cartOrder(sku:$items, quantity:$quantity, shop:$shop)
     }
   `;
        this.quickBidMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_7___default.a `
      mutation QuickBid($sku: String, $quantity: Int, $shop:String, $offer: Float){
        quickBid(sku: $sku, quantity: $quantity, shop: $shop, offer: $offer)
      }
   `;
        this.addToCartMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_7___default.a `
      mutation AddToCart($sku: String!, $quantity: Int!){
        addToCart(sku: $sku, quantity: $quantity)
      }
    `;
        const address = this.fb.group({
            'city': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            'street': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            'erf': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
        this.order_form = this.fb.group({
            'items': this.fb.array([]),
            'billing_address': address,
            'order_number': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([])],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(8)])]
        });
    }
    //   cancelOrderMutaion = gql`
    //     mutation orderProduct($orderNumber:String){
    //     }
    //   `
    //   addToCartMutation = gql`
    //   mutation orderProduct($sku:String!){
    //   }
    // `
    //   removeFromCartMutaion = gql`
    // mutation orderProduct($sku:String){
    // }
    // `
    //   removeFromOrderMutation = gql`
    // mutation orderProduct($sku:String, $orderNumber:String){
    // }
    // `
    //   deleteCartMutation = gql`
    // mutation orderProduct(){
    // }
    // `
    //   addToOrderMutation = gql`
    // mutation orderProduct($sku:String){
    // }
    // `
    // deleteFromOrderMutation = gql`
    // mutation orderProduct($sku:String){
    // }
    // `
    quickOrderHandler(sku, shop, quantity) {
        return this.apollo.mutate({
            mutation: this.quickOrderMutation,
            variables: {
                sku,
                quantity,
                shop
            }
        });
    }
    cartOrderHandler(cart) {
        return this.apollo.mutate({
            mutation: this.cartOrderMutation,
            variables: {
                cart
            }
        });
    }
    quickBidHandler(sku, quantity, shop, offer) {
        return this.apollo.mutate({
            mutation: this.quickBidMutation,
            variables: {
                shop,
                sku,
                quantity,
                offer
            }
        });
    }
    addToCartHandler(sku, quantity) {
        return this.apollo.mutate({
            mutation: this.addToCartMutation,
            variables: {
                sku,
                quantity
            }
        });
    }
    set_negotiation_item(sku, price, shop) {
        this.negotiation_item.next({ sku: sku, price: price, shop: shop });
    }
    /** manages order process */
    order_processor(form) {
        var order = form.value;
        return this.http.post(`${this.uri}/order`, order, { withCredentials: true });
    }
    get order_number() {
        return this.order_form.get('order_number');
    }
    get city() {
        return this.order_form.get('billing_address').get('city');
    }
    get street() {
        return this.order_form.get('billing_address').get('street');
    }
    get erf() {
        return this.order_form.get('billing_address').get('erf');
    }
    change_order_number(order_no) {
        this.order_number.setValue(order_no);
    }
    change_order_address(address) {
        this.city.setValue(address.city);
        this.street.setValue(address.street);
        this.erf.setValue(address.erf);
    }
    get item_forms() {
        return this.order_form.get('items');
    }
    add_item(sku, title, price) {
        let add = true;
        //filters out duplicates
        this.order_form.value.items.forEach(item => {
            if (title == item.title) {
                add = false;
                return;
            }
        });
        //adding items to order form
        if (add) {
            const items = this.fb.group({
                'title': [title, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                'quantity': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].min(1)])],
                'price': [price],
                'sku': [sku]
            });
            this.item_forms.push(items);
        }
    }
    clear_form() {
        for (var i = 0; i < this.item_forms.length; i++) {
            this.item_forms.removeAt(i);
        }
        this.order_form.reset();
    }
};
TransactionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], apollo_angular__WEBPACK_IMPORTED_MODULE_6__["Apollo"]])
], TransactionService);



/***/ }),

/***/ "./src/app/user/user-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/user/user-routing.module.ts ***!
  \*********************************************/
/*! exports provided: UserRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRoutingModule", function() { return UserRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/user/components/home/home.component.ts");
/* harmony import */ var _components_list_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/list/list.component */ "./src/app/user/components/list/list.component.ts");
/* harmony import */ var _components_view_view_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/view/view.component */ "./src/app/user/components/view/view.component.ts");
/* harmony import */ var _components_product_view_product_view_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/product-view/product-view.component */ "./src/app/user/components/product-view/product-view.component.ts");
/* harmony import */ var _components_search_search_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/search/search.component */ "./src/app/user/components/search/search.component.ts");
/* harmony import */ var _user_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _components_order_form_order_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/order-form/order-form.component */ "./src/app/user/components/order-form/order-form.component.ts");
/* harmony import */ var _components_negotiate_negotiate_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/negotiate/negotiate.component */ "./src/app/user/components/negotiate/negotiate.component.ts");











const routes = [
    {
        path: '', component: _user_component__WEBPACK_IMPORTED_MODULE_8__["UserComponent"], children: [
            { path: 'home', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
            { path: 'list', component: _components_list_list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"] },
            { path: 'view', component: _components_view_view_component__WEBPACK_IMPORTED_MODULE_5__["ViewComponent"] },
            { path: 'product', component: _components_product_view_product_view_component__WEBPACK_IMPORTED_MODULE_6__["ProductViewComponent"] },
            { path: 'search', component: _components_search_search_component__WEBPACK_IMPORTED_MODULE_7__["SearchComponent"] },
            { path: 'order', component: _components_order_form_order_form_component__WEBPACK_IMPORTED_MODULE_9__["OrderFormComponent"] },
            { path: 'bid', component: _components_negotiate_negotiate_component__WEBPACK_IMPORTED_MODULE_10__["NegotiateComponent"] },
            { path: 'account', loadChildren: 'src/app/user/account/account.module#AccountModule' },
            { path: '', redirectTo: 'home', pathMatch: 'full' }
        ]
    }
];
let UserRoutingModule = class UserRoutingModule {
};
UserRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], UserRoutingModule);



/***/ }),

/***/ "./src/app/user/user.component.css":
/*!*****************************************!*\
  !*** ./src/app/user/user.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvdXNlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>\n<app-sidebar [color]=\"'#119ebd'\"></app-sidebar>\n<div class=\"main-grid\">\n    <div class=\"main-content-area\">\n        <router-outlet></router-outlet>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UserComponent = class UserComponent {
    constructor() { }
    ngOnInit() {
    }
};
UserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user',
        template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
        styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/user/user.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], UserComponent);



/***/ }),

/***/ "./src/app/user/user.module.ts":
/*!*************************************!*\
  !*** ./src/app/user/user.module.ts ***!
  \*************************************/
/*! exports provided: UserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _user_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-routing.module */ "./src/app/user/user-routing.module.ts");
/* harmony import */ var _user_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/user/components/home/home.component.ts");
/* harmony import */ var _components_order_form_order_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/order-form/order-form.component */ "./src/app/user/components/order-form/order-form.component.ts");
/* harmony import */ var _components_negotiate_negotiate_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/negotiate/negotiate.component */ "./src/app/user/components/negotiate/negotiate.component.ts");
/* harmony import */ var _components_list_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/list/list.component */ "./src/app/user/components/list/list.component.ts");
/* harmony import */ var _components_slider_slider_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/slider/slider.component */ "./src/app/user/components/slider/slider.component.ts");
/* harmony import */ var _components_product_view_product_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/product-view/product-view.component */ "./src/app/user/components/product-view/product-view.component.ts");
/* harmony import */ var _components_product_product_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/product/product.component */ "./src/app/user/components/product/product.component.ts");
/* harmony import */ var _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/navigation/navigation.component */ "./src/app/user/components/navigation/navigation.component.ts");
/* harmony import */ var _components_search_search_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/search/search.component */ "./src/app/user/components/search/search.component.ts");
/* harmony import */ var _components_view_view_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/view/view.component */ "./src/app/user/components/view/view.component.ts");
/* harmony import */ var _components_transact_transact_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/transact/transact.component */ "./src/app/user/components/transact/transact.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");




















let UserModule = class UserModule {
};
UserModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _user_component__WEBPACK_IMPORTED_MODULE_4__["UserComponent"],
            _components_home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
            _user_component__WEBPACK_IMPORTED_MODULE_4__["UserComponent"],
            _components_list_list_component__WEBPACK_IMPORTED_MODULE_8__["ListComponent"],
            _components_slider_slider_component__WEBPACK_IMPORTED_MODULE_9__["SliderComponent"],
            _components_product_view_product_view_component__WEBPACK_IMPORTED_MODULE_10__["ProductViewComponent"],
            _components_product_product_component__WEBPACK_IMPORTED_MODULE_11__["ProductComponent"],
            _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_12__["NavigationComponent"],
            _components_search_search_component__WEBPACK_IMPORTED_MODULE_13__["SearchComponent"],
            _components_view_view_component__WEBPACK_IMPORTED_MODULE_14__["ViewComponent"],
            _components_transact_transact_component__WEBPACK_IMPORTED_MODULE_15__["TransactComponent"],
            _components_order_form_order_form_component__WEBPACK_IMPORTED_MODULE_6__["OrderFormComponent"],
            _components_negotiate_negotiate_component__WEBPACK_IMPORTED_MODULE_7__["NegotiateComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _user_routing_module__WEBPACK_IMPORTED_MODULE_3__["UserRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_17__["RouterModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_18__["HttpClientModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_19__["SharedModule"]
        ]
    })
], UserModule);



/***/ })

}]);
//# sourceMappingURL=src-app-user-user-module.js.map