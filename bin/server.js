"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.connection = void 0;

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _cors = _interopRequireDefault(require("cors"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _ejs = _interopRequireDefault(require("ejs"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _api = _interopRequireDefault(require("./routes/v0/api"));

var _expressGraphql = _interopRequireDefault(require("express-graphql"));

var _schema = _interopRequireDefault(require("./routes/v1/schema"));

var _user = require("./routes/v1/user/user");

var _notification = _interopRequireDefault(require("./routes/v1/notification"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let app = (0, _express.default)();
/*
	cross origin rules -- disallows session setting
    of users from different IP's
*/

app.use((0, _cors.default)({
  origin: 'http://localhost:4200',
  credentials: true
}));
app.use((0, _cookieParser.default)());
const db = 'mongodb://LAPTOP-5SV7PL85:27017,LAPTOP-5SV7PL85:27018,LAPTOP-5SV7PL85:27019/logic?replicaSet=rs';
/*connection to database*/

const db_uri = process.env.MONGOLAB_SILVER_URI || db;

_mongoose.default.connect(db_uri, {
  useNewUrlParser: true
});

const connection = _mongoose.default.connection; //db connection configuration

exports.connection = connection;
connection.once('open', () => {
  console.log('mongoDb connection established');
});
connection.on('error', err => {
  console.log('server error occured : ' + err);
});
connection.on('SIGINT', () => {
  connection.close(() => {
    console.log('Disconnected through termination');
    process.exit(0);
  });
}); //views

app.set('views', _path.default.join(__dirname, '../www'));
app.set('view engine', 'ejs');
app.engine('html', _ejs.default.renderFile); //static folder

app.use(_express.default.static(_path.default.join(__dirname, '../www'))); //parsers

app.use(_bodyParser.default.json());
app.use(_bodyParser.default.urlencoded({
  extended: false
})); //route to api's

app.use('/api/v0', _api.default); //new api that works with graphql

app.use('/api/v1', (0, _expressGraphql.default)((req, res) => ({
  schema: _schema.default,
  graphiql: true,
  context: {
    req,
    res,
    tokenBody: (0, _user.extractTokenBody)(req)
  }
})));
app.use('/notify', _notification.default); // serves all html related requests

app.get('*', (req, res) => {
  res.render(_path.default.join(__dirname, '../www/index.html'));
});
var port = process.env.PORT || 3572;
app.listen(port, () => {
  console.log("listening on port ", port);
});