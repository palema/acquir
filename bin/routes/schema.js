"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _product = _interopRequireDefault(require("../models/product.model"));

var _department = _interopRequireDefault(require("../models/department.model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ProductType = new _graphql.GraphQLObjectType({
  name: 'Product',
  fields: () => ({
    title: {
      type: _graphql.GraphQLString
    },
    list_price: {
      type: _graphql.GraphQLString
    },
    sku: {
      type: _graphql.GraphQLString
    },
    quantity: {
      type: _graphql.GraphQLInt
    },
    display_image: {
      type: _graphql.GraphQLString
    },
    shop: {
      type: _graphql.GraphQLString
    },
    rating: {
      type: _graphql.GraphQLInt
    },
    purchases: {
      type: _graphql.GraphQLInt
    },
    views: {
      type: _graphql.GraphQLInt
    },
    cart_listings: {
      type: _graphql.GraphQLInt // department : { 
      //     type : ProductType,
      //     resolve(parent, args){
      //         //return _.find(authors, { id : parent.author_id})
      //         //return Department.find({})
      //     }
      // }

    }
  })
});
const DepartmentType = new _graphql.GraphQLObjectType({
  name: 'Department',
  fields: () => ({
    name: {
      type: _graphql.GraphQLString
    }
  })
});
const RootQuery = new _graphql.GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    product: {
      type: ProductType,
      args: {
        title: {
          type: _graphql.GraphQLID
        }
      },

      resolve(parent, args) {
        return _product.default.find({
          title: args.title
        });
      }

    },
    department: {
      type: DepartmentType,
      args: {
        name: {
          type: _graphql.GraphQLID
        }
      },

      resolve(parent, args) {
        return _department.default.find({
          name: args.name
        });
      }

    },
    products: {
      type: (0, _graphql.GraphQLList)(ProductType),

      resolve(parent, args) {
        return _product.default.find({});
      }

    }
  }
});
const Mutation = new _graphql.GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addProduct: {
      type: ProductType,
      args: {
        title: {
          type: _graphql.GraphQLString
        }
      },

      resolve(parent, args) {
        let product = new _product.default({
          title: args.title
        });
        return product.save();
      }

    }
  }
});
const schema = new _graphql.GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
var _default = schema;
exports.default = _default;