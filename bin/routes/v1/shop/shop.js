"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ShopMutation = exports.ShopQuery = exports.ShopType = void 0;

var _graphql = require("graphql");

var _product = require("../product/product");

var _product2 = _interopRequireDefault(require("../../../models/product.model"));

var _shop = _interopRequireDefault(require("../../../models/shop.model"));

var _user = require("../user/user");

var _user2 = _interopRequireDefault(require("../../../models/user.model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ShopType = new _graphql.GraphQLObjectType({
  name: 'Shop',
  fields: () => ({
    name: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
    },
    manager: {
      type: (0, _graphql.GraphQLNonNull)(_user.UserType)
    },
    products: {
      type: (0, _graphql.GraphQLList)((0, _graphql.GraphQLNonNull)(_product.ProductType)),

      resolve(parent) {
        return _product2.default.find({
          shop: parent.name
        });
      }

    }
  })
});
exports.ShopType = ShopType;
const ShopQuery = {
  shop: {
    type: ShopType,
    args: {
      name: {
        type: _graphql.GraphQLString
      }
    },

    resolve(parent, args, {
      tokenBody
    }) {
      if (args.name) return _shop.default.findOne({
        name: args.name
      });
      if (tokenBody.shop) return _shop.default.findOne({
        name: tokenBody.shop
      });
      throw Error('shop not specified');
    }

  },
  shops: {
    type: (0, _graphql.GraphQLList)(ShopType),
    args: {
      name: {
        type: (0, _graphql.GraphQLList)(_graphql.GraphQLString)
      }
    },

    resolve(parent, args) {
      if (args.length > 0) {
        return _shop.default.find({
          name: {
            $in: args.name
          }
        });
      } else {
        return _shop.default.find({});
      }
    }

  } // shopProducts: {
  //     type: GraphQLList(ProductType),
  //     resolve(_, { tokenBody }) {
  //         return Product.find({ shop: tokenBody.shop })
  //     }
  // }

};
exports.ShopQuery = ShopQuery;
const ShopMutation = {
  createShop: {
    type: _graphql.GraphQLString,
    args: {
      name: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      }
    },

    async resolve(parent, args, {
      tokenBody,
      res
    }) {
      if (!tokenBody.username) {
        throw Error('requires signed up user');
      }

      if (tokenBody.shop) {
        throw Error('account already attached to a shop');
      }

      var shop = new _shop.default({
        name: args.name,
        manager: tokenBody.username
      });
      await _user2.default.findOne({
        username: tokenBody.username
      }, {
        _id: 0,
        shop: 1
      }).then(reply => {
        if (reply.shop) {
          /** remove db and cookie inconsistency by updating users shop field in cookie */
          (0, _user.setToken)({
            username: tokenBody.username,
            shop: reply.shop.toString(),
            res
          });
          throw Error('account already attached to a shop');
        }
      });
      return ''; //return shop.save()
      // .then(reply=>{
      //     return User.findOneAndUpdate({username: tokenBody.username }, {$set : { shop: args.name}},{new:true})
      // })
      // .then(usereply=>{
      //     setToken({username:usereply.username.toString(), shop: usereply.shop.toString(), res})
      //     return  publicToken(usereply.username, 'seller')
      // })
      // .catch(err=>{
      //     throw err
      // })
    }

  }
};
exports.ShopMutation = ShopMutation;