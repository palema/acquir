"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _lodash = _interopRequireDefault(require("lodash"));

var _product = require("./product/product");

var _user = require("./user/user");

var _shop = require("./shop/shop");

var _department = require("./department/department");

var _home = require("./home");

var _transaction = require("./transaction");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* Local imports */

/* Defines all queries that can be perfomed on this schema */
const RootQuery = new _graphql.GraphQLObjectType({
  name: 'RootQueryType',
  fields: _lodash.default.merge(_department.DepartmentQuery, _user.UserQuery, _product.ProductQuery, _home.HomeQuery, _shop.ShopQuery, _transaction.OrderQuery)
});
/* Defines all mutations on objects */

const Mutation = new _graphql.GraphQLObjectType({
  name: 'Mutation',
  fields: _lodash.default.merge(_product.ProductMutation, _shop.ShopMutation, _user.UserMutation, _transaction.OrderMutation)
});
const schema = new _graphql.GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
var _default = schema;
exports.default = _default;