"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.extractTokenBody = extractTokenBody;
exports.setToken = setToken;
exports.publicToken = publicToken;
exports.UserMutation = exports.UserQuery = exports.UserType = void 0;

var _graphql = require("graphql");

var _shop = require("../shop/shop");

var _shop2 = _interopRequireDefault(require("../../../models/shop.model"));

var _user = _interopRequireDefault(require("../../../models/user.model"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const UserType = new _graphql.GraphQLObjectType({
  name: 'User',
  fields: () => ({
    username: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
    },
    email: {
      type: _graphql.GraphQLString
    },
    password: {
      type: _graphql.GraphQLString
    },
    balance: {
      type: _graphql.GraphQLFloat
    },
    shop: {
      type: _shop.ShopType,

      resolve(parent) {
        return _shop2.default.findOne({
          name: parent.shop
        });
      }

    }
  })
});
exports.UserType = UserType;
const UserQuery = {
  login: {
    type: _graphql.GraphQLString,
    args: {
      username: {
        type: _graphql.GraphQLString
      },
      password: {
        type: _graphql.GraphQLString
      }
    },
    resolve: async (parent, args, {
      res
    }) => {
      var data = await _user.default.findOne({
        $or: [{
          username: args.username
        }, {
          email: args.username
        }]
      }, {
        username: 1,
        _id: 0,
        shop: 1,
        password: 1
      });

      if (!data) {
        throw Error('invalid credentials');
      }

      var isSame = await _bcrypt.default.compare(args.password, data.password.toString());

      if (isSame) {
        setToken({
          username: data.username,
          shop: data.shop,
          res
        });
        return publicToken(data.username, data.shop ? 'seller' : 'user');
      } else {
        throw Error('invalid credentials');
      }
    }
  },
  userInfo: {
    type: UserType,

    resolve(parent, {
      tokenBody
    }) {
      return _user.default.findOne({
        username: tokenBody.username
      });
    }

  }
};
exports.UserQuery = UserQuery;
const UserMutation = {
  register: {
    type: _graphql.GraphQLString,
    args: {
      username: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      },
      password: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      },
      email: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      }
    },

    async resolve(parent, args, {
      res
    }) {
      var user = new _user.default({
        username: args.username,
        password: args.password,
        email: args.email
      });
      user.save().then(reply => {
        setToken({
          username: reply.username.toString(),
          shop: null,
          res
        });
        return publicToken(reply.username, 'user');
      }).catch(error => {
        throw Error(error);
      });
    }

  }
};
exports.UserMutation = UserMutation;
const jwtPrivateKey = 'dhdUGdfJKJHdsYdfksd';
/**
 * 
 * @param req client request object with cookie field
 *        extracts user information from req obj
 */

function extractTokenBody(req) {
  var tokenBody = {};

  if (req.cookies['token']) {
    tokenBody = _jsonwebtoken.default.verify(req.cookies['token'], jwtPrivateKey);
  }

  return tokenBody;
}

function setToken({
  username,
  shop = null,
  res
}) {
  const token = _jsonwebtoken.default.sign({
    username,
    shop
  }, jwtPrivateKey, {
    expiresIn: '7d'
  });

  res.cookie('token', token, {
    expiresIn: 10000 * 60 * 60 * 24 * 7
  });
}

function publicToken(username, rights) {
  return _jsonwebtoken.default.sign({
    login: true,
    rights,
    username
  }, 'sdfladklfsuKJHKsda', {
    expiresIn: '7d'
  });
}