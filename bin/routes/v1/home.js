"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HomeQuery = exports.HomeType = void 0;

var _redis = _interopRequireDefault(require("redis"));

var _graphql = require("graphql");

var _asyncRedis = _interopRequireDefault(require("async-redis"));

var _product = require("./product/product");

var _fetch = require("./redis/fetch");

var _product2 = _interopRequireDefault(require("../../models/product.model"));

var _user = require("./user/user");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let conn = _asyncRedis.default.decorate(_redis.default.createClient());

const HomeType = new _graphql.GraphQLObjectType({
  name: 'Home',
  fields: () => ({
    topic: {
      type: _graphql.GraphQLString
    },
    products: {
      type: (0, _graphql.GraphQLList)(_product.ProductType)
    }
  })
});
exports.HomeType = HomeType;
const HomeQuery = {
  home: {
    type: (0, _graphql.GraphQLList)(HomeType),
    args: {
      screenWidth: {
        type: _graphql.GraphQLInt
      },
      screenHeight: {
        type: _graphql.GraphQLInt
      },
      scroll: {
        type: _graphql.GraphQLInt
      }
    },

    async resolve(parent, args, {
      req,
      res
    }) {
      if (req.headers['cookie']) {
        /* get topics based what user usually views */
        var data = (0, _user.extractTokenBody)(req);
        var topics = await conn.zrevrange('user:' + data.user + ':topic', 0, 10);

        if (topics.length == 0) {
          //if user has no prior viewed topics fetch common
          topics = await conn.zrevrange('topic:top', 0, 10);
        } else if (topics.length < 10) {
          var extra_topics = await conn.zrevrange('topic:top', 0, 10 - topics.length);

          for (var i = 0; i < extra_topics.length; i++) {
            topics.push(extra_topics);
          }
        }

        return getTopicProducts(topics);
      } else {
        /* get list of top 10 popular topics */
        var topics = await conn.zrevrange('topic:top', 0, 10);
        return getTopicProducts(topics);
      }
    },

    recommended: {
      type: (0, _graphql.GraphQLList)(HomeType),
      args: {
        sku: {
          type: _graphql.GraphQLID
        }
      },

      resolve(parent, args) {
        /** find related products */
      }

    }
  }
};
exports.HomeQuery = HomeQuery;

async function getTopicProducts(topics) {
  var return_data = [];
  /** foreach topic get top products */

  let {
    products,
    not_found
  } = await (0, _fetch.get_products)(conn, 0, topics);
  /**if some items data aren't in the cache visit database */

  for (var i = 0; i < not_found.length; i++) {
    if (not_found[i][topics[i]].length > 0) {
      var db_product = await _product2.default.find({
        sku: {
          $in: not_found[i][topics[i]]
        }
      });
      db_product.forEach(prod => {
        products[topics[i]].push(prod);
      });
    }
  } //format return data to match api style


  for (var i = 0; i < topics.length; i++) {
    return_data.push({
      topic: topics[i],
      products: products[topics[i]]
    });
  }

  return return_data;
}