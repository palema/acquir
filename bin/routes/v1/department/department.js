"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DepartmentQuery = exports.DepartmentType = void 0;

var _graphql = require("graphql");

var _department = _interopRequireDefault(require("../../../models/department.model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DepartmentType = new _graphql.GraphQLObjectType({
  name: 'Department',
  fields: () => ({
    name: {
      type: _graphql.GraphQLString
    },
    parent: {
      type: DepartmentType,

      resolve(parent) {
        return _department.default.findOne({
          name: parent.parent
        });
      }

    },
    children: {
      type: (0, _graphql.GraphQLList)(DepartmentType),

      resolve(parent) {
        return _department.default.find({
          parent: parent.name
        });
      }

    }
  })
});
exports.DepartmentType = DepartmentType;
const DepartmentQuery = {
  department: {
    type: DepartmentType,
    args: {
      name: {
        type: _graphql.GraphQLID
      }
    },

    resolve(parent, args) {
      return _department.default.findOne({
        name: args.name
      });
    }

  },
  departments: {
    type: (0, _graphql.GraphQLList)(DepartmentType),

    resolve(parent, args) {
      return _department.default.findOne({
        name: args.name
      });
    }

  },
  rootDepartment: {
    type: (0, _graphql.GraphQLList)(DepartmentType),

    resolve() {
      return _department.default.find({
        parent: "none"
      });
    }

  }
};
exports.DepartmentQuery = DepartmentQuery;