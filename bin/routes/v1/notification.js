"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _webPush = _interopRequireDefault(require("web-push"));

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var notification = (0, _express.default)(); //push notification

const public_vapid_key = 'BB8Cmu_ps2h5xs6Mbi1qwzXiQr7bZYdA0DkWEC276HTCng2F3pdg3jwTsJrJO7cUEmyqjE6_iNC61Q7IOEHgD8k';
const private_vapid_key = 'wYU09ARY0v7kmmltbQOVhAZ2GVWT4-UunTz6OD6lyRs';

_webPush.default.setVapidDetails('mailto:test@test.com', public_vapid_key, private_vapid_key); // // Subscribe Route


notification.post('/subscribe', (req, res) => {
  //Get push subcription obj send to 
  console.log('receive push');
  const subscription = req.body;
  res.status(201).json({});
  const payload = JSON.stringify({
    title: 'push test'
  }); //pass obj into sendNotification

  console.log('sending push');

  _webPush.default.sendNotification(subscription, payload).catch(err => console.log(err));
});
var _default = notification;
exports.default = _default;