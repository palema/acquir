"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProductMutation = exports.ProductQuery = exports.ProductType = void 0;

var _graphql = require("graphql");

var _department = require("../department/department");

var _product = _interopRequireDefault(require("../../../models/product.model"));

var _department2 = _interopRequireDefault(require("../../../models/department.model"));

var _shop = require("../shop/shop");

var _shop2 = _interopRequireDefault(require("../../../models/shop.model"));

var _userAction = require("../redis/userAction");

var _asyncRedis = _interopRequireDefault(require("async-redis"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const conn = _asyncRedis.default.createClient();

const ProductType = new _graphql.GraphQLObjectType({
  name: 'Product',
  fields: () => ({
    title: {
      type: _graphql.GraphQLString
    },
    list_price: {
      type: _graphql.GraphQLString
    },
    sku: {
      type: _graphql.GraphQLString
    },
    quantity: {
      type: _graphql.GraphQLInt
    },
    display_image: {
      type: _graphql.GraphQLString
    },
    rating: {
      type: _graphql.GraphQLInt
    },
    purchases: {
      type: _graphql.GraphQLInt
    },
    views: {
      type: _graphql.GraphQLInt
    },
    cart_listings: {
      type: _graphql.GraphQLInt
    },
    department: {
      type: (0, _graphql.GraphQLList)(_department.DepartmentType),

      resolve(parent) {
        return _department2.default.find({
          department: {
            $in: parent.department
          }
        });
      }

    },
    shop: {
      type: _shop.ShopType,

      resolve(parent) {
        return _shop2.default.findOne({
          name: parent.shop
        });
      }

    }
  })
});
exports.ProductType = ProductType;
const ProductFeatureType = new _graphql.GraphQLInputObjectType({
  name: 'Feature',
  fields: () => ({
    name: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
    },
    value: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
    },
    unit: {
      type: _graphql.GraphQLString
    }
  })
});
const ProductQuery = {
  product: {
    type: ProductType,
    args: {
      title: {
        type: _graphql.GraphQLString
      },
      sku: {
        type: _graphql.GraphQLString
      }
    },

    resolve(parent, args, {
      req,
      user
    }) {
      return _product.default.findOne({
        title: args.title
      });
    }

  }
};
exports.ProductQuery = ProductQuery;
const ProductMutation = {
  addProduct: {
    type: ProductType,
    args: {
      title: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      },
      list_price: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLFloat)
      },
      quantity: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLInt)
      },
      display_image: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      },
      department: {
        type: (0, _graphql.GraphQLNonNull)((0, _graphql.GraphQLList)((0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)))
      },
      features: {
        type: (0, _graphql.GraphQLList)(ProductFeatureType)
      }
    },

    resolve(parent, args, {
      tokenBody
    }) {
      if (!tokenBody.shop) {
        throw new Error('shop not registered');
      }

      let sku = gen_sku(args.title, tokenBody.shop);
      let product = new _product.default({
        title: args.title,
        list_price: args.list_price,
        quantity: args.quantity,
        display_image: args.display_image,
        sku: sku,
        specs: args.features,
        shop: tokenBody.shop,
        department: args.department
      }); // redis_func.add_product(conn, args.shop, args.title, sku)
      // redis_func.add_remove_topics(conn, sku, args.department)

      return product.save();
    }

  },
  deleteProduct: {
    type: ProductType,
    args: {
      title: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      }
    },

    resolve(parent, args, {
      tokenBody
    }) {
      if (!tokenBody.shop) {
        throw new Error('Not authenticated');
      }

      return _product.default.findOneAndDelete({
        title: args.title
      });
    }

  },
  addToCart: {
    type: ProductType,
    args: {
      sku: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      },
      quantity: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLInt)
      }
    },

    resolve(parent, args, {
      tokenBody
    }) {
      if (!tokenBody.user) {
        throw new Error('Not authenticated');
      }

      (0, _userAction.add_to_cart)({
        conn,
        user: tokenBody.user,
        sku: args.sku,
        count: args.quantity
      });
    }

  }
};
exports.ProductMutation = ProductMutation;

function gen_sku(title, shop) {
  return title.substr(0, 5) + shop.substr(0, 5) + Math.floor(Math.random() * 1000);
}