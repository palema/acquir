"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.add_product = exports.product_vote = void 0;

var _lodash = require("lodash");

var _bluebird = _interopRequireDefault(require("bluebird"));

var _redis = _interopRequireDefault(require("redis"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_bluebird.default.promisifyAll(_redis.default.RedisClient.prototype);

_bluebird.default.promisifyAll(_redis.default.Multi.prototype);

const ONE_WEEK_IN_SECONDS = 7 * 86400;
const VOTE_SCORE = 423;

const product_vote = ({
  conn,
  user,
  sku
}) => {
  let cutoff = new Date().getTime() - ONE_WEEK_IN_SECONDS;
  conn.zscore('time:', sku, (err, reply) => {
    console.log("valid time");

    if (parseFloat(reply) < cutoff) {
      console.log("cannot view");
      conn.sadd('viewed:' + sku, user, (err, reply) => {
        console.log(reply, " can vote");

        if (reply) {
          console.log("added viewed");
          conn.zincrby('score:', VOTE_SCORE, sku);
          conn.hincrby('product:' + sku, 'votes', 1);
        }
      });
    }
  });
};

exports.product_vote = product_vote;

const add_product = async (conn, shop, title, sku) => {
  conn.incr('product:', (err, reply) => {
    let viewed = 'viewed:' + sku;
    conn.sadd(viewed, shop);
    conn.expire(viewed, ONE_WEEK_IN_SECONDS);
    let now = new Date().getTime();
    let product = 'product:' + sku;
    conn.hmset(product, {
      'title': title,
      'shop': shop,
      'time': now,
      'views': 1
    });
    conn.zadd('score:', now + VOTE_SCORE, product, (err, reply) => {
      console.log(reply);
    });
    conn.zadd('time:', now, sku, (err, reply) => {
      console.log(reply);
    });
  });
};

exports.add_product = add_product;
const PRODUCTS_PER_PAGE = 10;

const get_products = (conn, page, order = 'score') => {
  let start = (page - 1) * PRODUCTS_PER_PAGE;
  let end = start + PRODUCTS_PER_PAGE - 1;
  let skus = [];
  conn.zrevrange(order, start, end, (err, reply) => {
    skus = reply.splice(0, -1);
  });
  let products = [];
  skus.forEach(sku => {
    conn.hgetall(sku, (err, reply) => {
      let product_data = reply;
      product_data['sku'] = sku;
      products.push(product_data);
    });
  });
};

const add_remove_groups = (conn, sku, to_add = [], to_remove = []) => {
  let product = 'product:' + sku;
  to_add.forEach(group => {
    conn.sadd('group:' + group, product);
  });
  to_remove.forEach(group => {
    conn.srem('group:' + group, product);
  });
};

const get_group_product = (conn, group, page, order = 'score') => {
  let key = order + group;
  conn.exists(key, (err, reply) => {
    if (!reply) {
      conn.zinterstore(key, ['group:' + group, order]);
    }

    conn.expire(key, 60);
  });
  return get_products(conn, page, key);
};

const check_token = (conn, token) => {
  return conn.hget('login:', token);
};

const update_token = (conn, token, user, item = null) => {
  let timestamp = new Date().getTime;
  conn.hset('login:', token, user);
  conn.zadd('recent:', token, timestamp);

  if (item) {
    conn.zadd('viewed:' + token, item, timestamp);
    conn.zremrangebyrank('viewed:' + token, 0, -26);
    conn.zincrby('viewed:', item, '-1');
  }
};

let QUIT = false;
let LIMIT = 5;

const clean_sessions = conn => {
  while (!QUIT) {
    conn.zcard('recent:', (err, size) => {
      if (size <= LIMIT) {
        setInterval(() => {
          clean_sessions;
        }, 1000);
      }

      let end_index = (0, _lodash.min)([size - LIMIT, 100]);
      conn.zrange('recent:', 0, end_index - 1, (err, sessions) => {
        let session_keys = [];
        sessions.forEach(session => {
          session_keys.push('viewed:' + session);
          session_keys.push('cart:' + session);
        });
        conn.del(session_keys);
        conn.hdel('login:', sessions);
        conn.zrem('recent:', sessions);
      });
    });
  }
};

const add_to_cart = (conn, session, item, count) => {
  if (count <= 0) {
    conn.hdel('cart:' + session, item);
  } else {
    conn.hset('cart:' + session, item, count.toString());
  }
};

const rescale_viewed = conn => {
  while (!QUIT) {
    conn.zremrangebyrank('viewed:', 20000, -1);
    conn.zinterstore('viewed', 0.5);
  }
};