"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.get_products = void 0;

const get_products = async (conn, count, topics) => {
  let products = [];
  let not_found = [];

  for (var i = 0; i < topics.length; i++) {
    var skus = await conn.zrevrange('topic:' + topics[i], 0, count + 5);
    var temp_not_found = {};
    products[topics[i]] = [];
    temp_not_found[topics[i]] = [];

    for (var j = 0; j < skus.length; j++) {
      /* gets top products info */
      var product_data = await conn.hgetall('product:' + skus[j]);

      if (product_data) {
        product_data['sku'] = skus[j];
        products[topics[i]].push(product_data);
      } else {
        temp_not_found[topics[i]].push(skus[j]);
      }
    }

    not_found.push(temp_not_found);
  }

  return {
    products,
    not_found
  };
};

exports.get_products = get_products;