"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.product_rate = exports.createOrder = exports.add_to_order = exports.add_to_cart = exports.update_token = void 0;
const VOTE_SCORE = 423;
/* tracks user login sessions */

const update_token = (conn, token, user) => {
  let timestamp = new Date().getTime();
  conn.hset('login:', token, user);
  conn.zadd('recent:', token, timestamp);
};
/* adds product to user's cart and updates products cart inclusion count */


exports.update_token = update_token;

const add_to_cart = ({
  conn,
  user,
  sku,
  count
}) => {
  var res = null;

  if (count <= 0) {
    return conn.hdel('user:' + user + ':cart', sku).then(reply => {
      return conn.hincrby('product:' + sku, 'carts', -count);
    }).then(reply => {
      return reply;
    }).catch(err => {
      return err;
    });
  } else {
    return conn.hincrby('user:' + user + ':cart', sku, count).then(reply => {
      return conn.hincrby('product:' + sku, 'carts', count);
    }).then(reply => {
      return reply;
    }).catch(err => {
      return err;
    });
  }
};

exports.add_to_cart = add_to_cart;

const add_to_order = ({
  conn,
  token,
  items,
  orderNo = null,
  total
}) => {
  for (var i = 0; i < items.length; i++) {
    conn.hmset('user:' + token + ':order:' + orderNo, items[i].sku, items[i].quantity);
  }

  conn.hincrby('user:' + token + ':order:' + orderNo, 'total', total.toString());
};

exports.add_to_order = add_to_order;

const createOrder = async ({
  conn,
  token,
  items,
  total,
  orderNo,
  city,
  street,
  erf,
  password,
  shop
}) => {
  try {
    for (var i = 0; i < items.length; i++) {
      conn.hmset('user:' + token + ':order:' + orderNo, items[i].sku, items[i].quantity);
    }

    return await conn.hmset('user:' + token + ':order:' + orderNo, 'shop', shop, 'city', city, 'street', street, 'erf', erf, 'password', password, 'total', total.toString());
  } catch (err) {
    throw Error(err);
  }
};

exports.createOrder = createOrder;

const complete_purchase = (conn, sku, qty) => {
  conn.hincrby('product:' + sku, 'purchases', qty);
  conn.hincrby('product:' + sku, 'quantity', -qty);
};
/* handles product rating */


const product_rate = ({
  conn,
  user,
  sku
}) => {
  var reply = conn.sadd('rated:' + sku, user);

  if (reply) {
    conn.zincrby('score:', VOTE_SCORE, sku);
    conn.hincrby('product:' + sku, 'views', 1);
  }
};

exports.product_rate = product_rate;