"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OrderQuery = exports.OrderMutation = exports.OrderType = void 0;

var _product = _interopRequireDefault(require("../../models/product.model"));

var _user = _interopRequireDefault(require("../../models/user.model"));

var _server = require("../../server");

var _graphql = require("graphql");

var _product2 = require("./product/product");

var _order = require("../../models/order.model");

var _negotiation = require("../../models/negotiation.model");

var _userAction = require("../../routes/v1/redis/userAction");

var _asyncRedis = _interopRequireDefault(require("async-redis"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var conn = _asyncRedis.default.createClient();

var NOT_LOGGED_IN = 'not logged in';
var NOT_HAVING_SHOP = 'not having shop';
const LocationType = new _graphql.GraphQLObjectType({
  name: 'Location',
  fields: () => ({
    city: {
      type: _graphql.GraphQLString
    },
    street: {
      type: _graphql.GraphQLString
    },
    erf: {
      type: _graphql.GraphQLString
    },
    latitude: {
      type: _graphql.GraphQLFloat
    },
    longitude: {
      type: _graphql.GraphQLFloat
    }
  })
});
const LocationInputType = new _graphql.GraphQLInputObjectType({
  name: 'LocationInput',
  fields: () => ({
    city: {
      type: _graphql.GraphQLString
    },
    street: {
      type: _graphql.GraphQLString
    },
    erf: {
      type: _graphql.GraphQLString
    },
    latitude: {
      type: _graphql.GraphQLFloat
    },
    longitude: {
      type: _graphql.GraphQLFloat
    }
  })
});
const OrderResponseType = new _graphql.GraphQLObjectType({
  name: 'OrderResponse',
  fields: () => ({
    number: {
      type: _graphql.GraphQLString
    },
    password: {
      type: _graphql.GraphQLString
    }
  })
});
const OrderItemType = new _graphql.GraphQLInputObjectType({
  name: 'OrderItem',
  fields: () => ({
    sku: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
    },
    quantity: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLInt)
    }
  })
});
const OrderType = new _graphql.GraphQLObjectType({
  name: 'Order',
  fields: () => ({
    number: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
    },
    products: {
      type: (0, _graphql.GraphQLList)((0, _graphql.GraphQLNonNull)(TransactionProductType))
    },
    total: {
      type: _graphql.GraphQLFloat
    },
    deliveryLocationType: {
      type: LocationType
    },
    status: {
      type: new _graphql.GraphQLEnumType({
        name: 'status',
        values: {
          fulifilled: {
            value: 'fulfilled'
          },
          pending: {
            value: 'pending'
          },
          enroute: {
            value: 'enroute'
          }
        }
      })
    },
    shop: {
      type: _graphql.GraphQLString
    },
    user: {
      type: _graphql.GraphQLString
    },
    password: {
      type: _graphql.GraphQLString
    }
  })
});
exports.OrderType = OrderType;
const TransactionProductType = new _graphql.GraphQLObjectType({
  name: 'TransactionProduct',
  fields: () => ({
    quantity: {
      type: _graphql.GraphQLInt
    },
    product: {
      type: _product2.ProductType,

      resolve(parent) {
        return _product.default.findOne({
          sku: parent.sku
        }, {
          _id: 0,
          title: 1,
          department: 1
        });
      }

    }
  })
});
const BidType = new _graphql.GraphQLObjectType({
  name: 'BidType',
  fields: () => ({
    shop: {
      type: _graphql.GraphQLString
    },
    offer: {
      type: _graphql.GraphQLFloat
    },
    products: {
      type: (0, _graphql.GraphQLList)(TransactionProductType)
    },
    status: {
      type: _graphql.GraphQLString
    },
    user: {
      type: _graphql.GraphQLString
    }
  })
});
const OrderMutation = {
  /** method for handling ordering out of cart */
  cartOrder: {
    type: OrderResponseType,
    //returns order number and password
    args: {
      cartNumber: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      }
    },

    async resolve(parent, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not Signed Up');
      }
      /** get cart items from redis cache and store them in db */


      orderHandler(_server.connection, args.items, tokenBody.username).then(reply => {
        if (reply) {}
      }).catch(error => {
        return error;
      });
    }

  },

  /** method for handling direct order not from cart */
  quickOrder: {
    type: _graphql.GraphQLString,
    args: {
      sku: {
        type: _graphql.GraphQLString
      },
      shop: {
        type: _graphql.GraphQLString
      },
      quantity: {
        type: _graphql.GraphQLInt
      }
    },

    async resolve(parent, args, {
      tokenBody,
      redisConn
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged In');
      }

      return orderHandler(_server.connection, [{
        sku: args.sku,
        quantity: args.quantity
      }], tokenBody.username).then(async () => {
        /** first check if user has orders to this shop */
        var order = await _order.Order.findOne({
          user: tokenBody.username,
          shop: args.shop,
          status: 'pending'
        });

        if (order) {
          var dbProducts = order.products;
          var found = false;

          for (var i = 0; i < dbProducts.length; i++) {
            if (dbProducts[i].sku == args.sku) {
              //if item already exists then increament
              dbProducts[i].quantity += args.quantity;
              found = true;
            }
          }

          if (!found) {
            //else append new item
            dbProducts.push({
              sku: args.sku,
              quantity: args.quantity
            });
          }

          return _order.Order.update({
            number: order.number
          }, {
            $set: {
              products: dbProducts
            }
          }).then(res => {
            return 'item added';
          }).catch(err => {
            return err;
          });
        } else {
          var password = '3452';
          var number = '33';
          var newOrder = new _order.Order({
            products: [{
              sku: args.sku,
              quantity: args.quantity
            }],
            password,
            user: tokenBody.username,
            number,
            shop: args.shop
          });
          newOrder.save();
          return password;
        }
      }).catch(error => {
        return error;
      });
    }

  },
  updateOrder: {
    type: _graphql.GraphQLString,
    args: {
      cartId: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      },
      toAdd: {
        type: OrderItemType
      },
      toRemove: {
        type: OrderItemType
      }
    },

    async resolve(parent, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged In');
      }

      if (args.toAdd) {}

      if (args.toRemove) {}
    }

  },
  cancelOrder: {
    type: _graphql.GraphQLString,
    args: {
      number: {
        type: _graphql.GraphQLString
      }
    },

    async resolve(parent, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged In');
      } // to cancel order set it's status to false


      return _order.Order.update({
        number: args.number
      }, {
        $set: {
          status: 'cancelled'
        }
      }).then(reply => {
        return reply;
      }).catch(err => {
        return err;
      });
    }

  },
  confirmDelivery: {
    //customer enters password to to confirm delivery
    type: _graphql.GraphQLString,
    args: {
      password: {
        type: _graphql.GraphQLString
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged In');
      }

      return _negotiation.Negotiation.findOneAndUpdate({
        number: args.number,
        password: args.password
      }, {
        $set: {
          status: 'delivered'
        }
      }).then(reply => {
        return 'delivered';
      }).catch(error => {
        return error;
      });
    }

  },
  quickBid: {
    type: _graphql.GraphQLString,
    args: {
      sku: {
        type: _graphql.GraphQLString
      },
      quantity: {
        type: _graphql.GraphQLInt
      },
      offer: {
        type: _graphql.GraphQLFloat
      },
      shop: {
        type: _graphql.GraphQLString
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged in');
      }

      var bid = new _negotiation.Negotiation({
        number: 'ee',
        products: [{
          sku: args.sku,
          quantity: args.quantity
        }],
        shop: args.shop,
        offer: args.offer,
        user: tokenBody.username
      });
      return bid.save().then(reply => {
        return 'bid made';
      }).catch(err => {
        return err;
      });
    }

  },

  /** bid for multiple items */
  batchBid: {
    type: _graphql.GraphQLString,
    args: {
      sku: {
        type: _graphql.GraphQLString
      },
      quantity: {
        type: _graphql.GraphQLInt
      },
      offer: {
        type: _graphql.GraphQLFloat
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged In');
      }

      var bid = new _negotiation.Negotiation({
        number: 'e',
        products: [{}]
      });
    }

  },
  acceptBid: {
    type: _graphql.GraphQLString,
    args: {
      number: {
        type: _graphql.GraphQLString
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.shop) {
        throw Error('Not shop');
      }

      return _negotiation.Negotiation.findOneAndUpdate({
        number: args.number
      }, {
        $set: {
          status: 'accepted'
        }
      }).then(reply => {
        return 'offer accepted';
      }).catch(error => {
        return error;
      });
    }

  },
  rejectBid: {
    type: _graphql.GraphQLString,
    args: {
      number: {
        type: _graphql.GraphQLString
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.shop) {
        throw Error('Not shop');
      }

      return _negotiation.Negotiation.findOneAndUpdate({
        number: args.number
      }, {
        $set: {
          status: 'rejected'
        }
      }).then(reply => {
        return 'offer rejected';
      }).catch(error => {
        return error;
      });
    }

  },
  counterBid: {
    type: _graphql.GraphQLString,
    args: {
      number: {
        type: _graphql.GraphQLString
      },
      offer: {
        type: _graphql.GraphQLFloat
      },
      //amount to counter bid by
      quantity: {
        type: _graphql.GraphQLInt
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.shop) {
        throw Error('Not shop');
      }
      /** set counter field */


      return _negotiation.Negotiation.findOneAndUpdate({
        number: args.number
      }, {
        $set: {
          counter: args.counter
        }
      }).then(reply => {
        return 'offer countered';
      }).catch(error => {
        return error;
      });
    }

  },
  updateBid: {
    type: _graphql.GraphQLString,
    args: {
      number: {
        type: _graphql.GraphQLString
      },
      offer: {
        type: _graphql.GraphQLFloat
      },
      quantity: {
        type: _graphql.GraphQLInt
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged In');
      }

      return _negotiation.Negotiation.findOneAndUpdate({
        number: args.number
      }, {
        $set: {
          offer: args.counter
        }
      }).then(reply => {
        return 'offer countered';
      }).catch(error => {
        return error;
      });
    }

  },
  addToCart: {
    type: _graphql.GraphQLString,
    args: {
      sku: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
      },
      quantity: {
        type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLInt)
      }
    },

    async resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error('Not logged In');
      }

      return (0, _userAction.add_to_cart)({
        conn,
        user: tokenBody.username,
        sku: args.sku,
        count: args.quantity
      }).then(reply => {
        return 'item added';
      }).catch(err => {
        return err;
      });
    }

  }
};
exports.OrderMutation = OrderMutation;
const OrderQuery = {
  userOrder: {
    type: (0, _graphql.GraphQLList)(OrderType),
    args: {},

    resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error(NOT_LOGGED_IN);
      }

      return _order.Order.find({
        user: tokenBody.username,
        status: {
          $ne: 'delivered'
        }
      }).then(reply => {
        return reply;
      }, error => {
        return error;
      });
    }

  },
  userOffer: {
    type: (0, _graphql.GraphQLList)(BidType),

    resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.username) {
        throw Error(NOT_LOGGED_IN);
      }

      return _negotiation.Negotiation.find({
        user: tokenBody.username
      }).then(reply => {
        return reply;
      }, error => {
        return error;
      });
    }

  },
  shopOrder: {
    type: OrderType,
    args: {},

    resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.shop) {
        throw Error(NOT_HAVING_SHOP);
      }

      return _order.Order.find({
        shop: tokenBody.shop
      }).then(reply => {
        return reply;
      }, error => {
        return error;
      });
    }

  },
  shopOffer: {
    type: BidType,
    args: {},

    resolve(_, args, {
      tokenBody
    }) {
      if (!tokenBody.shop) {
        throw Error(NOT_HAVING_SHOP);
      }

      return _negotiation.Negotiation.find({
        shop: tokenBody.shop
      }).then(reply => {
        return reply;
      }, error => {
        return error;
      });
    }

  }
};
exports.OrderQuery = OrderQuery;

async function orderHandler(conn, order, username) {
  var session = await conn.startSession({
    defaultTransactionOptions: {}
  });
  session.startTransaction({
    readConcern: {
      level: 'snapshot'
    },
    writeConcern: {
      w: 'majority'
    }
  });

  try {
    var skus = [];

    for (var i = 0; i < order.length; i++) {
      skus.push(order[i].sku);
    }
    /** find items user wishes to purchase */


    var foundItems = await _product.default.find({
      sku: {
        $in: skus
      }
    }, {
      _id: 0,
      quantity: 1,
      list_price: 1,
      title: 1,
      sku: 1
    });

    if (order.length != foundItems.length) {
      //if some are not found ask user how to proceed
      var not_found = [];

      for (var i = 0; i < order.length; i++) {
        var found = false;

        for (var j = 0; j < foundItems.length; j++) {
          var tmp_item = foundItems[i].toJSON();

          if (order[i].sku == tmp_item.sku) {
            found = true;
            return;
          }
        }

        if (found) {
          found = false;
        } else {
          not_found.push(order[i].sku);
        }
      }

      return not_found;
    }

    var totalCost = 0;
    /** calculate total cost of items */

    for (var i = 0; i < foundItems.length; i++) {
      var tmp_item = foundItems[i].toJSON();
      totalCost += order[i].quantity * tmp_item.list_price;
    }

    var userInfo = await _user.default.findOne({
      username
    }, {
      balance: 1
    });

    if (userInfo.balance < totalCost) {
      throw Error('Insufficient amount');
    }
    /** deduct total from user balance */


    await _user.default.updateOne({
      username: username
    }, {
      $inc: {
        balance: -totalCost
      }
    });

    for (var i = 0; i < foundItems.length; i++) {
      var tmp_item = foundItems[i].toJSON();

      for (var j = 0; order.length; j++) {
        if (order[j].sku == tmp_item.sku) {
          var m = await _product.default.updateOne({
            sku: tmp_item.sku
          }, {
            $inc: {
              quantity: -order[j].quantity
            }
          });

          if (!m.nModified) {
            /** verify that each document was modified */
            throw Error('an error occured'); //happens when fails to update document
          }
        }
      }
    }

    return;
  } catch (err) {
    session.abortTransaction();
    throw Error(err);
  }
}