"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _department = _interopRequireDefault(require("../models/department.model"));

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var product_schema = _mongoose.default.model('Product');

let common_api = _express.default.Router();

const not_found = {
  status: false,
  message: 'not matches found' // common_api.route('/insertDepartment').post((req, res) => {
  // 	var r = req.body
  // 	const department = new Department({
  // 		name: r.name,
  // 		keywords: r.keywords,
  // 		features: r.features
  // 	})
  // 	department.save((err, doc) => {
  // 		res.send(doc)
  // 	})
  // })

};
common_api.route('/findDepartment').get((req, res) => {
  _department.default.find((err, docs) => {
    if (err) res.send(new Error('An error occured : ' + err));
    if (docs) res.send(docs);
  });
});

const search = (req, res, next) => {
  let name = req.params.q;
  let escaped = name.replace(/[\\[.+*?(){|^$]/g, "\\$&");
  product_schema.aggregate([{
    $match: {
      $or: [{
        title: new RegExp(`${escaped}`, 'gi')
      }, {
        brief_description: ''
      }]
    }
  }, {
    $sort: {
      'quantity': -1
    }
  }, {
    $limit: 50
  }], (err, docs) => {
    if (err) return next({
      status: false,
      message: 'Internal server error'
    });
    if (!docs) return next({
      status: false,
      message: 'No match found'
    });else res.send(docs);
  });
};

common_api.route('/search/:q').get(search);
var _default = common_api;
exports.default = _default;