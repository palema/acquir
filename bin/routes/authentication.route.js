"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.destroy_session = exports.check_session = exports.set_session = exports.save_user = exports.find_by_email = void 0;

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _user = _interopRequireDefault(require("../models/user.model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var port = process.env.PORT || 7200;

const find_by_email = function (req, res, next) {
  const email = req.body.email;
  const password = req.body.password;

  _user.default.findOne({
    email: email
  }, (err, data) => {
    if (err) {
      return next(err);
    }

    if (!data) {
      return res.json({
        status: false,
        message: 'incorect email or password'
      });
    }

    if (data.shop) {
      req.params.shop = data.shop;
    }

    _bcrypt.default.compare(password, data.password, function (err, result) {
      if (err) return next(err);
      if (!result) return res.json({
        status: false,
        message: 'incorect email or password'
      });
      return next();
    });
  });
};

exports.find_by_email = find_by_email;

const save_user = (req, res, next) => {
  const user = new _user.default(req.body);
  user.save((err, data) => {
    if (err) return next(err);else return next();
  });
};

exports.save_user = save_user;

const set_session = (req, res) => {
  req.session.email = req.body.email;

  if (req.params.shop) {
    req.session.shop = req.params.shop;
  }

  res.send({
    status: true,
    message: 'successful'
  });
};

exports.set_session = set_session;

const check_session = (req, res) => {
  res.json({
    status: !!req.session.email
  });
};

exports.check_session = check_session;

const destroy_session = (req, res) => {
  req.session.destroy();
  res.send({
    status: true,
    message: "logged out"
  });
};

exports.destroy_session = destroy_session;