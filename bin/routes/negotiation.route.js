"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.find_product = find_product;
exports.find_negotiation_user = find_negotiation_user;
exports.find_negotiation_shop = find_negotiation_shop;
exports.update_negotiation = update_negotiation;
exports.create_negotiation = create_negotiation;
exports.default = void 0;

var _negotiation = _interopRequireDefault(require("../models/negotiation.model"));

var _product = _interopRequireDefault(require("../models/product.model"));

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let negotiation_api = _express.default.Router();

const not_found_err = {
  status: false,
  message: 'Item not found'
};

function find_product(req, res, next) {
  console.log('searched');

  if (req.session.email) {
    const form = req.body;
    console.log('searchedw');

    _product.default.findOne({
      sku: form.sku
    }, (err, doc) => {
      if (err) return res.send(err);
      if (!doc) return res.send(not_found_err);
      req.params.cost = doc.list_price.valueOf() * form.quantity;
      req.params.title = doc.title;
      next();
    });
  } else {
    return res.send({
      status: false,
      message: 'not logged in'
    });
  }
}

function find_negotiation_user(req, res, next) {
  _negotiation.default.findOne({
    buyer: req.session.email
  }, (err, doc) => {
    if (err) return res.json(err);
    if (!doc) return res.json(not_found_err);
    req.params.ngt_doc = doc;
    next();
  });
}

function find_negotiation_shop(req, res, next) {
  _negotiation.default.findOne({
    shop: req.body.buyer
  }, (err, doc) => {
    if (err) return res.json(err);
    if (!doc) return res.json(not_found_err);
    req.params.ngt_doc = doc;
    next();
  });
}

function update_negotiation(req, res, next) {
  const tmp_negotiation = req.params.ngt_doc;
  const counter_doc = req.body;

  try {
    for (var i = 0; i < tmp_negotiation.products.length; i++) {
      if (tmp_negotiation.products[i].sku == counter_doc.sku) {
        tmp_negotiation.products[i].offer = counter_doc.offer;

        _negotiation.default.updateOne({
          buyer: counter_doc.buyer
        }, tmp_negotiation, (err, result) => {
          if (err) return res.send(err);
          if (result) return res.send({
            status: true,
            message: "successful"
          });
        });
      }
    }
  } catch (err) {
    return res.send(err);
  }
}

function create_negotiation(req, res, next) {
  if (req.session.email) {
    const form = req.body;
    const neg = new _negotiation.default({
      buyer: req.session.email,
      products: [{
        sku: form.sku,
        title: req.params.title,
        offer: form.offer,
        quantity: form.quantity,
        shop: form.shop
      }]
    });
    neg.save((err, response) => {
      if (err) return res.send(err);
      if (response) return res.send({
        status: true,
        message: 'bid set'
      });
    });
  } else {
    return res.send({
      status: false,
      message: 'user not signed in'
    });
  }
}

negotiation_api.route('/new').post(find_product, create_negotiation);
negotiation_api.route('/b/update').post(find_product, find_negotiation_user, update_negotiation);
negotiation_api.route('/s/update').post(find_product, find_negotiation_shop, update_negotiation);
negotiation_api.route('/get').get((req, res) => {
  if (req.session.email) {
    _negotiation.default.findOne({
      buyer: req.session.email
    }, {
      products: true,
      _id: false
    }, (err, doc) => {
      if (err) res.send(err);
      if (doc) res.send(doc);
    });
  }
});
var _default = negotiation_api;
exports.default = _default;