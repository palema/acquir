"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _fs = _interopRequireDefault(require("fs"));

var _expressSession = _interopRequireDefault(require("express-session"));

var _product = _interopRequireDefault(require("./product.route"));

var _order = _interopRequireDefault(require("./order.route"));

var _common = _interopRequireDefault(require("./common.route"));

var _negotiation = _interopRequireDefault(require("./negotiation.route"));

var _user = _interopRequireDefault(require("./user.route"));

var _shop = _interopRequireDefault(require("./shop.route"));

var _home = _interopRequireDefault(require("./home.route"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RedisStore = require('connect-redis')(_expressSession.default);

let api = _express.default.Router();

api.use((0, _expressSession.default)({
  store: process.env.NODE_ENV === 'production' ? new RedisStore({
    url: process.env.REDISCLOUD_URL
  }) : new RedisStore({
    url: 'redis://127.0.0.1:6379'
  }),
  secret: 'fdsjJKJusfKsf451s',
  resave: true,
  saveUninitialized: true
}));
/* routes to specified api's e.g all product
requests pass through the product api */

api.use('/product', _product.default);
api.use('/order', _order.default);
api.use('/common', _common.default);
api.use('/negotiate', _negotiation.default);
api.use('/user', _user.default);
api.use('/shop', _shop.default);
api.use('/home', _home.default);
api.route('/file/:name').get((req, res) => {
  var file = _fs.default.readFileSync('assets/images/' + req.params.name);

  res.send(file);
});
var _default = api;
exports.default = _default;