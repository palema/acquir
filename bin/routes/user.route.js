"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _user = _interopRequireDefault(require("../models/user.model"));

var _authentication = require("./authentication.route");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let user_api = _express.default.Router(); //user model is used for setting up operations with the database


user_api.route('/findByUsername/:username').get((req, res) => {});

const find_by_username = function (req, res, next) {
  _user.default.findOne({
    username: req.params.username //,

  }).exec((err, user) => {
    if (err) console.log(err);else return next();
  });
}; //routes to user resources


user_api.route('/sign-up/').post(find_by_username, _authentication.save_user, _authentication.set_session);
user_api.route('/sign-in/').post(_authentication.find_by_email, _authentication.set_session);
user_api.route('/verify-login/').post(_authentication.check_session);
user_api.route('/sign-out').get(_authentication.destroy_session); // user_api.route('/info/get').get((req, res) => {
// 	if (req.session.email) {
// 		User.findOne({ email: req.session.email }, { pasword: false, _id: false }, (err, doc) => {
// 			if (err) return res.send(err)
// 			if (doc) return res.send(doc)
// 		})
// 	} else {
// 		res.send({})
// 	}
// })

var _default = user_api;
exports.default = _default;