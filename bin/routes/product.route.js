"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.insert = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _product = require("../models/product.model");

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var product_api = _express.default.Router();

const not_found = {
  status: false,
  message: 'not matches found'
};
product_api.get('/findAll', (req, res) => {
  _product.Product.find((err, products) => {
    if (err) console.log(err);else console.log("request processed");
    res.json(products);
  });
});
product_api.route('/findBySku/:sku').get((req, res) => {
  _product.Product.schema.statics.findBySku(req.params.sku, (err, products) => {
    if (err) console.log(err);else res.json(products);
  });
});
product_api.route('/findByDepartment/:department').get((req, res) => {
  _product.Product.schema.statics.findByDepartment(req.params.department, (err, product) => {
    if (err) res.send(err.msg);
    if (product) res.send(product);else res.send(not_found);
  });
});

const insert = (req, res, next) => {
  var errors = [];
  var data = req.body;
  const product = new _product.Product({
    title: data.title,
    description: data.description,
    list_price: data.list_price,
    quantity: data.quantity,
    display_image: data.display_image.name,
    specs: data.specs
  });
  var sku = generate_sku(data.title, data.department[0].name, data.list_price);
  product.sku = sku;
  data.department.forEach(department => {
    product.department.push(department.name);
  }); //saves main image on server

  var dImg = data.display_image.file.replace(/^data:image\/\w+base64,/, "");
  var buf = new Buffer(dImg, 'base64');

  _fs.default.writeFile('./src/assets/' + data.display_image.name, buf, err => {
    if (err) {
      errors.push(err);
    }
  }); //saves angle images on server


  data.angle_image.forEach(img => {
    var temp_img = img.file.replace(/^data:image\/\w+base64,/, "");
    var buf = new Buffer(temp_img, 'base64');
    product.angle_images.push(img.name);

    _fs.default.writeFile('./src/assets/' + img.name, buf, err => {
      if (err) {
        errors.push(err);
      }
    });
  });

  if (errors.length === 0) {
    product.save((err, product) => {
      if (err) res.send({
        status: false,
        message: err.code
      });
      if (product) res.send({
        status: true,
        message: "uploaded"
      });
    });
  }
};

exports.insert = insert;
var _default = product_api;
exports.default = _default;

function generate_sku(title, department, list_price) {
  var sku = title.substring(0, 3);
  sku = sku + department.substr(0, 3);
  sku = sku + list_price;
  return sku;
} // api.route('/shop').get((req, res) => {
// 	if (req.session.shop) {
// 		Product.schema.statics.findByShop(req.session.shop, (err, docs) => {
// 			if (err) res.send(new Error('An error occured : ' + err))
// 			if (docs) res.send(docs)
// 		})
// 	}
// })


product_api.route('/findByShop/:shop').get((req, res) => {
  _product.Product.schema.statics.findByShop(req.params.shop, (err, docs) => {
    if (err) res.send(new Error('An error occured : ' + err));
    if (docs) res.send(docs);
  });
});
product_api.route('/delete/:sku').delete((req, res) => {
  _product.Product.findOneAndDelete({
    sku: req.params.sku
  }, (err, doc) => {});
});

const get_department_products = (req, res, next) => {
  _product.Product.aggregate([{
    $match: {
      department: req.params.department
    }
  }, {
    $limit: 50
  }, {
    $sort: {
      title: 1
    }
  }, {
    $project: {
      _id: 0
    }
  }], (err, docs) => {
    if (err) return next({
      status: false,
      message: 'internal server error'
    });
    if (!docs) return next(not_found);else res.send(docs);
  });
};

const get_recommended_products = (req, res, next) => {//based on data from user cookie
};

const get_related_products = (req, res, next) => {
  let base_product = req.body;

  _product.Product.aggregate([{
    $match: {
      department: {
        $in: base_product.departments
      }
    }
  }, {
    $limit: 10
  }, {
    $sort: {
      title: 1
    }
  }, {
    $project: {
      _id: 0
    }
  }], (err, docs) => {
    if (err) return next({
      status: false,
      message: 'internal server error'
    });
    if (!docs) return next(not_found);else res.send(docs);
  });
};

const get_trending_products = (req, res, next) => {
  _product.Product.aggregate([{
    $sort: {
      views: -1
    }
  }, {
    $limit: 10
  }, {
    $project: {
      _id: 0
    }
  }], (err, docs) => {
    if (err) return next({
      status: false,
      message: 'internal server error'
    });
    if (!docs) return next(not_found);else res.send(docs);
  });
};

const get_retailers_products = (req, res, next) => {};