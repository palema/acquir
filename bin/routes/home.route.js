"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _product = _interopRequireDefault(require("../models/product.model"));

var _redis = _interopRequireDefault(require("redis"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* home products should be based on :
    -user activity
    -product status
    -product recency

    steps : 
    -check cache or cookies
    -find all related products
    -sort products
    -send
*/
const home_api = _express.default.Router();

home_api.get('/findTrending', (req, res) => {
  _product.default.aggregate([{
    $sort: {
      'purchases': -1,
      'views': -1,
      'cart_listings': -1,
      'rating': -1
    }
  }, {
    $limit: 10
  }], (err, docs) => {
    if (err) return res.send({
      message: "could not fetch data"
    });
    return res.send(docs);
  });
});
home_api.get('/findWanted', (req, res) => {
  _product.default.aggregate([{
    $sort: {
      'offers_made': -1,
      'cart_listings': -1
    }
  }, {
    $limit: 10
  }], (err, docs) => {
    if (err) return res.send({
      message: "could not fetch data"
    });
    return res.send(docs);
  });
});
home_api.get('/findRecommended', (req, res) => {
  var redis_connection = _redis.default.createClient();

  redis_connection.on('connect', () => {
    console.log('connected to redis');
    redis_connection.get('user', (err, obj) => {
      console.log(obj);
    });
    redis_connection.end(true);
  });

  _product.default.aggregate([{
    $sort: {
      'offers_made': -1,
      'cart_listings': -1
    }
  }, {
    $limit: 10
  }], (err, docs) => {
    if (err) return res.send({
      message: "could not fetch data"
    });
    return res.send(docs);
  });
});
home_api.get('/getDeals');
home_api.get('/findMen', (req, res) => {
  _product.default.schema.statics.findByDepartment("men", (err, product) => {
    if (err) res.send(err.msg);
    if (product) res.send(product);else res.send({
      message: "not found"
    });
  });
});
var _default = home_api;
exports.default = _default;