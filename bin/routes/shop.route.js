"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _shop = _interopRequireDefault(require("../models/shop.model"));

var _order = _interopRequireDefault(require("../models/order.model"));

var _negotiation = _interopRequireDefault(require("../models/negotiation.model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let shop_api = _express.default.Router();

shop_api.route('/insert').post((req, res) => {
  var r = req.body;
  const shop = new _shop.default({
    name: r.name,
    catalogue: r.catalogue
  });
  shop.save((err, doc) => {
    res.send(doc);
  });
});
shop_api.route('/findBid').get((req, res) => {
  if (!req.session.email) {
    _negotiation.default.findOne({}, (err, docs) => {
      if (err) return res.status(400);
      if (docs) return res.send(docs);
    });
  } else {
    return res.status(401);
  }
});
shop_api.route('/findShop').get((req, res) => {
  _shop.default.find((err, docs) => {
    if (err) res.send(new Error('An err occured ' + err));
    if (docs) res.send(docs);
  });
});
shop_api.route('/findOrders').get((req, res) => {
  if (req.session.shop) {
    _order.default.aggregate([{
      $match: {
        participants: req.session.shop
      }
    }, {
      $project: {
        order_items: {
          $filter: {
            input: "$order_items",
            as: "item",
            cond: {
              $eq: ["$$item.shop", req.session.shop]
            }
          }
        },
        order_number: true,
        _id: false,
        billing_address: true,
        status: true
      }
    }], (err, docs) => {
      if (docs) return res.status(200).send(docs);
    });
  }
});
var _default = shop_api;
exports.default = _default;