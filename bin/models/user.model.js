"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.User = exports.user_schema = void 0;

var _mongoose = require("mongoose");

var _bcrypt = _interopRequireDefault(require("bcrypt"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const user_schema = new _mongoose.Schema({
  username: {
    type: String,
    unique: true,
    lowercase: true,
    required: true
  },
  email: {
    type: String,
    lowercase: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  balance: {
    type: Number,
    min: 0
  },
  shop: {
    type: _mongoose.Schema.Types.String,
    ref: 'Shop'
  }
});
exports.user_schema = user_schema;
user_schema.pre('save', function (next) {
  if (this.get('password') && this.isModified('password')) {
    this.set('password', _bcrypt.default.hashSync(this.get('password'), 10));
  }

  next();
});
const User = (0, _mongoose.model)('User', user_schema);
exports.User = User;
var _default = User;
exports.default = _default;