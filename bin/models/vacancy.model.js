"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Vacancy = new _mongoose.default.Schema({
  firm: String,
  job_description: String,
  job_title: String,
  offered_salary: Number,
  requirements: [],
  start_date: Date,
  end_date: Date
});

var _default = _mongoose.default.model('Vacancy', Vacancy);

exports.default = _default;