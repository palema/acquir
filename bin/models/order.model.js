"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Order = exports.order_schema = void 0;

var _mongoose = require("mongoose");

const order_schema = new _mongoose.Schema({
  user: {
    type: _mongoose.Schema.Types.String,
    ref: 'User',
    required: true
  },
  number: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  products: {
    type: Array,
    items: {
      sku: String,
      quantity: Number
    },
    maxItems: 100
  },
  shop: {
    type: String,
    ref: 'Shop'
  },
  billing_address: {
    city: String,
    street: String,
    erf: Number
  },
  status: {
    type: String,
    enum: ['pending', 'fulfilled', 'enroute', 'cancelled'],
    default: 'pending'
  }
});
exports.order_schema = order_schema;
const Order = (0, _mongoose.model)('Order', order_schema);
exports.Order = Order;
var _default = Order;
exports.default = _default;