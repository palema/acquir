"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Department = new _mongoose.default.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  keywords: [String],
  parent: {
    type: Array,
    items: {
      ref: '#Department'
    }
  },
  children: {
    type: Array,
    items: {
      ref: '#Department'
    }
  },
  features: {
    type: Array,
    items: {
      name: {
        type: String,
        required: true
      },
      units: [String]
    }
  }
});

var _default = _mongoose.default.model('Department', Department);

exports.default = _default;