"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Shop = exports.shop_schema = void 0;

var _mongoose = require("mongoose");

const shop_schema = new _mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  catalogue: [String],
  location: {
    city: String,
    street: String,
    erf: String
  },
  products: [{
    type: _mongoose.Schema.Types.String,
    ref: 'Product'
  }],
  manager: {
    type: _mongoose.Schema.Types.String,
    ref: 'User'
  }
});
exports.shop_schema = shop_schema;
const Shop = (0, _mongoose.model)('Shop', shop_schema);
exports.Shop = Shop;
var _default = Shop;
exports.default = _default;