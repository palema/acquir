"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Collection = new _mongoose.default.Schema({
  owner: {
    type: _mongoose.default.Schema.Types.ObjectId,
    ref: 'User'
  },
  collection: {
    type: Array,
    items: {
      sku: String,
      quantity: Number,
      unit_price: Number,
      total: Number
    },
    maxItems: 100
  },
  collection_expiry: Date,
  total_cost: Number
});

var _default = _mongoose.default.model('Collection', Collection);

exports.default = _default;