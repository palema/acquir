"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Product = exports.product_schema = void 0;

var _mongoose = require("mongoose");

const product_schema = new _mongoose.Schema({
  sku: {
    type: String,
    unique: true,
    required: true
  },
  title: {
    type: String,
    unique: true,
    required: true
  },
  list_price: {
    type: Number,
    required: true
  },
  quantity: {
    type: Number,
    min: 0,
    required: true
  },
  department: [String],
  description: String,
  display_image: {
    type: String,
    required: true
  },
  angle_images: [String],
  shop: {
    type: _mongoose.Schema.Types.String,
    ref: 'Shop',
    required: true
  },
  rating: {
    type: Number,
    min: 1,
    max: 5
  },
  purchases: {
    type: Number,
    min: 0
  },
  views: {
    type: Number,
    min: 0
  },
  cart_listings: {
    type: Number,
    min: 0
  },
  offers_made: {
    type: Number,
    min: 0
  },
  negotiable: Boolean,
  specs: {
    type: Array,
    items: {
      name: String,
      value: String
    }
  }
});
exports.product_schema = product_schema;

product_schema.statics.findByDepartment = function (dep, callback) {
  return (0, _mongoose.model)('Product').find({
    department: dep
  }, callback);
};

product_schema.statics.findBySku = function (sku, callback) {
  return (0, _mongoose.model)('Product').find({
    sku: sku
  }, callback);
};

product_schema.statics.findByShop = function (retailer, callback) {
  return Product.find({
    retailer: retailer
  }, callback);
};

const Product = (0, _mongoose.model)('Product', product_schema);
exports.Product = Product;
var _default = Product;
exports.default = _default;