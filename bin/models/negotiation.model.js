"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Negotiation = exports.negotiation_schema = void 0;

var _mongoose = require("mongoose");

const negotiation_schema = new _mongoose.Schema({
  user: {
    type: String,
    ref: 'User',
    unique: true
  },
  products: {
    type: Array,
    items: {
      type: {
        sku: {
          type: String,
          ref: 'Product'
        },
        quantity: {
          type: Number,
          min: 1
        }
      }
    }
  },
  offer: Number,
  status: {
    type: String,
    enum: ['rejected', 'accepted', 'ongoing', 'pulledout'],
    default: 'ongoing'
  },
  shop: {
    type: String,
    ref: 'Shop'
  }
});
exports.negotiation_schema = negotiation_schema;
const Negotiation = (0, _mongoose.model)('Negotiation', negotiation_schema);
exports.Negotiation = Negotiation;
var _default = Negotiation;
exports.default = _default;