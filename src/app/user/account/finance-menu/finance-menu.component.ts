import { Component, OnInit, Input, NgModule } from '@angular/core';

@Component({
  selector: 'finance-menu',
  templateUrl: './finance-menu.component.html',
  styleUrls: ['./finance-menu.component.css']
})

@NgModule()
export class FinanceMenuComponent implements OnInit {

  @Input() show : boolean
  constructor() { }

  ngOnInit() {
  }

}
