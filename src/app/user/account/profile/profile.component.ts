import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private user_info

  constructor( private http : HttpClient) { }

  ngOnInit() {
    this.fetch_user_data()
  }

  private fetch_user_data(){
    this.http.get('http://localhost:4000/user/info/get', {withCredentials : true}).subscribe((rs) => {
      this.user_info = rs

    })
  }

  get username () {
    return this.user_info ? this.user_info.username : ''
  }

  
  get lastname () {
    return this.user_info ? this.user_info.lastname : ''
  }

  
  get firstname () {
    return this.user_info ? this.user_info.firstname : ''
  }

  
  get email () {
    return this.user_info ? this.user_info.email : ''
  }

  
  get age () {
    return this.user_info ? this.user_info.age : ''
  }

  
  get height () {
    return this.user_info ? this.user_info.height : ''
  }

  
  get gender () {
    return this.user_info ? this.user_info.gender : ''
  }


}
