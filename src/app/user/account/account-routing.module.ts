import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { FinanceMenuComponent } from './finance-menu/finance-menu.component';

const routes: Routes = [
  { path : '', children : [
      { path : 'profile', component : ProfileComponent },
      { path : 'settings', component : SettingsComponent },
      { path : 'finance', component : FinanceMenuComponent },
      { path : '', redirectTo : 'profile', pathMatch : 'full' }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
