import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment'


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private uri = environment.dbUrl;
  public list: BehaviorSubject<any> = new BehaviorSubject(null)
  public view: BehaviorSubject<any> = new BehaviorSubject(null)


  public product_for_view: BehaviorSubject<any> = new BehaviorSubject(null)

  constructor(private http: HttpClient) {
  }

  /**
   * 
   * @param str sku of wanted product
   */
  public get_product_for_view(str?: string) {
    if (str) {
      localStorage.setItem('LViewed', str)
    }
    var sku = str || localStorage.getItem('LViewed')
    return this.http.get(`${this.uri}/product/findBySku/${sku}`).subscribe((docs) => {
      this.product_for_view.next(docs[0])
    });
  }


  public getProduct(){
    return this.http.get(`${this.uri}/product/all`);
  }

  public getProductBySku(sku) {
    return this.http.get(`${this.uri}/product/sku/${sku}`);
  }
  //for departments
  set_page_variables(name: string) {

    if (name) {
      localStorage.setItem('listItems', name)
    }

    var name = localStorage.getItem('listItems')

    this.http.get(`${this.uri}/${name}`).subscribe((data) => {
 
      return
    })
    if(name == "shop"){
      this.http.get(`${this.uri}/shop/findShop`).subscribe((data) => {
        this.list.next({ heading: name, data: data })        
        return
      })
    }else if (name == "department"){
      this.http.get(`${this.uri}/common/findDepartment`).subscribe((data) => {
        this.list.next({ heading: name, data: data })        
        return
      })
    }
  }

  view_variables(name : string, field : string){

    if (name) {
      localStorage.setItem('viewItems', name)
      localStorage.setItem('viewField', field)
    }

    var name = localStorage.getItem('viewItems')
    var field = localStorage.getItem('viewField')

    if(field == "department"){
      this.http.get(`${this.uri}/product/findByDepartment/${name}`).subscribe((data) => {
        this.view.next({ heading: name, data: data })
        return
      })
    }else if (field == "shop"){
      this.http.get(`${this.uri}/product/findByShop/${name}`).subscribe((data) => {
        this.view.next({ heading: name, data: data })
        return
      })
    }

  }

  getProductByDepartment(department : String) {
    return this.http.get(`${this.uri}/product/findByDepartment/${department}`)
  }

  getProductByTopic(topic : String){
    return this.http.get(`${this.uri}/home/find${topic}`)
  }


}

interface Status {
  status: boolean,
}