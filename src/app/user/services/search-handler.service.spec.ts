import { TestBed } from '@angular/core/testing';

import { SearchHandlerService } from './search-handler.service';

describe('SearchHandlingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchHandlerService = TestBed.get(SearchHandlerService);
    expect(service).toBeTruthy();
  });
});
