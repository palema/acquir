import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SearchHandlerService {

  private uri = environment.dbUrl;

  constructor(private http : HttpClient) { }
  private results : Observable<any>
  
  get_results(): Observable<any>{
    return this.results
  }

  set_query(search_term? : String){
    var query = search_term || localStorage.getItem('last_search_term')
    this.results = this.http.get(`${this.uri}/common/search/${query}`) 
  }

}
