import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment'
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private uri = environment.dbUrl;
  public order_form: FormGroup; //dynamic form for orders
  public negotiation_item:
    BehaviorSubject<{ sku: String, price: String, shop: String }> =
    new BehaviorSubject(null);

  constructor(private http: HttpClient, private fb: FormBuilder, private apollo: Apollo) {
    const address = this.fb.group({
      'city': [null, Validators.required],
      'street': [null, Validators.required],
      'erf': [null, Validators.required]
    });

    this.order_form = this.fb.group({
      'items': this.fb.array([]),
      'billing_address': address,
      'order_number': [null, Validators.compose([])],
      'password': [null, Validators.compose([Validators.minLength(8)])]
    });
  }

  quickOrderMutation = gql`
      mutation QuickOrder($sku:String, $quantity: Int, $shop:String){
        quickOrder(sku:$sku, quantity:$quantity, shop:$shop)
      }
    `
  cartOrderMutation = gql`
     mutation CartOrder($sku:String!, $quantity: Int!, $shop:String!){
       cartOrder(sku:$items, quantity:$quantity, shop:$shop)
     }
   `
   quickBidMutation = gql`
      mutation QuickBid($sku: String, $quantity: Int, $shop:String, $offer: Float){
        quickBid(sku: $sku, quantity: $quantity, shop: $shop, offer: $offer)
      }
   `

    addToCartMutation = gql`
      mutation AddToCart($sku: String!, $quantity: Int!){
        addToCart(sku: $sku, quantity: $quantity)
      }
    `

  //   cancelOrderMutaion = gql`
  //     mutation orderProduct($orderNumber:String){

  //     }
  //   `
  //   addToCartMutation = gql`
  //   mutation orderProduct($sku:String!){

  //   }
  // `

  //   removeFromCartMutaion = gql`
  // mutation orderProduct($sku:String){

  // }
  // `

  //   removeFromOrderMutation = gql`
  // mutation orderProduct($sku:String, $orderNumber:String){

  // }
  // `

  //   deleteCartMutation = gql`
  // mutation orderProduct(){

  // }
  // `

  //   addToOrderMutation = gql`
  // mutation orderProduct($sku:String){

  // }
  // `

  // deleteFromOrderMutation = gql`
  // mutation orderProduct($sku:String){

  // }
  // `


  quickOrderHandler(sku:string, shop: string, quantity: number) {
    return this.apollo.mutate({
      mutation: this.quickOrderMutation,
      variables: {
        sku,
        quantity,
        shop
      }
    })
  }

  cartOrderHandler(cart) {
    return this.apollo.mutate({
      mutation: this.cartOrderMutation,
      variables: {
        cart
      }
    })
  }

  quickBidHandler(sku:string, quantity: number, shop:string, offer:number ){
    return this.apollo.mutate({
      mutation:this.quickBidMutation,
      variables: {
        shop,
        sku,
        quantity,
        offer
      }
    })
  }

  addToCartHandler(sku:string, quantity:number){
    return this.apollo.mutate({
      mutation: this.addToCartMutation,
      variables: {
        sku,
        quantity
      }
    })
  }









  set_negotiation_item(sku, price, shop) {
    this.negotiation_item.next({ sku: sku, price: price, shop: shop })
  }

  /** manages order process */
  public order_processor(form) {
    var order = form.value
    return this.http.post(`${this.uri}/order`, order, { withCredentials: true })
  }

  get order_number() {
    return this.order_form.get('order_number')
  }


  get city() {
    return this.order_form.get('billing_address').get('city')
  }

  get street() {
    return this.order_form.get('billing_address').get('street')
  }

  get erf() {
    return this.order_form.get('billing_address').get('erf')
  }

  public change_order_number(order_no) {
    this.order_number.setValue(order_no)
  }

  public change_order_address(address) {
    this.city.setValue(address.city)
    this.street.setValue(address.street)
    this.erf.setValue(address.erf)
  }

  get item_forms() {
    return this.order_form.get('items') as FormArray
  }

  add_item(sku, title, price) {
    let add = true

    //filters out duplicates
    this.order_form.value.items.forEach(item => {
      if (title == item.title) {
        add = false
        return
      }
    });



    //adding items to order form
    if (add) {
      const items = this.fb.group({
        'title': [title, Validators.required],
        'quantity': [null, Validators.compose([Validators.required, Validators.min(1)])],
        'price': [price],
        'sku': [sku]
      });

      this.item_forms.push(items)
    }

  }

  public clear_form() {
    for (var i = 0; i < this.item_forms.length; i++) {
      this.item_forms.removeAt(i)
    }

    this.order_form.reset()
  }


}
