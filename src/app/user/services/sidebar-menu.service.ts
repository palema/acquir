import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarMenuService {

  private sidebar = false;
  private active_window = 0
  private windows: Windows[] = [
    { name: 'shopping_cart', state: true },
    { name: 'user_menu', state: false }
  ]

  constructor() { }

  public toggle_sidebar(window) {
    //if sidebar is already open and active & desired windows same hide
    if (this.sidebar && window == this.active_window) {
      this.sidebar = false
    } else {
      this.open_sidebar(window)

    }

  }

  public open_sidebar(window) {
    this.active_window = window
    this.toggle_windows(window)
    this.sidebar = true //show sidebar
  }

  public toggle_windows(index) {
    this.windows.forEach(pane => {
      pane.state = false
    })
    this.windows[index].state = true
  }

  get sidebar_state() {
    return this.sidebar
  }

  public window_state(index) {
    return this.windows[index].state
  }
}

interface Windows {
  name: string, state: boolean
} 
