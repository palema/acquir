import { Component, OnInit, Output, Input } from '@angular/core';

import { ProductService } from '../../services/product.service';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  @Input() title

  constructor(private product_service: ProductService) { }

  public products
  private items_count
  private item_width
  private slider_width
  private total_width_of_items
  private initial_position = 0
  private current_position = `${this.initial_position}px`
  private items_slider_difference = 0

  public show_right_slider: boolean = false

  ngOnInit() {
    if (document.querySelector('#item_img')) {
      this.item_width = document.querySelector('#item_img').clientWidth
    }

    setTimeout(()=>{
      this.adjust_variables()
    }, 500)

    if ('ontouchstart' in document) {
      var start_pos
      var docs = document.querySelectorAll('#slide-inner')
      docs.forEach(element => {
        element.addEventListener("touchstart", function(this : Element, e: TouchEvent){
          var touchObj = e.changedTouches[0]
          start_pos = touchObj.clientX
        }, false)
      });

      console.log('touch')
      window.addEventListener('touchmove', (e) => {
        var touchObj = e.changedTouches[0];
        var move_dist = touchObj.clientX - start_pos;

        if (move_dist > 0) {
          this.slide_left()
        } else if (move_dist < 0) {
          this.slide_right()
        }

      }, false)
      window.addEventListener('touchend', function (e) { })
    }

  }

  slide_left() {
    if (this.initial_position < 0) {
      this.initial_position = this.initial_position + this.item_width;
      this.current_position = `${this.initial_position}px`;
      this.items_slider_difference -= this.item_width

      this.show_right_slider = true
    }

    if (this.initial_position > 0) {//resets position to zero
      this.initial_position = 0;
      this.current_position = `${0}px`
      this.items_slider_difference = this.slider_width - this.total_width_of_items

      this.show_right_slider = true
    }

  }

  slide_right() {
    if ((-this.items_slider_difference) > 0) {
      if (-this.items_slider_difference < this.item_width) { //if gap is small only move by gap size
        this.initial_position = this.initial_position + this.items_slider_difference; //plus because diff is negative
        this.current_position = `${this.initial_position}px`;
        this.items_slider_difference += -this.items_slider_difference
      } else {
        this.initial_position = this.initial_position - this.item_width;
        this.current_position = `${this.initial_position}px`;
        this.items_slider_difference += this.item_width
      }

      if (this.items_slider_difference >= 0) {
        this.show_right_slider = false
      }

    }

  }

  set_variables(event) {
    var w = event * this.items_count
    this.item_width = event
    this.total_width_of_items = w
  }

  //updates product position variables
  adjust_variables() {
    var new_slide_width = document.querySelector('.products-slide').clientWidth
    this.item_width = document.querySelector('#item_img').clientWidth

    this.set_variables(this.item_width)

    this.slider_width = new_slide_width
    this.items_slider_difference = this.slider_width - this.total_width_of_items

    if (this.items_slider_difference < 0) {//checks if right slide icon should show
      this.show_right_slider = true
    }

    if (this.items_slider_difference >= 0) {//adjusts position of items when resizing
      this.initial_position = 0;
      this.current_position = this.initial_position + 'px'
    }

  }


}
