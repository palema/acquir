import { Component, OnInit, Input } from '@angular/core';
import { SearchHandlerService } from '../../services/search-handler.service';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { environment } from 'src/environments/environment'

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {

  public dbUrl = environment.dbUrl
  
  constructor(private search : SearchHandlerService, private router : Router, private product_service : ProductService) { 

  }

  ngOnInit() {
    this.search.set_query()
  }
  
  public search_results(){
     return this.search.get_results()
  }

  public view_item(sku){
    this.product_service.get_product_for_view(sku)
    this.router.navigate(['/user/product'])
  }
}

