import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-departments',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public data$ : Observable<any>

  constructor( private product_service : ProductService, private router : Router) { }

  ngOnInit() {
    this.product_service.set_page_variables(null)
    this.data$ =this.product_service.list

  }

  public visit(name : string, field : string){
    this.product_service.view_variables(name, field)
    this.router.navigate(['/user/view'])
  }
}
