import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  public data$ : Observable<any>

  constructor( private product_service : ProductService) { }

  ngOnInit() {
    this.product_service.view_variables(null, null)
    this.data$ =this.product_service.view
  }

}
