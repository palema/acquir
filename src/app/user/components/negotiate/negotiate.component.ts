import { Component, OnInit, NgModule } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TransactionService } from 'src/app/user/services/transaction.service';
import { environment } from 'src/environments/environment'

@Component({
  selector: 'app-negotiate',
  templateUrl: './negotiate.component.html',
  styleUrls: ['./negotiate.component.css']
})
@NgModule()
export class NegotiateComponent implements OnInit {
  public negotiate: FormGroup
  public negotiation_item$
  public prior_negotiations
  public form_result
  public show_form : boolean

  public dbUrl = environment.dbUrl
  
  constructor(private fb: FormBuilder, private http: HttpClient, private transact: TransactionService) { }

  ngOnInit() {
    this.negotiate = this.fb.group({
      sku: [null, Validators.compose([Validators.required])],
      offer: [null, Validators.compose([Validators.required])],
      quantity: [null, Validators.compose([Validators.required])],
      shop: [null, Validators.compose([Validators.required])],
      type: ['new']
    })

    this.transact.negotiation_item.subscribe((res) => {
      this.negotiation_item$ = res
    })

    this.fetch_prior_negotiations()
  }


  get sku() {
    return this.negotiate.get('sku')
  }

  get offer() {
    return this.negotiate.get('offer')
  }

  get quantity() {
    return this.negotiate.get('quantity')
  }

  get shop() {
    return this.negotiate.get('shop')
  }

  get type() {
    return this.negotiate.get('type')
  }


  public form_handler() {

    if (this.negotiate.value.type === 'new') {
      this.sku.setValue(this.negotiation_item$.sku)
      this.shop.setValue(this.negotiation_item$.shop)
      if (this.negotiate.valid) {

        this.http.post(this.dbUrl+'/negotiate/new', this.negotiate.value, { withCredentials: true })
        .subscribe((response : Server_respone) => {
          response.message = 'set'
          this.form_result = response;

          if(response.status){
            this.negotiate.reset()
            setTimeout(()=>{
              
              this.form_result = null
            }, 4000)
            
          }

        })
      }
    }else if(this.negotiate.value.type === 'counter'){
      if (this.negotiate.valid) {

        this.http.post(this.dbUrl+'/negotiate/b/update', this.negotiate.value, { withCredentials: true })
        .subscribe((response : Server_respone) => {
          response.message = 'updated'
          this.form_result = response;
          
          if(response.status){
            this.negotiate.reset()
            setTimeout(()=>{
              
              this.form_result = null
            }, 4000)
            
          }

        })
      }

    }
  }

  public fetch_prior_negotiations() {
    this.http.get(this.dbUrl+'/negotiate/get', { withCredentials: true }).subscribe((rs) => {
      if (rs) {
        this.prior_negotiations = rs
      }
    })
  }

  public change_negotiation(parent, sku: String, shop: String, quantity: Number, offer: Number) {
    this.negotiate.reset()
    this.show_form = true
    this.sku.setValue(sku)
    this.shop.setValue(shop)
    this.quantity.setValue(quantity)
    this.offer.setValue(offer)
    this.type.setValue('counter')

    var form = document.getElementById('negotiate-outer-wrapper')
    document.getElementById(parent).appendChild(form)

  }
}

interface Server_respone {status : boolean, message : String}