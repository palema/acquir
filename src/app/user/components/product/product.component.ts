import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment'
import { TransactionService } from '../../services/transaction.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() product
  @Input() position
  @Input() alter_width

  @Output() width = new EventEmitter()

  public imgUrl = environment.imgUrl
  private _click_count = 0
  public show_form : boolean = false

  constructor(
    private product_service: ProductService,
    private router: Router,
    private transact: TransactionService
  ) { }

  ngOnInit() {
  }



  view_product(sku) {
    ++this._click_count

    setTimeout(()=>{
      if(this._click_count == 1){
        this.product_service.get_product_for_view(sku)
        this.router.navigate(['user/product'])
      }else if(this._click_count == 2){
        this.transact.addToCartHandler('mint', 1)
        .subscribe(({data})=>{
          console.log(data)
        }, error=>{
          console.log(error)
        })
      }else if(this._click_count > 2){
        this.show_form = true
      }
    }, 1000)
  }

  orderHandler(form) {
    if (form.valid) {
      this.transact.quickOrderHandler('22', 'trainwreck', form.value.quantity)
        .subscribe(({ data }) => {
          console.log(data)
        },
          error => {
            console.log(error)
          })
    }
  }

  bidHandler(form:NgForm){
    if(form.valid){
      this.transact.quickBidHandler('mint', form.value.quantity, 'trainwreck', form.value.offer)
      .subscribe(({data})=>{
        console.log(data)
      }, error =>{
        console.log(error)
      })
    }
  }

}
