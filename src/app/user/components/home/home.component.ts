import { Component, OnInit } from '@angular/core'
import { Apollo } from 'apollo-angular'
import gql from 'graphql-tag'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'


@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {
  constructor(private apollo: Apollo) { }
  topics: Observable<any>
  ngOnInit() {
    console.log("before home")
    this.topics = this.apollo.watchQuery<any>({
      query: gql`
          query homeTopics($screenWidth:Int, $screenHeight:Int, $scroll: Int) {
            home(screenWidth:$screenWidth, screenHeight:$screenHeight, scroll: $scroll){
              topic
              products{
                title
                display_image
                list_price
              }
            }
          }
        `,
        variables: {
          screenWidth: 1,
          screenHeight: 1,
          scroll: 1
        }
        
    })
      .valueChanges
      .pipe(map(({data})=>{
        console.log(data, ' from home query')
        return data.home
      }))
  }
}