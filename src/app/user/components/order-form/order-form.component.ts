import { Component, OnInit, NgModule } from '@angular/core';
import { FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { TransactionService } from 'src/app/user/services/transaction.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css']
})
@NgModule()
export class OrderFormComponent implements OnInit {

  public order_form: FormGroup = this.transact.order_form
  public order_feedback : Order_status = {status: false, message: '', set: false}
  public total = 0
  public orders
  private items_state: boolean
  private panel_state: boolean


  constructor(private fb : FormBuilder, private transact : TransactionService, private http : HttpClient) { }


  ngOnInit() {
    this.order_form = this.transact.order_form   
    this.fetch_orders()

  }

  get item_forms(){
    return this.order_form.get('items') as FormArray
  }

  order_handler(){
    this.transact.order_processor(this.order_form).subscribe((res : Order_status) => {
      if(res.status === true){
        this.order_form.reset()
        this.total = 0
      }
      this.order_feedback.message = res.message
      this.order_feedback.status = res.status
      this.order_feedback.set = true

      setTimeout(()=>{
        this.order_feedback.set = false
      }, 2000)

    })

  }
  

  
  remove_item(i){
    this.total -= this.order_form.value.items[i].qty * this.order_form.value.items[i].price //decreaments total when item is removed
    this.transact.item_forms.removeAt(i)
  }

  
  calculate_total(){
    var total = 0;
    this.order_form.value.items.forEach( (value) => {
      total += value.quantity * value.price
    });

    this.total = total

  }

  get city(){
    return this.order_form.get('billing_address').get('city')
  }

  get street(){
    return this.order_form.get('billing_address').get('street')
  }

  get erf(){
    return this.order_form.get('billing_address').get('erf')
  }

  get order_number(){
    return this.order_form.get('order_number')
  }

  get password(){
    return this.order_form.get('password')
  }

  public activate_form(number, address){
    this.transact.clear_form()
    this.transact.change_order_number(number)
    this.transact.change_order_address(address)
  }

  
  fetch_orders(){
    this.http.get('http://localhost:4000/orders/get', {withCredentials : true})
    .subscribe((response) => {
      this.orders = response
    })
  }

  public reset_form(){
    this.transact.clear_form()
  }

  toggle_order_items(){
    this.items_state = !this.items_state
  }

  toggle_panel(){
    this.panel_state = !this.panel_state
  }

  move_form(id, order_no?, address?){

    var form = document.getElementById('order-form')
    document.getElementById(id).appendChild(form)

    if(order_no && address){
      this.activate_form(order_no, address)
      this.toggle_order_items()
    }

    this.reset_form()

  }

}


interface Order_status{
  status : boolean,
  message : string,
  set : boolean
}