import { Component, OnInit } from '@angular/core'
import { ProductService } from '../../services/product.service'
import { SidebarMenuService } from '../../services/sidebar-menu.service'
import { environment } from 'src/environments/environment'


@Component({
  selector: 'product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  public product$
  private _screen_size  = window.innerWidth;
  public dbUrl = environment.dbUrl

  constructor(private product_service : ProductService,
              private sidebar_service : SidebarMenuService) { }

  ngOnInit() {
    this.product_service.get_product_for_view()
    this.product$ = this.product_service.product_for_view
  }

  private item_height = 100
  private total_items_height = 100 * 9
  private box_height = 300
  private current_item_position = 0

  items = ["1111.jpg", "1111.jpg", "1111.jpg", "1111.jpg", "1111.jpg","1111.jpg"]

  slide_up(){
    if(this.current_item_position<0){
      this.current_item_position += this.item_height
    }
  }

  slide_down(){
    if((this.current_item_position*-1)<this.box_height){
      this.current_item_position -= this.item_height
    }
  }


  get screen_size(){
    return window.innerWidth
  }


}
