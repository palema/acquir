import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { RegistrationService } from 'src/app/core/registration/authentication.service';
import { SidebarMenuService } from '../../services/sidebar-menu.service';
import { SearchHandlerService } from '../../services/search-handler.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionService } from 'src/app/shared/action.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  
  constructor(private product_service: ProductService, private fb: FormBuilder,
    private auth: RegistrationService, private search: SearchHandlerService,
    private sidebar_service: SidebarMenuService, private router: Router, private action_service : ActionService) {


    this.search_form = this.fb.group({
      query: [null, Validators.compose([Validators.required, Validators.minLength(1)])]
    })
  }

  private _user_options_state = false

  public windows_list = [
    {
      name: "department", state: false, options: [
        { name: "accessories", keywords: ['accessory'] }, { name: "men's fashion", keywords: ['men'] }
      ]
    },
    {
      name: "shop", state: false, options: [
        { name: "markhams", keywords: ['markhams'] }, { name: "pick n pay", keywords: ['pick n pay'] }
      ]
    },
    // {
    //   name: "brand", state: false, options: [{ name: "nike", keywords: ['adidas'] }, { name: "adidas", keywords: ['adidas'] }]
    // }
  ]

  private _active_window = -1;
  public search_form: FormGroup
  public login_status_check$
  public last_search = localStorage.getItem('last_search_term')
  public show_search : boolean = false;
  public screen_width: number;


  ngOnInit() {

    this.login_status_check$ = false //this.auth.login_status
    document.addEventListener('click', (evt)=>{
      this._user_options_state = false 
    })

  }

  get query() {
    return this.search_form.get('query')
  }

  get user_options_state(){
    return this._user_options_state
  }

  toggle_user_options(event: Event){
    event.stopPropagation()
    this._user_options_state = !this.user_options_state
  }

  toggle_show_search(){
    this.show_search = !this.show_search
  }

  check_screen_width(){
    this.screen_width = document.querySelector('#navbar-wrapper').clientWidth

  }

  visible_class = "options-visible"

  public toggle_navigation_list(index, event: Event) {

    if (this.windows_list[index].state && event.type == "click") {
      this.windows_list[index].state = false
      this.active_window = -1
      return
    }

    this.windows_list.forEach(i => {
      i.state = false
    })
    this.windows_list[index].state = true;

    this._active_window = index
  }

  private hide_list() {
    this._active_window = -1
  }

  set active_window(index) {
    this._active_window = index
  }

  get active_window() {
    return this._active_window
  }

  public send_query() {
    if (this.search_form.valid) {
      this.search.set_query(this.search_form.value.query)
      localStorage.setItem('last_search_term', this.search_form.value.query)
      this.router.navigate(['user/search'])
    }
  }

  private visit_window(window, option) {
    this.product_service.set_page_variables(this.windows_list[window].name)

    this.router.navigate(['user/list'])
  }

  private logout_handler() {
    //this.auth.sign_out()
  }

  public toggle_sidebar(){
    this.action_service.toggle_sidebar()
  }


}

interface Status {
  status: boolean
}
