import { Component, OnInit, Input } from '@angular/core'
import { SidebarMenuService } from '../../services/sidebar-menu.service'
import { TransactionService } from '../../services/transaction.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-transact',
  templateUrl: './transact.component.html',
  styleUrls: ['./transact.component.css']
})
export class TransactComponent implements OnInit {

  @Input() product 

  constructor(private transact : TransactionService, 
    private sidebar_service : SidebarMenuService, private router : Router) { }

  ngOnInit() {
  }

  get_form(sku, title, price) {
    this.transact.add_item(sku, title, price)
    this.router.navigate(['/user/order'])
  }

  get_negotiation_form(sku, price, shop) {
    this.transact.set_negotiation_item(sku, price, shop)
    this.sidebar_service.open_sidebar(0)
    this.router.navigate(['/user/bid'])

  }

}
