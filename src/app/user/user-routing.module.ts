import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ListComponent } from './components/list/list.component';
import { ViewComponent } from './components/view/view.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { SearchComponent } from './components/search/search.component';
import { UserComponent } from './user.component';
import { OrderFormComponent } from './components/order-form/order-form.component';
import { NegotiateComponent } from './components/negotiate/negotiate.component';

const routes: Routes = [
  {
    path: '', component : UserComponent,  children: [
      { path: 'home', component: HomeComponent, data: { animation: 'isLeft'} },
      { path: 'list', component: ListComponent,  data: { animation: 'isRight'} },
      { path: 'view', component: ViewComponent },
      { path: 'product', component: ProductViewComponent },
      { path: 'search', component: SearchComponent },
      { path: 'order', component: OrderFormComponent},
      { path: 'bid', component: NegotiateComponent},
      { path: 'account', loadChildren: 'src/app/user/account/account.module#AccountModule' },
      { path: '', redirectTo: 'home', pathMatch: 'full' }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
