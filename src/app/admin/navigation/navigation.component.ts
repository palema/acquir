import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/shared/action.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class AdminNavigationComponent implements OnInit {

  constructor(private action_service : ActionService) { }

  ngOnInit() {
  }

  toggle_sidebar(){
    this.action_service.toggle_sidebar()
  }

}
