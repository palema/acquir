import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from './order/order.component';
import { OfferComponent } from './offer/offer.component';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent, children: [
      { path: 'orders', component: OrderComponent, data: { animation: 'isCenter'} },
      { path: 'offers', component: OfferComponent, data: { animation: 'isRight'} },
      { path: 'catalogue', loadChildren: './catalogue/catalogue.module#CatalogueModule', data: { animation: 'isLeft' }},
      { path: '', redirectTo: 'catalogue', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
