import { Component, OnInit } from '@angular/core'
import { environment } from 'src/environments/environment'
import { CommonService } from '../services/common.service'
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  constructor (private common : CommonService) {}
  
  private uri = environment.dbUrl
  private orders : Observable<any>

  ngOnInit() {
    this.orders = this.common.getShopOrders()
    .pipe(map(({data, loading})=>{
      return data.shopOffers
    }))
  }


}
