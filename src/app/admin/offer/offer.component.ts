import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'
import { CommonService } from '../services/common.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {

  constructor(private common: CommonService) { }

  private uri = environment.dbUrl
  public offers : Observable<any>

  ngOnInit() {
    this.offers = this.common.getShopOffers()
    .pipe(map(({data})=>{ return data.shopOffers }))
  }

}
