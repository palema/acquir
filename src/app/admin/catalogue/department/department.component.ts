import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  @Input('group') department_group: FormGroup

  constructor(private fb: FormBuilder, private common: CommonService) { }
  public dbDepartments: Observable<any>
  public departmentTree: DepartmentNode

  ngOnInit() {
    this.departmentTree = new DepartmentNode(this.common, null)
    this.getDepartment()
  }

  get department() {
    return <FormArray>this.department_group.controls['department']
  }

  add_department(name) {
    this.department.push(this.department_template(name))

    //get subdepartments
  }

  department_template(name) {
    return this.fb.group({
      name: [name, Validators.compose([])],
    })
  }

  remove_department(index: number) {
    this.department.removeAt(index)
  }

  getDepartment() {   
    this.dbDepartments = this.common.getRootDepartment()
      .pipe(map(({ data }) => {
        for(var i = 0; i< data.rootDepartment.length; i++){
          console.log(data.rootDepartment[i].name, 'list')
          this.departmentTree.addSubdepartment = new DepartmentNode(this.common, data.rootDepartment[i].name)
        }
        return data.rootDepartment
      }))
  }

}

//department data structure to hold users choice of product departments
class DepartmentNode {
  private subDepartments: [] = []
  private parent: String

  constructor( private common: CommonService, private departmentName: string){}

  get name() {
    return this.departmentName
  }

  get subDeps() {
    if (this.subDepartments == []) {
      this.common.getRootDepartment()
        .pipe(map(({ data }) => {
          return data.rootDepartment
        }))
    }
    else
      return this.subDepartments
  }

  set addSubdepartment(department: DepartmentNode) {
    //this.subDepartments.push("department")
  }
}
