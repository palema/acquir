import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { CatalogueComponent } from './catalogue.component';
import { ViewComponent } from './view/view.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { RemoveComponent } from './remove/remove.component';

const routes = [
    { path : '', component : CatalogueComponent, children : [
        { path : 'view', component : ViewComponent},
        { path : 'add', component : AddComponent},
        { path : 'edit', component : EditComponent},
        { path : 'remove', component : RemoveComponent},
        { path : '', redirectTo : 'view', pathMatch: 'full'},
    ]},

]


@NgModule({
    imports : [RouterModule.forChild(routes)],
    exports : [RouterModule]
})

export class CatalogueRoutingModule{}

