import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage'
import { CatalogueManagementService } from '../../services/catalogue-management.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public product_form: FormGroup
  public submit_result = { status: false, msg: null }

  constructor(private fb: FormBuilder, private storage: AngularFireStorage, private ctM: CatalogueManagementService ) { }

  ngOnInit() {
    this.product_form = this.fb.group({
      title: [null, Validators.compose([Validators.required, Validators.maxLength(50)])],
      description: [null, Validators.compose([Validators.required])],
      list_price: [null, Validators.compose([Validators.required])],
      department: this.fb.array([]),
      quantity: [null, Validators.compose([Validators.required, Validators.min(1)])],
      specs: this.fb.array([]),
      display_image: [null],
      angle_image: this.fb.array([])
    })
  }

  get display_image() {
    return this.product_form.get('display_image')
  }

  get title() {
    return this.product_form.get('title')
  }

  get description() {
    return this.product_form.get('description')
  }

  get list_price() {
    return this.product_form.get('list_price')
  }

  get quantity() {
    return this.product_form.get('quantity')
  }

  public submit_form() {
    

    var department = []
    for(var i=0; i<this.product_form.value.department.length; i++){
      department.push(this.product_form.value.department[i].name)
    }

    this.ctM.addHandler(this.product_form.value)
    .subscribe(({data})=>{
      console.log(data)
    },
    (error)=>{
      console.log(error)
    })

    // var filepath = this.storage.ref(this.display_image.value.name)
    // this.storage.upload(this.display_image.value.name, this.display_image.value.file).snapshotChanges().pipe(
    //   finalize(() => {
    //     filepath.getDownloadURL().subscribe((url) => {
    //       console.log(url)
    //     })
    //   })
    // ).subscribe()

  }


  //for main image
  file_upload(event) {
    var file = event.target.files[0]

    const reader = new FileReader()

    if (file) {
      reader.onload = ((dImg) => {
        return (e) => {
          this.display_image.setValue({
            name: file.name,
            src: e.target.result,
            file: file
          })
        }
      })(file)
      reader.readAsDataURL(file)
    }
  }

}


