import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngleImageComponent } from './angle-image.component';

describe('AngleImageComponent', () => {
  let component: AngleImageComponent;
  let fixture: ComponentFixture<AngleImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngleImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngleImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
