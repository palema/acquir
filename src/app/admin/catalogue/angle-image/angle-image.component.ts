import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';


@Component({
  selector: 'app-angle-image',
  templateUrl: './angle-image.component.html',
  styleUrls: ['./angle-image.component.css']
})
export class AngleImageComponent implements OnInit {

  @Input('group') image_group: FormGroup
  constructor(private fb : FormBuilder) { }

  ngOnInit() {
    this.area_width = document.getElementById('display-area').clientWidth
    this.item_width = document.getElementById('aImg-container').clientWidth
  }

  private position = 0
  private item_width = 0 
  private area_width = 0 
  private total_width = 0


  public slide_prev() {
    var files = <any>document.getElementsByClassName('angle-image-wrapper')

    if (files && this.position < 0) {
      this.position += this.item_width

      for (var i = 0; i < files.length; i++) {
        files[i].style.left = this.position + 'px'
      }
    }

  }

  public slide_next() {
    var files = <any>document.getElementsByClassName('angle-image-wrapper')
    this.total_width = this.item_width * files.length
    if (files && (files.length * this.item_width) - this.area_width > 0 && this.total_width > -this.position+this.item_width) {

      this.position -= this.item_width

      for (var i = 0; i < files.length; i++) {
        files[i].style.left = this.position + 'px'
      }
    }

  }

  get angle_img() {
    return <FormArray>this.image_group.controls['angle_image']
  }



  public file_upload(event) {
    var files = event.target.files

    for (var file of files) {
      if (file.type.startsWith('image/')) {

        const reader = new FileReader()
        reader.onload = ((aImg) => {
          return (e) => {
            this.angle_img.push(this.fb.group({
              name: [file.name],
              src: [e.target.result],
              file: [file]
            }))
            setTimeout(()=>{
              this.slide_next()
            }, 500)
          }
        })(file)
        reader.readAsDataURL(file)
      }
    }

  }

  delete(i, event : Event){
    event.stopPropagation()
    this.angle_img.removeAt(i)
  }
}