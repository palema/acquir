import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogueComponent } from './catalogue.component';
import { AddComponent } from './add/add.component'
import { SpecsComponent } from './specs/specs.component'
import { SizeQuantityComponent } from './size-quantity/size-quantity.component'
import { ViewComponent } from './view/view.component'
import { EditComponent } from './edit/edit.component'
import { RemoveComponent } from './remove/remove.component'
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CatalogueRoutingModule } from './catalogue.routing.module';
import { DepartmentComponent } from './department/department.component';
import { AngleImageComponent } from './angle-image/angle-image.component';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [
    CatalogueComponent, 
    AddComponent, 
    SizeQuantityComponent,
    SpecsComponent,
    ViewComponent,
    EditComponent,
    RemoveComponent,
    DepartmentComponent,
    AngleImageComponent,
    FormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule, 
    CatalogueRoutingModule,
  ]
})
export class CatalogueModule { }
