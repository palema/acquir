import { Component, OnInit } from '@angular/core'
import { CatalogueManagementService } from '../../services/catalogue-management.service'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { CommonService } from '../../services/common.service'

@Component({
  selector: 'app-product',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor(private common: CommonService) { }
  public products : Observable<any>

  ngOnInit() {
    this.getProducts()
  }

  delete_handler(sku : string){
    //document.alert("Are you sure?")
    console.log(sku)
  }

  getProducts(){
    this.products = this.common.getShopProducts()
    .pipe(map(({data })=>{
      console.log(data, " dsfs")
      return data.shopProducts
    }))
  }

}
