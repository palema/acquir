import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-size-quantity',
  templateUrl: './size-quantity.component.html',
  styleUrls: ['./size-quantity.component.css']
})
export class SizeQuantityComponent implements OnInit {
  @Input('group') size_quantity_group : FormGroup

  constructor(private fb : FormBuilder) { }

  ngOnInit() {
    this.add_count ()
  }

  add_count () {
    var count = this.fb.group({
      size : [null, Validators.compose([])],
      quantity : [null, Validators.compose([])]
    })

    this.count.push(count)
  }

  remove_count(index : number){
    this.count.removeAt(index)  
  }

  get count () {
    return <FormArray>this.size_quantity_group.controls['count']
  }

}
