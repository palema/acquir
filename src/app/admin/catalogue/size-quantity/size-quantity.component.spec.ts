import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizeQuantityComponent } from './size-quantity.component';

describe('SizeQuantityComponent', () => {
  let component: SizeQuantityComponent;
  let fixture: ComponentFixture<SizeQuantityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizeQuantityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizeQuantityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
