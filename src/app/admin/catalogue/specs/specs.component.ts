import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder, FormArray, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-specs',
  templateUrl: './specs.component.html',
  styleUrls: ['./specs.component.css']
})
export class SpecsComponent implements OnInit {
  @Input('group') specs_group : FormGroup
  
  constructor(private fb : FormBuilder) { }

  ngOnInit() {
    this.add_spec ()
  }

  get specs () {
    return <FormArray>this.specs_group.controls['specs'] 
  }

  add_spec () {
    this.specs.push(this.specs_template())
  }

  specs_template () {
    return this.fb.group({
      name : [null, Validators.compose([])],
      value : [null, Validators.compose([])],
    })
  }

  delete_spec(index: number){
    this.specs.removeAt(index)
  }

}
