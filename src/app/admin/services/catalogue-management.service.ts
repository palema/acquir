import { Injectable } from '@angular/core'
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';


@Injectable({
  providedIn: 'root'
})

export class CatalogueManagementService {
constructor(private apollo: Apollo) { }



addMutation = gql`
  
    mutation addProduct(
      $title:String!, 
      $quantity:Int!, 
      $list_price: Float!, 
      $display_image: String!,
      $department: [String!]!,
      $features: [Feature]
      ){
        addProduct(
          title:$title,
          quantity:$quantity, 
          list_price:$list_price, 
          display_image:$display_image,
          department:$department,
          features:$features
        ){
          title
        }
      }
  `

editMutation = gql`
  mutation editProduct($sku:String){
    editProduct(sku:$sku){
      title
    }
  }
`
  

  addHandler(addForm){
    return this.apollo.mutate({
      mutation:this.addMutation,
      variables:{
        title: 'rr',
        list_price:55,
        display_image:'gdsdg',
        quantity: 3,
        department: ['yah'],
        features:[{name:"ee", value:"ee", unit:"ee"}]
      }
    })
  }

  deleteMutation = gql`
    mutation deleteProductMutation($sku:string){
      deleteProduct(sku:$sku){
        sku
      }
    }
  `

  deleteHandler(sku: string){
    return this.apollo.mutate({
      mutation: this.deleteMutation,
      variables: {sku}
    })
  }

}
