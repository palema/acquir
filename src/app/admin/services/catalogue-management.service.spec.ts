import { TestBed } from '@angular/core/testing';

import { CatalogueManagementService } from './catalogue-management.service';

describe('CatalogueManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CatalogueManagementService = TestBed.get(CatalogueManagementService);
    expect(service).toBeTruthy();
  });
});
