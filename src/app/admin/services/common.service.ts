import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private apollo: Apollo) { }
  shopProducts = gql`
    query ShopProducts {
      shopProducts{
        title
        sku
        quantity
      }
    }
  `
  shopOrders = gql`
    query ShopOrders {
        shopOffers{
          user
          products{
            quantity
          }
        }
    }
  `

  shopOffers = gql`
    query ShopOffers{
        shopOffers{
          user
          products{
            quantity
          }
          offer
        }
    }
  `

  rootDepartments = gql`
    query RootDepartment {
      rootDepartment{
        name
        children{
          name
        }
      }
    }
  `

  getShopProducts(){
    return this.apollo.watchQuery<any>({
      query: this.shopProducts
    }).valueChanges
  }

  getShopOffers(){
    return this.apollo.watchQuery<any>({
      query: this.shopOffers
    }).valueChanges
  }

  getShopOrders(){
    return this.apollo.watchQuery<any>({
      query: this.shopOrders
    }).valueChanges
  }

  getRootDepartment(){
    return this.apollo.watchQuery<any>({
      query: this.rootDepartments
    }).valueChanges
  }
  
}
