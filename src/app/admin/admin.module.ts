import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { OrderComponent } from './order/order.component';
import { OfferComponent } from './offer/offer.component';
import { AdminNavigationComponent } from './navigation/navigation.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AdminComponent,
    OrderComponent,
    OfferComponent,
    AdminNavigationComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    RouterModule,
    SharedModule
  ]
})
export class AdminModule { }
