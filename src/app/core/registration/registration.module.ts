import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CreateShopComponent } from './create-shop/create-shop.component';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    CreateShopComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule
  ],
  exports : [
    SignInComponent,
    SignUpComponent,
    CreateShopComponent
  ]
})
export class RegistrationModule { }
