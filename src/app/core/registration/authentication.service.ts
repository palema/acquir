import { Injectable } from '@angular/core'
import gql from 'graphql-tag'
import { Apollo } from 'apollo-angular'


@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  constructor(private apollo: Apollo){}

  loginQuery = gql`
    query Login($username: String!, $password: String!){
      login(username:$username, password: $password)
    }
  `

  registerMutation = gql`
    mutation Register($username: String!, $email: String!, $password:String!){
      register(username:$username, email:$email, password:$password)
    }
  `

  registerUser(username: string, email: string, password: string){
    return this.apollo.mutate<any>({
      mutation: this.registerMutation,
      variables:{
        username,
        email,
        password
      }
    })
  }

  loginUser(username: string, password){
    return this.apollo.watchQuery<any>({
      query: this.loginQuery,
      variables: {
        username,
        password
      }
    }).valueChanges
  }

}
