import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../authentication.service'
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import gql from 'graphql-tag';

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  public loginError = null

  constructor( 
    private auth : RegistrationService, 
    private fb : FormBuilder, 
    private router : Router, 
  ) { }


  ngOnInit() {
  }

  public su_form = this.fb.group({
    username : [null, Validators.compose([Validators.required ])],
    email : [null, Validators.compose([Validators.required, Validators.email ])],
    password : [null, Validators.compose([Validators.required, Validators.minLength(8) ])],
    cpassword : [null, Validators.compose([Validators.required, Validators.minLength(8) ])],
  })

  registerMutation = gql`
      mutation RegisterMutation($username: String!, $password: String!, $email: String!) {
        register(username: $username, password: $password, email: $email) {
          username
        }
      }
    `

  public sign_up(){
      this.auth.registerUser(this.username.value, this.email.value, this.password.value)
      .subscribe(({data})=>{
        localStorage.setItem("pub", data.register)
        this.router.navigate(['/home'])
      },(error)=>{
        this.loginError = 'username exists'
      })
  }

   
  get username(){
    return this.su_form.get('username')
  }

  get email(){
    return this.su_form.get('email')
  }

  get password(){
    return this.su_form.get('password')
  }

  get confirm_password(){
    return this.su_form.get('cpassword')
  }

  get pass_cpass(){
    return this.password.value == this.confirm_password.value 
        && this.confirm_password.value !=  null
        && this.password.value != null
        ?true:false
  }
}
