import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../authentication.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  private _password_visibility = false
  public signInError = null

  constructor(
    private auth : RegistrationService, 
    private fb : FormBuilder, 
    private router : Router
    ) { }

  si_form = this.fb.group({
    email : [null, Validators.compose([Validators.required, Validators.email])], 
    password : [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(12)])]
  })

  ngOnInit() {
  }

  get password_visibility() {
    return this._password_visibility
  }

  public show_password() {
    this._password_visibility = !this._password_visibility
  }
  
  sign_in () {
    this.auth.loginUser(this.email.value, this.password.value)
    .subscribe(({data})=>{
      localStorage.setItem("pub", data.login)
      this.router.navigate(['/home'])
    },
    (error)=>{
      this.signInError = error
    })
    

  }

  get email(){
    return this.si_form.get('email')
  }

  get password(){
    return this.si_form.get('password')
  }

}

interface sign_in_response {
  status : boolean,
  message : string,
  show : boolean
}