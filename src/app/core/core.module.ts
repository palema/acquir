import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { CoreRoutingModule } from './core-routing.module'
import { CoreComponent } from './core.component'
import { RouterModule } from '@angular/router'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { RegistrationModule } from './registration/registration.module'

import { AngularFireModule } from '@angular/fire'
import { AngularFireStorageModule } from '@angular/fire/storage'

import { ApolloModule, APOLLO_OPTIONS, Apollo } from "apollo-angular"
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http"
import { InMemoryCache } from "apollo-cache-inmemory"

import { SwUpdate, SwPush } from '@angular/service-worker'
import { environment } from '../../environments/environment'

@NgModule({
  declarations: [
    CoreComponent,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    RouterModule,
    RegistrationModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyBDUpVWn_xKv12j51fv3qzXJdzDfLvZrMY",
      authDomain: "yammer-1904.firebaseapp.com",
      databaseURL: "https://yammer-1904.firebaseio.com",
      projectId: "yammer-1904",
      storageBucket: "yammer-1904.appspot.com",
      messagingSenderId: "903312655353",
      appId: "1:903312655353:web:583c787dc3e5c81f"
    }),
    AngularFireStorageModule,
    ApolloModule,
    HttpLinkModule
  ],
  providers: [{
    provide: APOLLO_OPTIONS,
    useFactory: (httpLink: HttpLink) => {
      return {
        cache: new InMemoryCache(),
        link: httpLink.create({
          uri: environment.graphqlUrl,
          withCredentials: true
        })
      }
    },
    deps: [HttpLink]
  }],
  exports: [CoreComponent]
})
export class CoreModule { 
  constructor(update : SwUpdate, push : SwPush, apollo : Apollo, httpLink : HttpLink ){
    update.available.subscribe(update => {
      console.log('update available')
    })

    push.messages.subscribe(msg => {
      console.log(msg)
    })
    subscribeToNotification()
    function subscribeToNotification (){
      push.requestSubscription({ serverPublicKey : 'BB8Cmu_ps2h5xs6Mbi1qwzXiQr7bZYdA0DkWEC276HTCng2F3pdg3jwTsJrJO7cUEmyqjE6_iNC61Q7IOEHgD8k'})
      .then(PushSubscription => {
        console.log(PushSubscription, "subscribing")
      })
      .catch(err=>{ console.error(err)})
    }

    // push.messages.subscribe((err)=>{
    //   self.registration.showNotification(data.title, {
    //     body : 'Notified by Me',
    //     icon: 'http://imahe.ibb.ico/frYDFd/tmlogo.png'
    // })
    // })
    
    console.log('service worker loaded')
  }
 }
