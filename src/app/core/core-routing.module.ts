import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SignUpComponent } from './registration/sign-up/sign-up.component';
import { SignInComponent } from './registration/sign-in/sign-in.component';
import { CreateShopComponent } from './registration/create-shop/create-shop.component';

const routes: Routes = [
  { path : 'user', loadChildren : 'src/app/user/user.module#UserModule'},
  { path : 'admin', loadChildren : 'src/app/admin/admin.module#AdminModule'},
  { path : 'sign-up', component : SignUpComponent },
  { path : 'sign-in', component : SignInComponent },
  { path: 'create-shop', component : CreateShopComponent },
  { path : '', redirectTo : 'user', pathMatch : 'full' },
  { path : '**', component : PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
