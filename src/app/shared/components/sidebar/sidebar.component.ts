import { Component, OnInit, Input } from '@angular/core';
import { ActionService } from '../../action.service';
import { Router } from '@angular/router';
import { RegistrationService } from 'src/app/core/registration/authentication.service';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() color : String


  constructor(
    private action_service: ActionService, 
    private router : Router, 
    private auth : RegistrationService,
    private apollo: Apollo
    ) { }

  public sidebar_open

  ngOnInit() {
    this.action_service.sidebar_open.subscribe((status)=>{
      this.sidebar_open = status
    })
  }

  public toggle_sidebar(){
    this.action_service.toggle_sidebar()
  }

  public navigate(route : String){
    this.action_service.toggle_sidebar()
    this.router.navigate([route])
  }

  public logout(){
    // this.auth.sign_out()
    // this.router.navigate(['/user'])
  }

  createShop(form){
    if(form.valid){
      console.log(form.value.shopName)
      this.apollo.mutate({
        mutation: gql`
         mutation CreateShop($name:String!){
            createShop(name:$name){
              name
            }
          }
        `,
        variables:{
          name:form.value.shopName
        }
      }).subscribe(({data})=>{
        console.log(data)
      },(error)=>{
        console.log(error)
      })
    }
  }

}
