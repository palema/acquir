import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor() { }

  public sidebar_open : BehaviorSubject<boolean> = new BehaviorSubject(null)

  private status_tracker = true

  public toggle_sidebar(){
    this.sidebar_open.next(!this.status_tracker)
    this.status_tracker = !this.status_tracker
  }
}
