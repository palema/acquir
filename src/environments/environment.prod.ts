export const environment = {
  production: true,
  // dbUrl : 'https://acquir.herokuapp.com/api',
  // imgUrl: 'https://firebasestorage.googleapis.com/v0/b/yammer-1904.appspot.com/o'
  dbUrl: 'http://localhost:3572/api/v0',
  imgUrl: 'http://localhost:3572/api/v0/file',
  graphqlUrl: 'http://localhost:3572/api/v1'
}
