import {Model, Schema, model, Document } from 'mongoose'
import bcrypt from 'bcrypt'

export interface IUser extends Document{
	username : String,
	email : String,
	password : String,
	balance : Number,
	shop : String
} 

export const user_schema = new Schema({
	username : { type : String, unique : true, lowercase : true, required:true },
	email : { type : String, lowercase : true, required:true },
	password : { type: String, required:true},
	balance : { type : Number, min : 0 },
	shop : { type: Schema.Types.String, ref: 'Shop' }
})

user_schema.pre('save', function (next){
	if (this.get('password') && this.isModified('password')){
		this.set('password', bcrypt.hashSync(this.get('password'), 10))
	}

	next()
})

export const User : Model<IUser> = model<IUser>('User', user_schema)

export default User
