import { Schema, Model, model, Document }from 'mongoose'

export interface IShop extends Document {
    name : String, 
    catalogue: [],
    location: {
        city : String,
        street : String,
        erf : String
    },
    manager: String,
    products : [String],
}

export const shop_schema: Schema  = new Schema({
    name: { type: String, unique: true, required:true },
    catalogue: [String],
    location: {
        city : String,
        street : String,
        erf : String
    },
    products : [{type : Schema.Types.String, ref: 'Product'}],
    manager: { type: Schema.Types.String, ref: 'User' }
})


export const Shop : Model<IShop> = model <IShop>('Shop', shop_schema)

export default Shop

