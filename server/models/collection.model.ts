import mongoose from 'mongoose'

const Collection = new mongoose.Schema({
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    collection: {
        type: Array,
        items: {
            sku: String,
            quantity: Number,
            unit_price: Number,
            total: Number
        },
        maxItems: 100
    },
    collection_expiry: Date,
    total_cost: Number,

})

export default mongoose.model('Collection', Collection)