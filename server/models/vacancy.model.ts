import mongoose from 'mongoose';

var Vacancy = new mongoose.Schema({
    firm : String,
    job_description : String,
    job_title : String,
    offered_salary : Number,
    requirements : [],
    start_date : Date,
    end_date : Date
})


export default mongoose.model('Vacancy', Vacancy)