import { Document, Model, model, Schema } from 'mongoose';

export interface IOrder extends Document { 
    user: string,
    number: string,
    password: string,
    total: number,
    participants : [string],
    status : string,
    products: [
        {
            sku: string,
            quantity: number
        }
    ],
    shop : string
    billing_address : {
        city : string,
        street : string,
        erf : string
    }
}

export interface IOrder_model extends Model<IOrder> {

}

export const order_schema: Schema = new Schema({
    user: {
        type: Schema.Types.String,
        ref: 'User',
        required:true
    },

    number: {
        type : String, unique : true,
        required:true
    },
    password: {type: String, required:true},
    products: {
        type: Array,
        items: {
            sku: String,
            quantity: Number,
        },
        maxItems: 100
    },
    shop : {
        type : String,
        ref : 'Shop'
    },
    billing_address: {
        city: String,
        street: String,
        erf: Number
    },
    status: {
        type : String,
        enum: ['pending', 'fulfilled', 'enroute', 'cancelled'],
        default : 'pending'
    }
})


export const Order: Model<IOrder> = model<IOrder>('Order', order_schema)

export default Order

