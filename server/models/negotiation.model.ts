import { Schema, Model, model, Document } from 'mongoose'

export interface INegotiation extends Document { 
    user : String,
    products : [
        {
            sku : String,
            quantity : Number
        }
    ],
    number: String
    shop : String,
    offer : Number,
    status: String
}

export interface INegotiation_model extends Model<INegotiation> { }

export const negotiation_schema: Schema = new Schema({
    user: {
        type: String,
        ref: 'User',
        unique: true
    },
    products: {
        type: Array,
        items: {
            type: {
                sku: {
                    type: String,
                    ref: 'Product'
                },
                quantity: {
                    type: Number,
                    min: 1
                }
            }
        }
    },
    offer: Number,
    status: {
        type : String,
        enum : ['rejected', 'accepted', 'ongoing', 'pulledout'],
        default : 'ongoing'
    },
    shop: {
        type: String,
        ref: 'Shop'
    }
})

export const Negotiation: Model<INegotiation> = model<INegotiation>('Negotiation', negotiation_schema)

export default Negotiation