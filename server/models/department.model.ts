import mongoose from 'mongoose';

var Department = new mongoose.Schema({
    name: {type : String, unique : true, required:true},
    keywords: [String],
    parent: {
        type: Array,
        items: {
            ref: '#Department'
        }
    },
    children: {
        type: Array,
        items: {
            ref: '#Department'
        }
    },
    features : {
        type : Array,
        items : {
            name : { type:String, required:true},
            units : [String]
        }
    }

})

export default mongoose.model('Department', Department)