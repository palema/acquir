import {Model, Schema, model, Document } from 'mongoose'

export interface IProduct extends Document {
	sku : String,
	title : String,
	list_price : Number,
 	quantity : Number,
	department : [String],
	description : String,
	display_image : String,
	angle_images : [String],
	shop : String,
	rating : Number,
	purchases : Number,
	views :Number,
	cart_listings : Number,
	specs : [{name : String, value : String}]
}

export const product_schema : Schema = new Schema ({
		sku : {
			type : String,
			unique : true,
			required:true
		},
		title : {
			type: String,
			unique: true,
			required:true
		},
		list_price : { type:Number, required:true},
		quantity : { type: Number, min: 0, required:true},
		department : [String],
		description : String,
		display_image : { type:String, required:true},
		angle_images : [String],
		shop : {type: Schema.Types.String, ref: 'Shop', required:true},
		rating : {type :Number, min : 1, max : 5},
		purchases : {type :Number, min : 0},
		views :{type :Number, min : 0},
		cart_listings : {type :Number, min : 0},
		offers_made : { type : Number, min: 0 },
		negotiable : Boolean,
		specs : {
			type : Array,
			items : {
				name : String,
				value : String
			}
		}
})


product_schema.statics.findByDepartment = function(dep : string , callback : Function){
	return model('Product').find({department: dep},callback)
}

product_schema.statics.findBySku = function(sku : string, callback : Function){
	return model('Product').find({sku: sku},callback)
}

product_schema.statics.findByShop = function(retailer : String, callback : any){
	return Product.find({retailer: retailer},callback)
}

export const Product : Model<IProduct> = model <IProduct>('Product', product_schema)

export default Product



