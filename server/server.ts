import express,{ Request, Response} from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import cors from 'cors'
import mongoose from 'mongoose'
import ejs from 'ejs'
import cookieParser from 'cookie-parser'

import api from './routes/v0/api'
import graphqlHTTP from 'express-graphql'

import schema from "./routes/v1/schema"
import { extractTokenBody } from './routes/v1/user/user'

import notification from './routes/v1/notification'

let app = express()

/*
	cross origin rules -- disallows session setting
    of users from different IP's
*/
app.use(cors({ origin: 'http://localhost:4200', credentials: true }))
app.use(cookieParser())
const  db = 'mongodb://LAPTOP-5SV7PL85:27017,LAPTOP-5SV7PL85:27018,LAPTOP-5SV7PL85:27019/logic?replicaSet=rs'
/*connection to database*/
const db_uri = process.env.MONGOLAB_SILVER_URI || db
mongoose.connect(db_uri, { useNewUrlParser: true })

export const connection = mongoose.connection

//db connection configuration
connection.once('open', () => {
	console.log('mongoDb connection established');
});

connection.on('error', (err) => {
	console.log('server error occured : ' + err);
});

connection.on('SIGINT', () => {
	connection.close(() => {
		console.log('Disconnected through termination');
		process.exit(0);
	});
});




//views
app.set('views', path.join(__dirname, '../www'))
app.set('view engine', 'ejs')
app.engine('html', ejs.renderFile)


//static folder
app.use(express.static(path.join(__dirname, '../www')))

//parsers
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

//route to api's
app.use('/api/v0', api)

//new api that works with graphql
app.use('/api/v1', graphqlHTTP((req, res) => ({
	schema,
	graphiql: true,
	context: { req, res, tokenBody: extractTokenBody(req) }
}))
)

app.use('/notify', notification)


// serves all html related requests
app.get('*', (req, res) => {
	res.render(path.join(__dirname, '../www/index.html'))
})

var port = process.env.PORT || 3572
app.listen(port, () => {
	console.log("listening on port ", port)
})

