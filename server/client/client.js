const publicVapidKey = 'BB8Cmu_ps2h5xs6Mbi1qwzXiQr7bZYdA0DkWEC276HTCng2F3pdg3jwTsJrJO7cUEmyqjE6_iNC61Q7IOEHgD8k'

//check for service worker
if('serviceWorker' in navigator){
    send().catch(err=> console.error(err))
}

// Register SW, Register Push, Send Push
async function send(){
    console.log('Registering service worker')
    const register = await navigator.serviceWorker.register('/worker.js',
    {
        scope : '/'
    })
    console.log('service worker registered')
    
    //register push
    console.log('registering push')
    const subscription = await register.pushManager.subscribe({
        userVisibleOnly : true,
        applicationServerKey: urlBased64ToUint8Array(publicVapidKey)
    })

    //send push notification
console.log('pushing notification')
await  fetch('http://localhost:3572/subscribe', {
    method : 'POST',
    body : JSON.stringify(subscription),
    headers: {
        'content-type' : 'application/json'
    }
})
}

function urlBased64ToUint8Array(base64String){
    const padding = '='.repeat((4 - base64String.length % 4) % 4)
    const base64 = (base64 + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/')

    const rawData = window.atob(base64)
    const outputArray = new Uint8Array(rawData.length)

    for (let i = 0; i < rawData.length; ++i){
        outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
}

