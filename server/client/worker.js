console.log('service worker loaded')

self.addEventListener('push', e=>{
    console.log('pushed notification')
    const data = e.data.json()
    console.log('push received')
    self.registration.showNotification(data.title, {
        body : 'Notified by Me',
        icon: 'http://imahe.ibb.ico/frYDFd/tmlogo.png'
    })
})