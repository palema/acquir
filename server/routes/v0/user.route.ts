import express from 'express'
let user_api = express.Router()

//user model is used for setting up operations with the database
import User from '../../models/user.model'

import { 
    save_user, set_session, find_by_email, check_session,
    destroy_session

} from './authentication.route'

user_api.route('/findByUsername/:username').get((req, res) => {
	
})

const find_by_username = function(req, res, next){
    User.findOne({
		username: req.params.username//,
	})
		.exec((err, user) => {
			if (err)
				console.log(err)
			else
				return next()
		})
}

//routes to user resources
user_api.route('/sign-up/').post(find_by_username, save_user, set_session)
user_api.route('/sign-in/').post(find_by_email, set_session)
user_api.route('/verify-login/').post(check_session)
user_api.route('/sign-out').get(destroy_session)


// user_api.route('/info/get').get((req, res) => {
// 	if (req.session.email) {
// 		User.findOne({ email: req.session.email }, { pasword: false, _id: false }, (err, doc) => {
// 			if (err) return res.send(err)
// 			if (doc) return res.send(doc)
// 		})
// 	} else {
// 		res.send({})
// 	}
// })

export default user_api