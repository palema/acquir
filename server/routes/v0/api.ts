import express from 'express'
import fs from 'fs'
import session from 'express-session'


import product_api from './product.route'
import order_api from './order.route'
import common_api from './common.route'
import negotiation_api from './negotiation.route'
import user_api from './user.route'
import shop_api from './shop.route'
import home_api from './home.route';

var RedisStore = require('connect-redis')(session)

let api = express.Router()

api.use(session({
    store: process.env.NODE_ENV === 'production'
        ? new RedisStore({
            url: process.env.REDISCLOUD_URL
        })
        : new RedisStore({
            url: 'redis://127.0.0.1:6379'
        }),
    secret: 'fdsjJKJusfKsf451s',
    resave: true,
    saveUninitialized: true
}))

/* routes to specified api's e.g all product
requests pass through the product api */
api.use('/product', product_api)
api.use('/order', order_api)
api.use('/common', common_api)
api.use('/negotiate', negotiation_api)
api.use('/user', user_api)
api.use('/shop', shop_api)
api.use('/home', home_api)

api.route('/file/:name').get((req, res) => {
    var file = fs.readFileSync('assets/images/' + req.params.name)

    res.send(file)
})

export default api