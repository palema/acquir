import { Order } from '../../models/order.model'
import User from '../../models/user.model'
import Product from '../../models/product.model'

import express from 'express'

let order_api = express.Router()

export const find_product = async (req, res, next) => {
    var order = req.body
    var items = req.body.items;
    var total = 0

    var item_list = []


    for (var i = 0; i < items.length; i++) {
        item_list.push(items[i].sku)
    }

    item_list.sort()


    await Product.find({ sku: { $in: item_list } }, (err, docs) => {
        if (docs) {
            for (var i = 0; i < docs.length; i++) {
                if (docs[i].sku == items[i].sku && docs[i].quantity > items[i].quantity) {
                    total += (items[i].quantity * docs[i].list_price.valueOf())
                    items[i].shop = docs[i].shop
                    items[i].title = docs[i].title
                }
            }
        }
    }).sort({ sku: 'asc' })


    req.params.total_cost = total
    order.items = items
    req.params.user_order = order
    req.params.collection_charges = total * 0.15
    return next()
}

export const check_user_balance = (req, res, next) => {
    User.findOne({ email: req.session.email }, (err, doc: any) => { //user doc to check balance

        if (err) return res.send({status : false, message :"user not found"})
        if (!doc) return res.send({ status: false, message: "user's doc not found" })

        if (req.params.collection_charges > doc.balance) {
            return next({ status: false, message: "Insufficient amount" })
        } else {
            return next()
        }
    })
}

export const decreament_user_balance = (req, res, next) => {
    User.updateOne({ email: req.session.email }, { $inc: { balance: -req.params.collection_charges } }, (err, response) => {
        if (err) return next(err)
        console.log('decreament')
        if (response) return next()

    })
}

export const update_order = (req, res, next) => {
    var user_order = req.params.user_order
    var order_number = req.body.order_number
    Order.findOne({ owner: req.session.email, order_no: order_number }, (err, doc) => {
        if (err) {
            return next({ status: false, message: err })
        }


        if (doc) {
            var db_orders = doc
            var db_order_items = db_orders.order_items
            var tmp_order_items = db_order_items  // document to be substituted for db's doc
            var item_found = false

            for (var j = 0; j < user_order.length; j++) {
                for (var k = 0; k < db_order_items.length; k++) {  //check if item is in order already
                    if (user_order[j].sku == db_order_items[k].sku) {
                        tmp_order_items[k].quantity += user_order[j].quantity
                        item_found = true
                    }
                }

                console.log(user_order, "user_order")

                if (!item_found) {
                    tmp_order_items.push({ //else add a new entry
                        sku: user_order[j].sku,
                        title: user_order[j].title,
                        quantity: user_order[j].quantity,
                        sub_total: user_order[j].quantity * user_order[j].price,
                        shop: user_order[j].shop
                    })
                }
            }

            //update db's order doc
            Order.update({ owner: req.session.email }, { $set: { "order_items": tmp_order_items } }, (err, doc) => {
                if (err) return next(err)
                if (doc) next()
            })
        }else{
            create_order_document(req, res, next)
        }

    })
}

export const decreament_db_product = async (req, res, next) => {
    var errors = []

    await req.params.user_order.items.forEach(element => {
        Product.findOneAndUpdate({ sku: element.sku }, { $inc: { quantity: -element.quantity } }, (err, doc) => {
            if (err) errors.push(err)
        })
    });

    if (!errors.length) {
        res.status(200).send({ status: true, message: "order successfull" })
    }
}

export const create_order_document = async (req, res, next) => {
    var email = req.session.email
    if (!email) {
        res.json({ status: false, message: "user not logged in" })
    } else {

        var order_password = req.body.password
        var order_number = gen_order_number()

        const user_order = req.params.user_order

        const order_info = new Order({
            order_number: order_number,
            password: order_password,
            billing_address: {
                city: user_order.billing_address.city,
                street: user_order.billing_address.street,
                erf: user_order.billing_address.erf
            }

        })

        await user_order.items.forEach(item => {
            order_info.order_items.push(
                {
                    sku: item.sku,
                    title: item.title,
                    quantity: item.quantity,
                    shop: item.shop,
                    sub_total: item.quantity * item.price
                }
            )

            order_info.participants.push(item.shop)
        })


        order_info.save((err, data) => {
            if (err)
                console.log(err)
            //return res.json({ status: false, message: "err : order unsuccesful" })
            if (data)
                return next()

        })
    }
}

function gen_order_number() {
    var character_space = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

    var time = new Date().getTime().toString()
    var generated_string = ''
    var index
    for (var i = time.length - 1; i > 2; i--) {
        index = Math.floor(Math.random() * (character_space.length - parseInt(time[i]))) + parseInt(time[i]);
        generated_string = generated_string.concat(character_space[index])
    }

    return generated_string

}

order_api.route('/').post(check_user_balance, find_product,
	update_order, decreament_user_balance, decreament_db_product);

order_api.route('/orders').get((req, res) => {
	Order.findOne({ owner: req.session.email }, { orders: 1, _id: 0 }, (err, doc) => {
		if (doc) res.json(doc)
	})
})


export default order_api