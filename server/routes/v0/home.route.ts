/* home products should be based on :
    -user activity
    -product status
    -product recency

    steps : 
    -check cache or cookies
    -find all related products
    -sort products
    -send
*/

import express from 'express'
import Product from '../../models/product.model'
import redis from 'redis'

const home_api = express.Router()

home_api.get('/findTrending', (req, res) => {
    Product.aggregate([
        {
            $sort: { 'purchases': -1, 'views': -1, 'cart_listings': -1, 'rating' : -1 }
        },
        {
            $limit: 10
        }
    ], (err, docs)=>{
        if(err) return res.send({message : "could not fetch data"})
        return res.send(docs)
    })
})

home_api.get('/findWanted', (req, res) => {
    Product.aggregate([
        {
            $sort: { 'offers_made': -1 ,'cart_listings': -1 }
        },
        {
            $limit: 10
        }
    ], (err, docs)=>{
        if(err) return res.send({message : "could not fetch data"})
        return res.send(docs)
    })
})

home_api.get('/findRecommended', (req, res) => {
    var redis_connection = redis.createClient()
    redis_connection.on('connect', () => {
        console.log('connected to redis')
        redis_connection.get('user', (err, obj) => {
            console.log(obj)
        })

        redis_connection.end(true)
    })


    Product.aggregate([
        {
            $sort: { 'offers_made': -1 ,'cart_listings': -1 }
        },
        {
            $limit: 10
        }
    ], (err, docs)=>{
        if(err) return res.send({message : "could not fetch data"})
        return res.send(docs)
    })
})

home_api.get('/getDeals')

home_api.get('/findMen', (req, res) => {
    Product.schema.statics.findByDepartment("men", (err, product) => {
        if(err) res.send(err.msg)
        if(product) res.send(product)
        else res.send({message: "not found"})
    })
})
export default home_api
