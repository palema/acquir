import express from 'express'
let shop_api = express.Router()

import Shop from '../../models/shop.model'
import Order from '../../models/order.model';
import Negotiation from '../../models/negotiation.model';

shop_api.route('/insert').post((req, res) => {
	var r = req.body
	const shop = new Shop({
		name: r.name,
		catalogue: r.catalogue
	})

	shop.save((err, doc) => {
		res.send(doc)
	})
})

shop_api.route('/findBid').get((req, res) => {
	if (!req.session.email) {
		Negotiation.findOne({}, (err, docs) => {
			if (err) return res.status(400)
			if (docs) return res.send(docs)
		})
	} else {
		return res.status(401)
	}
})

shop_api.route('/findShop').get((req, res) => {
	Shop.find((err, docs) => {
		if (err) res.send(new Error('An err occured ' + err))
		if (docs) res.send(docs)
	})
})

shop_api.route('/findOrders').get((req, res) => {
	if (req.session.shop) {
		Order.aggregate([{ $match: { participants: req.session.shop } },
		{
			$project: {
				order_items: {
					$filter: {
						input: "$order_items",
						as: "item",
						cond: { $eq: ["$$item.shop", req.session.shop] }
					}
				},
				order_number : true,
				_id : false,
				billing_address : true,
				status : true
			}
		}
		], (err, docs) => {
			if(docs) return res.status(200).send(docs)
			
		})
	}
})

export default shop_api