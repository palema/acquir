import bcrypt from 'bcrypt'
import redis from 'redis'
import session from 'express-session'

import User from '../../models/user.model'
var port = process.env.PORT || 7200

export const find_by_email = function (req, res, next) {
    const email = req.body.email
    const password = req.body.password

    User.findOne({ email: email }, (err, data: any) => {
        if (err) {
            return next(err)
        }
        if (!data) {
            return res.json({ status: false, message: 'incorect email or password' })
        }

        if (data.shop) {
            req.params.shop = data.shop
        }

        bcrypt.compare(password, data.password, function (err, result) {
            if (err) return next(err)
            if (!result) return res.json({ status: false, message: 'incorect email or password' })
            return next()
        })

    })

}

export const save_user = (req, res, next) => {

    const user = new User(req.body)

    user.save((err, data) => {
        if (err)
            return next(err)
        else
            return next()
    })
}

export const set_session = (req, res) => {
    req.session.email = req.body.email
    
    if (req.params.shop) {
        req.session.shop = req.params.shop
    }

    res.send({ status: true, message: 'successful' })

}

export const check_session = (req, res) => {
    res.json({
        status: !!req.session.email
    })
}

export const destroy_session = (req, res) => {
    req.session.destroy()
    res.send({
        status: true, message: "logged out"
    })
}
