import mongoose from 'mongoose';
import ngram from 'n-gram'

import Department from '../../models/department.model'
var product_schema = mongoose.model('Product')

import express from 'express'
let common_api = express.Router()

const not_found = { status: false, message: 'not matches found' }


// common_api.route('/insertDepartment').post((req, res) => {
// 	var r = req.body
// 	const department = new Department({
// 		name: r.name,
// 		keywords: r.keywords,
// 		features: r.features
// 	})

// 	department.save((err, doc) => {
// 		res.send(doc)
// 	})
// })


common_api.route('/findDepartment').get((req, res) => {
	Department.find((err, docs) => {
		if (err) res.send(new Error('An error occured : ' + err))
		if (docs) res.send(docs)
	})
})




const search = (req, res, next) => {
    let name = req.params.q
    let escaped = name.replace(/[\\[.+*?(){|^$]/g, "\\$&");
    product_schema.aggregate([{
        $match:
            { $or: [{ title: new RegExp(`${escaped}`, 'gi') }, { brief_description: '' }] }
    },
    {
        $sort: { 'quantity': -1 }
    },
    {
        $limit: 50
    }],
        (err, docs) => {
            if (err) return next({ status: false, message: 'Internal server error' })
            if (!docs) return next({ status: false, message: 'No match found' })
            else res.send(docs)
        })
}



common_api.route('/search/:q').get(search)


export default common_api