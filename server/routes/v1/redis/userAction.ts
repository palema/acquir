import { asyncRedis } from 'async-redis'

const VOTE_SCORE = 423

/* tracks user login sessions */
export const update_token = (conn: asyncRedis, token: string, user: string) => {
    let timestamp = new Date().getTime()
    conn.hset('login:', token, user)
    conn.zadd('recent:', token, timestamp)
}


/* adds product to user's cart and updates products cart inclusion count */
export const add_to_cart = ({ conn, user, sku, count }: { conn: asyncRedis, user: string, sku: string, count: number }) => {
    var res = null
    if (count <= 0) {
        return conn.hdel('user:'+ user + ':cart', sku)
        .then((reply)=>{
            return conn.hincrby('product:' + sku, 'carts', -count)
        })
        .then(reply=>{
            return reply
        })
        .catch(err=>{
            return err
        })   
    } else {
        return conn.hincrby('user:'+ user + ':cart', sku, count)
        .then((reply)=>{
            return conn.hincrby('product:' + sku, 'carts', count)
        })
        .then(reply=>{
            return reply
        })
        .catch(err=>{
            return err
        })
    }
}

export const add_to_order = ({
    conn, token, items, orderNo = null, total
    }:{
        conn: asyncRedis, 
        token: string, 
        items: [
            {
                item:string, 
                quantity: number
            }
        ], 
        orderNo: number, 
        total:string
    }) => {
    for(var i=0;i<items.length;i++){
        conn.hmset('user:'+token+':order:'+orderNo, items[i].sku, items[i].quantity)
    }
    conn.hincrby('user:'+token+':order:'+orderNo, 'total', total.toString())
}

export const createOrder = async ({
    conn, token, items, total, orderNo, city, street, erf, password,shop
    }:{
        conn: asyncRedis, 
        token: string, 
        items: [
            {
                sku:string, 
                quantity: number
            }
        ], 
        orderNo: number, 
        total:number,
        city:string,
        street:string,
        erf:string,
        password:string,
        shop:string
    })=>{
        try{

        for(var i=0;i<items.length;i++){
            conn.hmset('user:'+token+':order:'+orderNo, items[i].sku, items[i].quantity)
        }
        return await conn.hmset('user:'+token+':order:'+orderNo, 
            'shop', shop, 
            'city', city, 
            'street',street, 
            'erf', erf, 
            'password', password,
            'total', total.toString())
    }catch(err){
        throw Error(err)
    }

}

const complete_purchase = (conn : asyncRedis, sku : string, qty : number )=>{
    conn.hincrby('product:' + sku, 'purchases', qty)
    conn.hincrby('product:' + sku, 'quantity', -qty)
}

/* handles product rating */
export const product_rate = ({ conn, user, sku }: { conn: asyncRedis; user: string; sku: string; }) => {
    var reply = conn.sadd('rated:' + sku, user)
    if (reply) {
        conn.zincrby('score:', VOTE_SCORE, sku)
        conn.hincrby('product:' + sku, 'views', 1)
    }
}
