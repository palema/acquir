import { RedisClient } from 'redis'
import { min } from 'lodash'

/* readjusts item views to accomodate new ones */
const rescale_viewed = (conn: RedisClient) => {
    while (!QUIT) {
        conn.zremrangebyrank('viewed:', 20000, -1)
        conn.zinterstore('viewed', 0.5)
    }
}


let QUIT = false
let LIMIT = 5

const clean_sessions = (conn: RedisClient) => {
    while (!QUIT) {
        conn.zcard('recent:', (err, size) => {
            if (size <= LIMIT) {
                setInterval(() => {
                    clean_sessions
                }, 1000)
            }
            let end_index = min([size - LIMIT, 100])
            conn.zrange('recent:', 0, end_index - 1, (err, sessions) => {
                let session_keys = []
                sessions.forEach(session => {
                    session_keys.push('viewed:' + session)
                    session_keys.push('cart:' + session)
                })

                conn.del(session_keys)
                conn.hdel('login:', sessions)
                conn.zrem('recent:', sessions)
            })

        })

    }
}