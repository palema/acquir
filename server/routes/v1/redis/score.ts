import { RedisClient } from 'redis'

const ONE_WEEK_IN_SECONDS = 7 * 86400
const VOTE_SCORE = 423


/* copy of product when sellers enter it into database */
export const add_product = async (conn: RedisClient, shop: string, title: string, sku: string, price : number ) => {
    conn.incr('product:', (err, reply) => {
        let now = new Date().getTime()
        let product = 'product:' + sku

        conn.hmset(product, {
            'title': title,
            'shop': shop,
            'time': now,
            'views': 1,
            'list_price': price
        })

        conn.zadd('score:', now + VOTE_SCORE, product, (err, reply) => {
            console.log(reply)
        })
        conn.zadd('time:', now, sku, (err, reply) => {
            console.log(reply)
        })
    })
}

/* 
    creates department sets and add products to them 
    will be useful for homepage queries    
*/
export const add_remove_topics = (conn: RedisClient, sku: string, to_add = [], to_remove = []) => {
    let product = 'product:' + sku
    to_add.forEach(topic => {
        conn.sadd('topic:' + topic, product)
    })

    to_remove.forEach(topic => {
        conn.srem('topic:' + topic, product)
    })
}





