import asyncRedis from 'async-redis'

const VOTE_SCORE = 423

/* records data about product activity */
export const product_view = async ({ conn, token, sku }: { conn: asyncRedis; token: string; sku: string; }) => {
    var reply = conn.sadd('product:viewed:' + sku, token)
    if (reply) { /* only incr product views if user hasn't viewed it before */
        conn.zincrby('product:score', VOTE_SCORE, sku)
        conn.hincrby('product:' + sku, 'views', 1)
    }
}


/* tracks products or topics viewed by customer */
export const user_view = (conn: asyncRedis, token: string, sku : string = null, topic : string = null)=>{
    let timestamp = new Date().getTime()
    if(sku){
        conn.zadd('user:' + token + ':viewed', timestamp, sku)
        conn.zremrangebyrank('user:' + token + ':viewed', 0, -26)
    }

    if(topic){
        conn.zadd('user:' + token + ':topic', timestamp, topic) 
        conn.zremrangebyrank('user:' + token+ ':topic', 0, -26) //limit topic set to 26 items per user
    }
    
}

/** 
 * track all views including unique ones
 * >2019
 *  > 05
 *   > 01 
 */
function track_product_views(conn : asyncRedis, token : string ){
    let date = new Date()
    let dateString = `${date.getFullYear()}:${date.getMonth()}:${date.getDay()}`
    conn.incr(`${dateString}:product:views`)
    conn.sadd(`${dateString}:product:unique`, token)
}