import { GraphQLObjectType, GraphQLString, GraphQLID, GraphQLSchema, GraphQLList } from "graphql";
import Department from '../../../models/department.model'

export const DepartmentType = new GraphQLObjectType({
    name: 'Department',
    fields: () => ({
        name: { type: GraphQLString },
        parent : { 
            type: DepartmentType,
            resolve(parent){
                return Department.findOne({name: parent.parent})
            }
        },
        children : { 
            type: GraphQLList(DepartmentType),
            resolve(parent){
                return Department.find({parent: parent.name})
            }
        }
    })
})

export const DepartmentQuery = {
    department: {
        type: DepartmentType,
        args: {
            name: { type: GraphQLID }
        },
        resolve(parent, args) {
            return Department.findOne({ name: args.name })
        }
    },
    departments: {
        type: GraphQLList(DepartmentType),
        resolve(parent, args) {
            return Department.findOne({ name: args.name })
        }
    },
    rootDepartment: {
        type: GraphQLList(DepartmentType),
        resolve(){
            return Department.find({ parent: "none" })
        }
    }

}
