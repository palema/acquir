import { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLFloat, GraphQLInputObjectType } from 'graphql';
import { DepartmentType } from '../department/department';
import Product from '../../../models/product.model';
import Department from '../../../models/department.model'
import { ShopType } from '../shop/shop';
import Shop from '../../../models/shop.model';
import { add_to_cart } from '../redis/userAction'
import asyncRedis from 'async-redis'

const conn = asyncRedis.createClient()

export const ProductType = new GraphQLObjectType({
    name: 'Product',
    fields: () => ({
        title: { type: GraphQLString },
        list_price: { type: GraphQLString },
        sku: { type: GraphQLString },
        quantity: { type: GraphQLInt },
        display_image: { type: GraphQLString },
        rating: { type: GraphQLInt },
        purchases: { type: GraphQLInt },
        views: { type: GraphQLInt },
        cart_listings: { type: GraphQLInt },
        department: {
            type: GraphQLList(DepartmentType),
            resolve(parent) {
                return Department.find({ department: { $in: parent.department } })
            }
        },
        shop: {
            type: ShopType,
            resolve(parent) {
                return Shop.findOne({ name: parent.shop })
            }
        }
    })
})

const ProductFeatureType = new GraphQLInputObjectType({
    name: 'Feature',
    fields: () => ({
        name: { type: GraphQLNonNull(GraphQLString) },
        value: { type: GraphQLNonNull(GraphQLString) },
        unit: { type: GraphQLString }
    })
})

export const ProductQuery = {
    product: {
        type: ProductType,
        args: {
            title: { type: GraphQLString },
            sku: { type: GraphQLString }
        },
        resolve(parent, args, { req, user }) {
            return Product.findOne({ title: args.title })
        }
    }
}

export const ProductMutation = {
    addProduct: {
        type: ProductType,
        args: {
            title: { type: GraphQLNonNull(GraphQLString) },
            list_price: { type: GraphQLNonNull(GraphQLFloat) },
            quantity: { type: GraphQLNonNull(GraphQLInt) },
            display_image: { type: GraphQLNonNull(GraphQLString) },
            department: { type: GraphQLNonNull(GraphQLList(GraphQLNonNull(GraphQLString))) },
            features: { type: GraphQLList(ProductFeatureType) }
        },
        resolve(parent, args, { tokenBody }) {
            if (!tokenBody.shop) {
                throw new Error('shop not registered')
            }
            let sku = gen_sku(args.title, tokenBody.shop)
            let product = new Product({
                title: args.title,
                list_price: args.list_price,
                quantity: args.quantity,
                display_image: args.display_image,
                sku: sku,
                specs: args.features,
                shop: tokenBody.shop,
                department: args.department
            })

            // redis_func.add_product(conn, args.shop, args.title, sku)
            // redis_func.add_remove_topics(conn, sku, args.department)
            return product.save()
        }
    },

    deleteProduct: {
        type: ProductType,
        args: { title: { type: GraphQLNonNull(GraphQLString) } },
        resolve(parent, args, { tokenBody }) {
            if (!tokenBody.shop) {
                throw new Error('Not authenticated')
            }
            return Product.findOneAndDelete({ title: args.title })
        }
    },
    addToCart: {
        type: ProductType,
        args: {
            sku: { type: GraphQLNonNull(GraphQLString) },
            quantity: { type: GraphQLNonNull(GraphQLInt) }
        },
        resolve(parent, args, { tokenBody }) {
            if (!tokenBody.user) {
                throw new Error('Not authenticated')
            }
            add_to_cart({ conn, user: tokenBody.user, sku: args.sku, count: args.quantity })
        }
    }
}

function gen_sku(title: string, shop: string) {
    return title.substr(0, 5) + shop.substr(0, 5) + Math.floor(Math.random() * 1000)
}

