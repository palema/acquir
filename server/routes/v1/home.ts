import redis from 'redis'
import { GraphQLObjectType, GraphQLString, GraphQLList, GraphQLInt, GraphQLID } from 'graphql'
import asyncRedis from 'async-redis'

import { ProductType } from './product/product'
import { get_products } from './redis/fetch'

import Product from '../../models/product.model'
import { extractTokenBody } from './user/user'

let conn = asyncRedis.decorate(redis.createClient())


export const HomeType = new GraphQLObjectType({
    name: 'Home',
    fields: () => ({
        topic: { type: GraphQLString },
        products: { type: GraphQLList(ProductType) }
    })
})

export const HomeQuery = {
    home: {
        type: GraphQLList(HomeType),
        args: {
            screenWidth: { type: GraphQLInt },
            screenHeight: { type: GraphQLInt },
            scroll: { type: GraphQLInt },
        },
        async resolve(parent, args, { req, res }) {
            if (req.headers['cookie']) {
                /* get topics based what user usually views */
                var data: any = extractTokenBody(req)
                var topics = await conn.zrevrange('user:' + data.user + ':topic', 0, 10)
                if(topics.length == 0){ //if user has no prior viewed topics fetch common
                    topics = await conn.zrevrange('topic:top', 0, 10)
                }else if(topics.length < 10){
                    var extra_topics = await conn.zrevrange('topic:top', 0, 10-topics.length)
                    for(var i=0; i < extra_topics.length; i++){
                        topics.push(extra_topics)
                    }
                }
                return getTopicProducts(topics)
                
            } else {
                /* get list of top 10 popular topics */
                var topics = await conn.zrevrange('topic:top', 0, 10)
                return getTopicProducts(topics)
            }

        },
        recommended: {
            type: GraphQLList(HomeType),
            args: {
                sku: { type: GraphQLID }
            },
            resolve(parent, args) {
                /** find related products */
            }
        }
    }
}

async function getTopicProducts(topics: [string]) {
    var return_data = []
    /** foreach topic get top products */
    let { products, not_found } = await get_products(conn, 0, topics)
    
    /**if some items data aren't in the cache visit database */
    for (var i = 0; i < not_found.length; i++) {
        if (not_found[i][topics[i]].length > 0) {
            var db_product = await Product.find({ sku: { $in: not_found[i][topics[i]] } })
            db_product.forEach(prod => {
                products[topics[i]].push(prod)
            })
        }
    }

    //format return data to match api style
    for (var i = 0; i < topics.length; i++) {
        return_data.push({ topic: topics[i], products: products[topics[i]] })
    }
    return return_data
}
