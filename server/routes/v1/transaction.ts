import Product from '../../models/product.model'
import User from '../../models/user.model'
import mongoose from 'mongoose'
import { connection } from '../../server'
import { GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLList, GraphQLFloat, GraphQLEnumType, GraphQLInputObjectType, GraphQLInt } from 'graphql'
import { UserType } from './user/user'
import { ProductType } from './product/product'
import { Order } from '../../models/order.model'
import { Negotiation } from '../../models/negotiation.model'
import { add_to_cart } from '../../routes/v1/redis/userAction'
import asyncRedis from 'async-redis'
var conn = asyncRedis.createClient()
var NOT_LOGGED_IN = 'not logged in'
var NOT_HAVING_SHOP = 'not having shop'
const LocationType = new GraphQLObjectType({
	name: 'Location',
	fields: () => ({
		city: { type: GraphQLString },
		street: { type: GraphQLString },
		erf: { type: GraphQLString },
		latitude: { type: GraphQLFloat },
		longitude: { type: GraphQLFloat }
	})
})


const LocationInputType = new GraphQLInputObjectType({
	name: 'LocationInput',
	fields: () => ({
		city: { type: GraphQLString },
		street: { type: GraphQLString },
		erf: { type: GraphQLString },
		latitude: { type: GraphQLFloat },
		longitude: { type: GraphQLFloat }
	})
})

const OrderResponseType = new GraphQLObjectType({
	name: 'OrderResponse',
	fields: () => ({
		number: { type: GraphQLString },
		password: { type: GraphQLString }
	})
})

const OrderItemType = new GraphQLInputObjectType({
	name: 'OrderItem',
	fields: () => ({
		sku: { type: GraphQLNonNull(GraphQLString) },
		quantity: { type: GraphQLNonNull(GraphQLInt) }
	})
})
export const OrderType = new GraphQLObjectType({
	name: 'Order',
	fields: () => ({
		number: { type: GraphQLNonNull(GraphQLString) },
		products: {
			type: GraphQLList(GraphQLNonNull(TransactionProductType)),
		},
		total: { type: GraphQLFloat },
		deliveryLocationType: {
			type: LocationType
		},
		status: {
			type: new GraphQLEnumType({
				name: 'status',
				values: {
					fulifilled: { value: 'fulfilled' },
					pending: { value: 'pending' },
					enroute: { value: 'enroute' }
				}
			})
		},
		shop: { type: GraphQLString },
		user: { type: GraphQLString },
		password: { type: GraphQLString }
	})
})
const TransactionProductType = new GraphQLObjectType({
	name: 'TransactionProduct',
	fields: () => ({
		quantity: { type: GraphQLInt },
		product: {
			type: ProductType,
			resolve(parent) {
				return Product.findOne({ sku: parent.sku }, { _id:0, title: 1, department: 1})
			}
		}
	})
})
const BidType = new GraphQLObjectType({
	name: 'BidType',
	fields: () => ({
		shop: { type: GraphQLString },
		offer: { type: GraphQLFloat },
		products: { type: GraphQLList(TransactionProductType)},
		status: { type: GraphQLString },
		user: { type: GraphQLString }
	})
})

export const OrderMutation = {
	/** method for handling ordering out of cart */
	cartOrder: {
		type: OrderResponseType, //returns order number and password
		args: {
			cartNumber: { type: GraphQLNonNull(GraphQLString) }
		},
		async resolve(parent, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not Signed Up')
			}

			/** get cart items from redis cache and store them in db */

			orderHandler(connection, args.items, tokenBody.username)
				.then((reply) => {
					if (reply) {

					}
				})
				.catch(error => {
					return error
				})
		}
	},
	/** method for handling direct order not from cart */
	quickOrder: {
		type: GraphQLString,
		args: {
			sku: { type: GraphQLString },
			shop: { type: GraphQLString },
			quantity: { type: GraphQLInt }
		},
		async resolve(parent, args, { tokenBody, redisConn }) {
			if (!tokenBody.username) {
				throw Error('Not logged In')
			}

			return orderHandler(connection, [{ sku: args.sku, quantity: args.quantity }], tokenBody.username)
				.then(async () => {
					/** first check if user has orders to this shop */
					var order = await Order.findOne({ user: tokenBody.username, shop: args.shop, status: 'pending' })
					if (order) {
						var dbProducts = order.products
						var found = false
						for (var i = 0; i < dbProducts.length; i++) {
							if (dbProducts[i].sku == args.sku) { //if item already exists then increament
								dbProducts[i].quantity += args.quantity
								found = true
							}
						}
						if (!found) { //else append new item
							dbProducts.push({ sku: args.sku, quantity: args.quantity })
						}
						return Order.update({ number: order.number }, { $set: { products: dbProducts } })
							.then(res => {
								return 'item added'
							})
							.catch(err => {
								return err
							})

					} else {
						var password = '3452'
						var number = '33'
						var newOrder = new Order({
							products: [{ sku: args.sku, quantity: args.quantity }],
							password,
							user: tokenBody.username,
							number,
							shop: args.shop
						})
						newOrder.save()
						return password
					}
				})
				.catch(error => {
					return error
				})
		}
	},
	updateOrder: {
		type: GraphQLString,
		args: {
			cartId: { type: GraphQLNonNull(GraphQLString) },
			toAdd: { type: OrderItemType },
			toRemove: { type: OrderItemType }
		},
		async resolve(parent, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not logged In')
			}
			if (args.toAdd) {

			}

			if (args.toRemove) {

			}
		}
	},
	cancelOrder: {
		type: GraphQLString,
		args: { number: { type: GraphQLString } },
		async resolve(parent, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not logged In')
			}
			// to cancel order set it's status to false
			return Order.update({ number: args.number }, { $set: { status: 'cancelled' } })
				.then(reply => {
					return reply
				})
				.catch(err => {
					return err
				})
		}
	},
	confirmDelivery: { //customer enters password to to confirm delivery
		type: GraphQLString,
		args: { password: { type: GraphQLString } },
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not logged In')
			}

			return Negotiation.findOneAndUpdate({ number: args.number, password: args.password }, { $set: { status: 'delivered' } })
				.then(reply => {
					return 'delivered'
				})
				.catch(error => {
					return error
				})
		}
	},
	quickBid: {
		type: GraphQLString,
		args: {
			sku: { type: GraphQLString },
			quantity: { type: GraphQLInt },
			offer: { type: GraphQLFloat },
			shop: { type: GraphQLString }
		},
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not logged in')
			}
			var bid = new Negotiation({
				number: 'ee',
				products: [{ sku: args.sku, quantity: args.quantity }],
				shop: args.shop,
				offer: args.offer,
				user: tokenBody.username
			})

			return bid.save()
				.then(reply => {
					return 'bid made'
				})
				.catch(err => {
					return err
				})

		}
	},
	/** bid for multiple items */
	batchBid: {
		type: GraphQLString,
		args: {
			sku: { type: GraphQLString },
			quantity: { type: GraphQLInt },
			offer: { type: GraphQLFloat }
		},
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not logged In')
			}
			var bid = new Negotiation({
				number: 'e',
				products: [{}]
			})
		}
	},
	acceptBid: {
		type: GraphQLString,
		args: { number: { type: GraphQLString } },
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.shop) {
				throw Error('Not shop')
			}
			return Negotiation.findOneAndUpdate({ number: args.number }, { $set: { status: 'accepted' } })
				.then((reply) => {
					return 'offer accepted'
				})
				.catch((error) => {
					return error
				})
		}
	},
	rejectBid: {
		type: GraphQLString,
		args: { number: { type: GraphQLString } },
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.shop) {
				throw Error('Not shop')
			}
			return Negotiation.findOneAndUpdate({ number: args.number }, { $set: { status: 'rejected' } })
				.then(reply => {
					return 'offer rejected'
				})
				.catch(error => {
					return error
				})
		}
	},
	counterBid: {
		type: GraphQLString,
		args: {
			number: { type: GraphQLString },
			offer: { type: GraphQLFloat },//amount to counter bid by
			quantity: { type: GraphQLInt }
		},
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.shop) {
				throw Error('Not shop')
			}

			/** set counter field */
			return Negotiation.findOneAndUpdate({ number: args.number }, { $set: { counter: args.counter } })
				.then((reply) => {
					return 'offer countered'
				})
				.catch((error) => {
					return error
				})
		}
	},
	updateBid: {
		type: GraphQLString,
		args: {
			number: { type: GraphQLString },
			offer: { type: GraphQLFloat },
			quantity: { type: GraphQLInt }
		},
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not logged In')
			}

			return Negotiation.findOneAndUpdate({ number: args.number }, { $set: { offer: args.counter } })
				.then((reply) => {
					return 'offer countered'
				})
				.catch((error) => {
					return error
				})
		}
	},
	addToCart: {
		type: GraphQLString,
		args: {
			sku: { type: GraphQLNonNull(GraphQLString) },
			quantity: { type: GraphQLNonNull(GraphQLInt) }
		},
		async resolve(_, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error('Not logged In')
			}
			return add_to_cart({ conn, user: tokenBody.username, sku: args.sku, count: args.quantity })
				.then(reply => {
					return 'item added'
				})
				.catch(err => {
					return err
				})
		}
	}
}



export const OrderQuery = {
	userOrder: {
		type: GraphQLList(OrderType),
		args: {},
		resolve(_, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error(NOT_LOGGED_IN)
			}
			return Order.find({ user: tokenBody.username, status: { $ne: 'delivered' } })
				.then(reply => {
					return reply
				}, error => {
					return error
				})
		}
	},
	userOffer: {
		type: GraphQLList(BidType),
		resolve(_, args, { tokenBody }) {
			if (!tokenBody.username) {
				throw Error(NOT_LOGGED_IN)
			}
			return Negotiation.find({ user: tokenBody.username })
				.then(reply => {
					return reply
				}, error => {
					return error
				})
		}
	},
	shopOrder: {
		type: OrderType,
		args: {},
		resolve(_, args, { tokenBody }) {
			if (!tokenBody.shop) {
				throw Error(NOT_HAVING_SHOP)
			}
			return Order.find({ shop: tokenBody.shop })
				.then(reply => {
					return reply
				}, error => {
					return error
				})
		}
	},
	shopOffer: {
		type: BidType,
		args: {

		},
		resolve(_, args, { tokenBody }) {
			if (!tokenBody.shop) {
				throw Error(NOT_HAVING_SHOP)
			}
			return Negotiation.find({ shop: tokenBody.shop })
				.then(reply => {
					return reply
				}, error => {
					return error
				})
		}
	}
}


async function orderHandler(conn: mongoose.Connection, order: [{ sku: string, quantity: number }], username: string) {
	var session = await conn.startSession({ defaultTransactionOptions: {} })
	session.startTransaction({
		readConcern: { level: 'snapshot' },
		writeConcern: { w: 'majority' },
	})
	try {

		var skus = []
		for (var i = 0; i < order.length; i++) {
			skus.push(order[i].sku)
		}

		/** find items user wishes to purchase */
		var foundItems = await Product.find({ sku: { $in: skus } }, { _id: 0, quantity: 1, list_price: 1, title: 1, sku: 1 })
		if (order.length != foundItems.length) {
			//if some are not found ask user how to proceed
			var not_found = []
			for (var i = 0; i < order.length; i++) {
				var found = false
				for (var j = 0; j < foundItems.length; j++) {
					var tmp_item = foundItems[i].toJSON()
					if (order[i].sku == tmp_item.sku) {
						found = true
						return
					}
				}

				if (found) {
					found = false
				} else {
					not_found.push(order[i].sku)
				}
			}

			return not_found
		}
		var totalCost = 0
		/** calculate total cost of items */
		for (var i = 0; i < foundItems.length; i++) {
			var tmp_item = foundItems[i].toJSON()
			totalCost += (order[i].quantity * tmp_item.list_price)
		}


		var userInfo = await User.findOne({ username }, { balance: 1 })
		if (userInfo.balance < totalCost) {
			throw Error('Insufficient amount')
		}
		/** deduct total from user balance */
		await User.updateOne({ username: username }, { $inc: { balance: -totalCost } })

		for (var i = 0; i < foundItems.length; i++) {
			var tmp_item = foundItems[i].toJSON()
			for (var j = 0; order.length; j++) {
				if (order[j].sku == tmp_item.sku) {
					var m = await Product.updateOne({ sku: tmp_item.sku }, { $inc: { quantity: -order[j].quantity } })
					if (!m.nModified) { /** verify that each document was modified */
						throw Error('an error occured') //happens when fails to update document
					}
				}
			}
		}
		return
	} catch (err) {
		session.abortTransaction()
		throw Error(err)
	}
}

