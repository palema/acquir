import { GraphQLObjectType, GraphQLString, GraphQLList, GraphQLNonNull } from "graphql"
import { ProductType } from '../product/product'
import Product from '../../../models/product.model'
import Shop from '../../../models/shop.model'
import { UserType, setToken, publicToken } from '../user/user'
import User from '../../../models/user.model'

export const ShopType = new GraphQLObjectType({
    name: 'Shop',
    fields: () => ({
        name: { type: GraphQLNonNull(GraphQLString) },
        manager: { type : GraphQLNonNull(UserType)},
        products : { 
            type : GraphQLList(GraphQLNonNull(ProductType)),
            resolve(parent){
                return Product.find({shop: parent.name})
            }
        }
    })
})

export const ShopQuery = {
    shop : {
        type : ShopType,
        args : {
            name : { type: GraphQLString }
        },
        resolve(parent, args, {tokenBody}){
            if(args.name)
                return Shop.findOne({name : args.name })
            if(tokenBody.shop)
                return Shop.findOne({name : tokenBody.shop })
            throw Error('shop not specified')
        }
    },
    shops : {
        type : GraphQLList(ShopType),
        args : {
            name : { type: GraphQLList(GraphQLString) }
        },
        resolve(parent, args){
            if(args.length > 0){
                return Shop.find({name : { $in : args.name } })
            }else{
                return Shop.find({})
            }
        }
    },
    // shopProducts: {
    //     type: GraphQLList(ProductType),
        
    //     resolve(_, { tokenBody }) {
    //         return Product.find({ shop: tokenBody.shop })
    //     }
    // }
}

export const ShopMutation = {
    createShop: {
        type : GraphQLString,
        args: {
            name : { type : GraphQLNonNull(GraphQLString) }
        },
        async resolve(parent, args, { tokenBody, res }){
            if(!tokenBody.username){
                throw Error('requires signed up user')
            }
            if(tokenBody.shop){
                throw Error('account already attached to a shop')
            }
            var shop = new Shop({
                name : args.name,
                manager : tokenBody.username
            })
            await User.findOne({username: tokenBody.username}, {_id:0, shop:1})
            .then(reply=>{
                if(reply.shop){
                    /** remove db and cookie inconsistency by updating users shop field in cookie */
                    setToken({username:tokenBody.username, shop: reply.shop.toString(), res})
                    throw Error('account already attached to a shop')
                }
            })

            return ''

            //return shop.save()
                // .then(reply=>{
                //     return User.findOneAndUpdate({username: tokenBody.username }, {$set : { shop: args.name}},{new:true})
                // })
                // .then(usereply=>{
                //     setToken({username:usereply.username.toString(), shop: usereply.shop.toString(), res})
                //     return  publicToken(usereply.username, 'seller')
                // })
                // .catch(err=>{
                //     throw err
                // })
        }
    }
}
