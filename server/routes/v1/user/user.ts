import { GraphQLObjectType, GraphQLString, GraphQLFloat, GraphQLNonNull } from 'graphql'
import { ShopType } from '../shop/shop'
import _ from 'lodash'
import Shop from '../../../models/shop.model'
import User from '../../../models/user.model'
import bcrypt from 'bcrypt'

import jwt from 'jsonwebtoken'

export const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        username: { type: GraphQLNonNull(GraphQLString) },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
        balance: { type: GraphQLFloat },
        shop: {
            type: ShopType,
            resolve(parent) {
                return Shop.findOne({ name: parent.shop })
            }
        }
    })
})

export const UserQuery = {
    login: {
        type: GraphQLString,
        args: {
            username: { type: GraphQLString },
            password: { type: GraphQLString }
        },
        resolve: async (parent, args, { res }) => {

            var data: any = await User.findOne({ $or: [{ username: args.username }, { email: args.username }] }, { username: 1, _id: 0, shop: 1, password: 1 })
            if (!data) {
                throw Error('invalid credentials')
            }

            var isSame = await bcrypt.compare(args.password, data.password.toString())
            if (isSame) {
                setToken({ username: data.username, shop: data.shop, res })
                return publicToken(data.username, data.shop ? 'seller' : 'user')
            } else {
                throw Error('invalid credentials')
            }
        }
    },
    userInfo: {
        type: UserType,
        resolve(parent, { tokenBody }) {
            return User.findOne({ username: tokenBody.username })
        }
    }
}

export const UserMutation = {
    register: {
        type: GraphQLString,
        args: {
            username: { type: GraphQLNonNull(GraphQLString) },
            password: { type: GraphQLNonNull(GraphQLString) },
            email: { type: GraphQLNonNull(GraphQLString) }
        },
        async resolve(parent, args, { res }) {
            var user = new User({
                username: args.username,
                password: args.password,
                email: args.email
            })

            user.save()
            .then(reply=>{
                setToken({ username: reply.username.toString(), shop: null, res })
                return publicToken(reply.username, 'user')
            })
            .catch(error=>{
                throw Error(error)
            })

        }
    }
}

const jwtPrivateKey = 'dhdUGdfJKJHdsYdfksd'

/**
 * 
 * @param req client request object with cookie field
 *        extracts user information from req obj
 */
export function extractTokenBody(req) {
    var tokenBody = {}
    if (req.cookies['token']) {
        tokenBody = jwt.verify(req.cookies['token'], jwtPrivateKey)
    }
    return tokenBody
}

export function setToken({ username, shop = null, res }: { username: string, shop: string, res }) {
    const token = jwt.sign(
        { username, shop },
        jwtPrivateKey,
        { expiresIn: '7d' }
    )


    res.cookie('token', token, {
        expiresIn: 10000 * 60 * 60 * 24 * 7
    })
}

export function publicToken(username, rights) {
    return jwt.sign(
        { login: true, rights, username },
        'sdfladklfsuKJHKsda',
        { expiresIn: '7d' }
    )

}