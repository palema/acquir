import { GraphQLSchema, GraphQLObjectType } from 'graphql'
import _ from 'lodash'

/* Local imports */
import { ProductQuery, ProductMutation } from './product/product'
import { UserQuery, UserMutation } from './user/user'
import { ShopMutation, ShopQuery } from './shop/shop'
import { DepartmentQuery } from './department/department'
import { HomeQuery } from './home'
import { OrderMutation, OrderQuery } from './transaction'

/* Defines all queries that can be perfomed on this schema */
const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: _.merge(DepartmentQuery, UserQuery, ProductQuery, HomeQuery, ShopQuery, OrderQuery )
})

/* Defines all mutations on objects */
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: _.merge(ProductMutation, ShopMutation, UserMutation, OrderMutation)
})

const schema = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})

export default schema